/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import com.password4j.Hash;
import com.password4j.Password;
import java.util.ArrayList;
import java.util.UUID;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author tddaij
 */

public class Utility {

    public String passwordEncode(String plainText) {
        Hash hash = Password.hash(plainText).withBCrypt();
        return hash.getResult();
    }

    public boolean passwordDecode(String plainText, String encryptPassword) {
        return Password.check(plainText, encryptPassword).withBCrypt();
    }

    public String generateToken() {
        StringBuilder sb = new StringBuilder();
        sb = sb.append(UUID.randomUUID());
        sb = sb.append(UUID.randomUUID());

        System.out.println(sb.toString());
        return sb.toString();
    }
    
    public List<Integer> page (List<?> list, String page_raw, int amount_per_page){
        int current;
        if (page_raw == null || page_raw.isEmpty()) {
            current = 1;
        } else {
            current = Integer.parseInt(page_raw);
        }
        int next = current + 1;
        int prev = current - 1;
        int start = (current - 1) * amount_per_page;
        int end;
        if (current * amount_per_page > list.size()) {
            end = list.size();
        } else {
            end = current * amount_per_page;
        }
        int size = list.size();
        int pagenum = (size % amount_per_page == 0) ? (size / amount_per_page) : (size / amount_per_page + 1);
        return Arrays.asList(current, start, prev, next, end, pagenum);
    }
    
  public String getParam(
            String paramName, String defaultValue, HttpServletRequest req) {
        String paramVal = req.getParameter(paramName);
        if (paramVal == null || "".equals(paramVal.trim())) {
            paramVal = defaultValue;
        }
        return paramVal;
    }
  

    public String test(int number) {
        return null;
    }

    public static void main(String[] args) {
        int number = 3;
        StringBuilder s = new StringBuilder();
        for (int i = 1; i <= 5; i++) {
            if (i < number) {
                s.append("<a href=\"#\" data-rating-value=\"" + i + "\" data-rating-text=\"" + i + "\" class=\"br-selected\"></a>");
            } else if (i == number) {
                s.append("<a href=\"#\" data-rating-value=\"" + i + "\" data-rating-text=\"" + i + "\" class=\"br-selected br-current\"></a>");
            } else {
                s.append("<a href=\"#\" data-rating-value=\"" + i + "\" data-rating-text=\"" + i + "\"></a>");
            }
        }
        System.out.println(s.toString());
    }

}
