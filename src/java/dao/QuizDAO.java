/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import model.Quiz;
import utility.Utility;

/**
 *
 * @author tddaij
 */
public class QuizDAO {

    Utility util = new Utility();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    public List<Quiz> getAllQuiz() {
        List<Quiz> list = new ArrayList<>();
        String GET_QUIZ_BY_COURSE_ID_QUERY = "select id, name, description, thumbnail,categoryid, courseid, topicid, "
                + "lessonid, numofquest, timelimit, level, created_date, modified_date, publish from quiz where publish = true";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_QUIZ_BY_COURSE_ID_QUERY);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
               list.add(new Quiz(rs.getInt(1),
                        rs.getNString(2),
                        rs.getNString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10), rs.getInt(11), rs.getString(12), rs.getString(13), rs.getBoolean(14)));
            }
               conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
     
        return list;
    }
     public List<Quiz> searchQuiz(String name, String category, String level) {
        List<Quiz> list = new ArrayList<>();
        String query = "Select * from quiz where 1=1";

        if (name != null && !name.isEmpty()) {
            query = query + " AND name LIKE '%" + name + "%'";
        }
        if (category != null && !category.isEmpty()) {
            query = query + " AND categoryid = " + category ;
        }
        if (level != null && !level.isEmpty()) {
            query = query + " AND level = " + level ;
        }
        
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Quiz(rs.getInt(1),
                        rs.getNString(2),
                        rs.getNString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10), rs.getInt(11), rs.getString(12), rs.getString(13), rs.getBoolean(14)));

            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Quiz> getQuizByCourseID(int courseid) {
        List<Quiz> list = new ArrayList<>();
        String GET_QUIZ_BY_COURSE_ID_QUERY = "select id, name, description, thumbnail,categoryid, courseid, topicid, "
                + "lessonid, numofquest, timelimit, level, created_date, modified_date, publish from quiz "
                + "where courseid = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_QUIZ_BY_COURSE_ID_QUERY);
            ps.setInt(1, courseid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Quiz(rs.getInt(1),
                        rs.getNString(2),
                        rs.getNString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10), rs.getInt(11), rs.getString(12), rs.getString(13), rs.getBoolean(14)));
            }
           
        } catch (Exception e) {
            e.printStackTrace();
        }
     
        return list;
    }
    public List<Quiz> searchQuiz(String name, String courseid, String topicid, String lessonid) {
        List<Quiz> list = new ArrayList<>();
        String query = "select id, name, description, thumbnail,categoryid, courseid, topicid, "
                + "lessonid, numofquest,timelimit, level, created_date, modified_date, publish from quiz "
                + "where 1=1";
        if (name != null && !name.isEmpty()){
            query += " AND name LIKE '%" + name + "%'";
        }
        if (courseid != null && !courseid.isEmpty()){
            query += " AND courseid = " + courseid;
        }
        if (topicid != null && !topicid.isEmpty()){
            query += " AND topicid = " + topicid;
        }
        if (lessonid != null && !lessonid.isEmpty()){
            query += " AND lessonid = " + lessonid;
        }
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
               list.add(new Quiz(rs.getInt(1),
                        rs.getNString(2),
                        rs.getNString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10), rs.getInt(11), rs.getString(12), rs.getString(13), rs.getBoolean(14)));
            }
               conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    public List<Quiz> getQuizListByPage(List<Quiz> list, int start, int end) {

        List<Quiz> listP = new ArrayList<>();
        for (int i = start; i < end; i++) {
            listP.add(list.get(i));
        }

        return listP;
    }
    
    public static void main(String[] args) {
        QuizDAO daoQ = new QuizDAO();
        List<Quiz> list = daoQ.searchQuiz("Quiz", null, null);
        for (Quiz quiz : list) {
            System.out.println(quiz);
        }
    }
}
