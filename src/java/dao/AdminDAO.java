/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import utility.Utility;

/**
 *
 * @author tddaij
 */
public class AdminDAO {

    Utility util = new Utility();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    private DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private DateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();

    public boolean addAccount(String username, String email, String password, String name, String gender, String dob,  String phone, String bio, String role, String status) {
        String ADD_ACCOUNT_QUERY = "INSERT INTO user(username, email, password, name, gender, dob, phone, bio, role, status, avatar, created) values (?,?,?,?,?,?,?,?,?,?,?,?)";
        
        String date = now.toString();
        String encodedPassword = util.passwordEncode(password);
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(ADD_ACCOUNT_QUERY);
            ps.setString(1, username);
            ps.setString(2, email);
            ps.setString(3, encodedPassword);
            ps.setNString(4, name);
            ps.setString(5, gender);
            ps.setString(6, dob);
            ps.setString(7, phone);
            ps.setNString(8, bio);
            ps.setString(9, role);
            ps.setString(10, status);
            ps.setString(11, "default.jpg");
            ps.setString(12, date);
            int rs = ps.executeUpdate();
            if (rs == 1) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
    public int clearToken (){
        String CLEAR_TOKEN_QUERY = "DELETE FROM user_token_log WHERE valid = 0 OR expired_time < ?";
       
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(CLEAR_TOKEN_QUERY);
            ps.setString(1, now.toString());
            
            int numberTokenDeleted = ps.executeUpdate();
            return numberTokenDeleted;
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }
    
    public boolean deleteAllTokenByUserID (String id){
        String DELETE_TOKEN_QUERY = "DELETE FROM user_token_log WHERE id = ?";
       
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(DELETE_TOKEN_QUERY);
            ps.setString(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
    
    public List<Account> searchAccount(String id, String name, String email, String gender, String phone, String role, String status) {
        List<Account> list = new ArrayList<>();
        String query = "Select * from user where 1=1";

        if (id != null && !id.isEmpty()) {
            query = query + " AND id = " + id;
        }
        if (name != null && !name.isEmpty()) {
            query = query + " AND name LIKE '%" + name + "%'";
        }
        if (email != null && !email.isEmpty()) {
            query = query + " AND email LIKE '%" + email + "%'";
        }
        if (gender != null && !gender.isEmpty()) {
            query = query + " AND gender = " + gender;
        }
        if (phone != null && !phone.isEmpty()) {
            query = query + " AND phone LIKE '%" + phone + "%'";
        }
        if (role != null && !role.isEmpty()) {
            query = query + " AND role = " + role;
        }
        if (status != null && !status.isEmpty()) {
            query = query + " AND status = " + status;
        }
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getNString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getString(7),
                        rs.getNString(8),
                        rs.getString(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12),
                        rs.getString(13),
                        rs.getString(14)
                ));

            }
        } catch (Exception e) {
        }
        return list;
    }

    public Account getAccount(String id) {

        String GET_ACCOUNT_QUERY = "select * from user where id = ?";
        String dob = null;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_ACCOUNT_QUERY);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                 if (rs.getString(5) != null) {
                    dob = new SimpleDateFormat("dd/MM/yyyy").format(sdf.parse(rs.getString(5)));
                } else {
                    dob = "";
                }
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getNString(4),
                        dob,
                        rs.getInt(6),
                        rs.getString(7),
                        rs.getNString(8),
                        rs.getString(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12),
                        rs.getString(13),
                        rs.getString(14));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean deleteAccount(String id) {
        String DELETE_ACCOUNT_QUERY = "DELETE from user where id = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(DELETE_ACCOUNT_QUERY);
            ps.setString(1, id);
            int rs = ps.executeUpdate();
            if (rs == 1) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public List<Account> getAccountByPage(List<Account> list, int start, int end) {

        List<Account> listP = new ArrayList<>();
        for (int i = start; i < end; i++) {
            listP.add(list.get(i));
        }

        return listP;
    }

    public boolean updateInformation(String id, String username, String name, String gender, String dob, String phone, String bio, String role, String status) {
        
        String ADMIN_UPDATE_USER_INFORMATION_QUERY = "UPDATE user SET username = ?, name = ?, gender = ?, dob = ?, phone = ?, bio = ?, role = ?, status = ? WHERE id = ?";
        try {
            dob = new SimpleDateFormat("yyyy-MM-dd").format(sdf2.parse(dob));
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(ADMIN_UPDATE_USER_INFORMATION_QUERY);
            ps.setString(1, username);
            ps.setNString(2, name);
            ps.setString(3, gender);
            ps.setString(4, dob);
            ps.setString(5, phone);
            ps.setNString(6, bio);
            ps.setString(7, role);
            ps.setString(8, status);
            ps.setString(9, id);
            int rs = ps.executeUpdate();
            if (rs > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public List<Account> getListAccount() {
        List<Account> list = new ArrayList<>();
        String GET_LIST_ACCOUNT_QUERY = "SELECT * from user";
        String dob = null;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_LIST_ACCOUNT_QUERY);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString(5) != null) {
                    dob = new SimpleDateFormat("dd/MM/yyyy").format(sdf.parse(rs.getString(5)));
                } else {
                    dob = "";
                }
                list.add(new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getNString(4),
                        dob,
                        rs.getInt(6),
                        rs.getString(7),
                        rs.getNString(8),
                        rs.getString(9),
                        rs.getInt(10),
                        rs.getInt(11),
                        rs.getInt(12),
                        rs.getString(13),
                        rs.getTime(14) + " " + new SimpleDateFormat("dd/MM/yyyy").format(sdf.parse(rs.getString(14)))
                ));
            }
        } catch (Exception e) {
        }
        return list;

    }

}
