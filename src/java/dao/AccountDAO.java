/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Course;
import model.WishList;
import utility.Utility;

/**
 *
 * @author tddaij
 */
public class AccountDAO {

    CourseDAO daoCo = new CourseDAO();
    Utility util = new Utility();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private DateFormat sdf2 = new SimpleDateFormat("MM/dd/yyyy");
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();

    public Account login(String email, String password) {
        Account account = new Account();
        String LOGIN_QUERY = "select * from `user` where email LIKE ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(LOGIN_QUERY);
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (util.passwordDecode(password, rs.getString(13))) {
                    account.setID(rs.getInt(1));
                    account.setUsername(rs.getString(2));
                    account.setEmail(rs.getString(3));
                    account.setName(rs.getString(4));
                    account.setDob(new SimpleDateFormat("dd/MM/yyyy").format(sdf.parse(rs.getString(5))));
                    account.setGender(rs.getInt(6));
                    account.setPhone(rs.getString(7));
                    account.setBio(rs.getString(8));
                    account.setAvatar(rs.getString(9));
                    account.setLevel(rs.getInt(10));
                    account.setRole(rs.getInt(11));
                    account.setStatus(rs.getInt(12));
                    account.setPassword(rs.getString(13));
                    account.setCreated(rs.getString(14));
                    account.setWishlist(getWishList(account.getID()));
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return account;
    }

    public List<WishList> getWishList(int id) {
        List<WishList> list = new ArrayList<>();
        String GET_WISHLIST = "select * from `wishlist` where uid = ?";
        int i = 0;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_WISHLIST);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new WishList(rs.getInt(1),
                        rs.getInt(2),
                        null,
                        rs.getString(4)));
            }
            while (i != list.size()) {
                list.get(i).setCourse(daoCo.getCourseById(list.get(i).getId()));
                i++;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public boolean checkEmailExisted(String email) {

        String CHECK_EMAIL_EXISTED_QUERY = "select * from `user` where email LIKE ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(CHECK_EMAIL_EXISTED_QUERY);
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                {
                    return true;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean checkUsernameExisted(String username) {

        String CHECK_USERNAME_EXISTED_QUERY = "select * from `user` where username LIKE ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(CHECK_USERNAME_EXISTED_QUERY);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                {
                    conn.close();
                    return true;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public int getUserStatusByEmail(String email) {
        String GET_USER_STATUS_QUERY = "select status from `user` where email LIKE ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_USER_STATUS_QUERY);
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                {
                    return rs.getInt(1);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return -1;
    }

    public boolean register(String name, String username, String email, String password) {
        String REGISTER_QUERY = "INSERT INTO `user`(username, email, name, password, role, status, avatar, created) values (?,?,?,?,?,?,?,?)";
        int role = 0; //Role 0 = user , role 1 = admin
        int status = 0; // not activate = 0, activated = 1, banned = -1
        String date = now.toString();
        String encodedPassword = util.passwordEncode(password);
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(REGISTER_QUERY);
            ps.setString(1, username);
            ps.setString(2, email);
            ps.setNString(3, name);
            ps.setString(4, encodedPassword);
            ps.setInt(5, role);
            ps.setInt(6, 0);
            ps.setString(7, "default.jpg");
            ps.setString(8, date);
            int rs = ps.executeUpdate();
            if (rs == 1) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean updateInformation(int id, String name, int gender, String dob, String phone, String bio) {
        String UPDATE_USER_INFORMATION_QUERY = "UPDATE user SET name = ?, gender = ?, dob = ?, phone = ?, bio = ? WHERE id = ?";
        try {

            conn = new DBContext().getConnection();
            dob = new SimpleDateFormat("yyyy-MM-dd").format(sdf2.parse(dob));
            ps = conn.prepareStatement(UPDATE_USER_INFORMATION_QUERY);
            ps.setNString(1, name);
            ps.setInt(2, gender);
            ps.setString(3, dob);
            ps.setString(4, phone);
            ps.setNString(5, bio);
            ps.setInt(6, id);
            int rs = ps.executeUpdate();
            if (rs > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean updateStatus(String status) {
        String UPDATE_USER_STATUS_QUERY = "UPDATE `user` SET `status` = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(UPDATE_USER_STATUS_QUERY);
            if (status.equals("block")) {
                ps.setInt(1, -1);
            } else if (status.equals("verified")) {
                ps.setInt(1, 1);
            } else {
                ps.setInt(1, 0);
            }
            int rs = ps.executeUpdate();
            if (rs > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean changePassword(int id, String newPassword) {
        String CHANGE_USER_PASSWORD_QUERY = "UPDATE `user` SET password = ? WHERE id = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(CHANGE_USER_PASSWORD_QUERY);
            ps.setString(1, util.passwordEncode(newPassword));
            ps.setInt(2, id);
            int rs = ps.executeUpdate();
            if (rs > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public int getUserIdByEmail(String email) {

        String GET_USER_ID_BY_EMAIL_QUERY = "SELECT `user`.id FROM `user` WHERE email LIKE ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_USER_ID_BY_EMAIL_QUERY);
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                {
                    return rs.getInt(1);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return -1;
    }

    public int getUserIdByToken(String token) {

        String GET_USER_ID_BY_TOKEN_QUERY = "SELECT id FROM user_token_log WHERE token LIKE ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_USER_ID_BY_TOKEN_QUERY);
            ps.setString(1, token);
            rs = ps.executeQuery();
            while (rs.next()) {
                {
                    return rs.getInt(1);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return -1;
    }

    public boolean checkTokens(String tokens) {

        String CHECK_TOKEN_QUERY = "SELECT * FROM user_token_log WHERE token LIKE ? AND valid = 1 AND expired_time > ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(CHECK_TOKEN_QUERY);
            ps.setString(1, tokens);
            ps.setString(2, now.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean invalidateToken(String tokens) {

        String INVALIDATE_TOKEN_QUERY = "UPDATE `user_token_log` SET valid = 0 WHERE token = ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(INVALIDATE_TOKEN_QUERY);
            ps.setString(1, tokens);
            int rs = ps.executeUpdate();
            if (rs == 1) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean insertToken(int id, String token, String action) {
        String date = now.toString();
        String INSERT_TOKEN_QUERY = "INSERT INTO `user_token_log` VALUES(?,?,?,?,?,?)";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(INSERT_TOKEN_QUERY);
            ps.setInt(1, id);
            ps.setString(2, token);
            ps.setString(3, action);
            ps.setBoolean(4, true);
            ps.setString(5, date);
            if (action.equalsIgnoreCase("register")) {
                ps.setString(6, now.plusMinutes(60).toString());
            } else if (action.equalsIgnoreCase("forgot")) {
                ps.setString(6, now.plusMinutes(15).toString());
            }
            int rs = ps.executeUpdate();
            if (rs > 0) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public static void main(String[] args) {
        AccountDAO daoAc = new AccountDAO();
        Account a = daoAc.login("admin2@gmail.com", "admin");
        System.out.println(a);
        List<WishList> list = a.getWishlist();
        for (WishList wishList : list) {
            System.out.println(wishList);
        }

        List<WishList> listA = daoAc.getWishList(2);
        for (WishList wishList : listA) {
            System.out.println(wishList);
        }
    }
}
