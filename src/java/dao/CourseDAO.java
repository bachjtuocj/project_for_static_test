/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import model.Category;
import model.Course;
import model.CourseTopic;
import model.TopicLesson;
import utility.Utility;

import java.util.List;
import model.CourseReview;
import model.Discussion;
import model.DiscussionReply;

/**
 *
 * @author dai8p
 */
public class CourseDAO {

    Utility util = new Utility();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();

    public List<Category> getAllCategory() {
        List<Category> list = new ArrayList<>();
        String GET_ALL_CATEGORY = "select * from category";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_ALL_CATEGORY);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt(1),
                        rs.getString(2)));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Course> getAllCourse() {

        List<Course> list = new ArrayList<>();
        String GET_ALL_COURSE = "select * from course";
        try {
            //mo ket noi voi sql
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_ALL_COURSE);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Course(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3),
                        rs.getNString(4),
                        rs.getNString(5),
                        rs.getFloat(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        null));
            }
            int i = 0;
            while (i != list.size()) {
                list.get(i).setCourseReviewList(getCourseReviewByCourseId(list.get(i).getId()));
                i++;
            }
            return list;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public Course getCourseById(int id) {

        String GET_COURSE_QUERY = "select * from `course` where id = ?";
        try {

            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_COURSE_QUERY);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Course(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getFloat(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        getCourseReviewByCourseId(rs.getInt(1)));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Course> getCourseByPage(List<Course> list, int start, int end) {
        List<Course> listP = new ArrayList<>();
        for (int i = start; i < end; i++) {
            listP.add(list.get(i));
        }
        return listP;
    }

    public void insert(String name, String description, String thumbnail,
            String price, String content, String level, String duration, String category_id) {
        List<Course> list = new ArrayList<>();
        String INSERT_NEW_COURSE = "INSERT into course "
                + "(name,description,thumbnail, "
                + "price,content, "
                + "level,duration,"
                + "category_id)\n"
                + "VALUES(?,?,?,?,?,?,?,?)";
        try {
            //mo ket noi voi sql
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(INSERT_NEW_COURSE);
            ps.setString(1, name);
            ps.setString(2, description);
            ps.setString(3, thumbnail);
            ps.setString(4, price);
            ps.setString(5, content);
            ps.setString(6, level);
            ps.setString(7, duration);
            ps.setString(8, category_id);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void update(String name, String description, String thumbnail,
            String price, String content, String level, String duration, String category_id, String pid) {
        String UPDATE_COURSE_INFORMATION = "update course\n"
                + "set name=?,\n"
                + "description=?,\n"
                + "thumbnail=?, \n"
                + "price=?,\n"
                + "content=?, \n"
                + "level=?,\n"
                + "duration=?,\n"
                + "category_id=?\n"
                + "where id=?";
        try {
            //mo ket noi voi sql
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(UPDATE_COURSE_INFORMATION);
            ps.setString(1, name);
            ps.setString(2, description);
            ps.setString(3, thumbnail);
            ps.setString(4, price);
            ps.setString(5, content);
            ps.setString(6, level);
            ps.setString(7, duration);
            ps.setString(8, category_id);
            ps.setString(9, pid);
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void deleteCourse(String pid) {
        String DELETE_COURSE_BY_ID = "delete course where id=?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(DELETE_COURSE_BY_ID);
            ps.setString(1, pid);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public List<Course> searchCourse(String input) {
        List<Course> list = new ArrayList<>();
        String SEARCH_COURSE_BY_ANY_INFORMATION = "Select * from course where name like ? or price like ? or level like ? ";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(SEARCH_COURSE_BY_ANY_INFORMATION);
            ps.setString(1, '%' + input + '%');
            ps.setString(2, '%' + input + '%');
            ps.setString(3, '%' + input + '%');
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Course(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3),
                        rs.getNString(4),
                        rs.getNString(5),
                        rs.getFloat(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getInt(9),
                        rs.getInt(10), null));
            }
            int i = 0;
            while (i != list.size()) {
                list.get(i).setCourseReviewList(getCourseReviewByCourseId(list.get(i).getId()));
                i++;
            }
            return list;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Course> getCourseOrderBy(String action) {
        List<Course> list = new ArrayList<>();
        String COURSE_ORDER_BY_ACTION = "Select * from course order by ";
        if (action.equalsIgnoreCase("sortID")) {
            COURSE_ORDER_BY_ACTION = COURSE_ORDER_BY_ACTION + " id asc";
        }
        if (action.equalsIgnoreCase("sortName")) {
            COURSE_ORDER_BY_ACTION = COURSE_ORDER_BY_ACTION + " name asc";
        }
        if (action.equalsIgnoreCase("sortPrice")) {
            COURSE_ORDER_BY_ACTION = COURSE_ORDER_BY_ACTION + " price asc";
        }

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(COURSE_ORDER_BY_ACTION);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Course(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3),
                        rs.getNString(4),
                        rs.getNString(5),
                        rs.getFloat(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        null));
            }
            int i = 0;
            while (i != list.size()) {
                list.get(i).setCourseReviewList(getCourseReviewByCourseId(list.get(i).getId()));
                i++;
            }
            return list;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public Course checkAccountExist(String courseName) {
        String CHECK_ACCOUNT_BY_NAME = "select * from course\n"
                + "where `name`=?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(CHECK_ACCOUNT_BY_NAME);
            ps.setString(1, courseName);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Course(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3),
                        rs.getNString(4),
                        rs.getNString(5),
                        rs.getFloat(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getInt(9),
                        rs.getInt(10), null);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean checkPrice(String price) {
        int value = Integer.parseInt(price);
        if (value <= 0) {
            return false;
        }
        return true;
    }

    public List<CourseTopic> getAllCourseTopic() {

        String GET_COURSE_TOPIC_QUERY = "select * from `course_topic`";
        int i = 0;

        try {
            List<CourseTopic> result = new ArrayList<>();
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_COURSE_TOPIC_QUERY);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(new CourseTopic(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3),
                        rs.getNString(4), null));
            }

            while (i != result.size()) {
                result.get(i).setTopicLesson(getTopicLessonByTopicID(result.get(i).getId()));
                i++;
            }
            return result;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public List<TopicLesson> getAllTopicLesson() {

        String GET_TOPIC_LESSON_QUERY = "select * from `topic_lesson`";

        try {
            List<TopicLesson> result = new ArrayList<>();
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_TOPIC_LESSON_QUERY);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(new TopicLesson(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3),
                        rs.getNString(4),
                        rs.getNString(5)));
            }
            return result;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public List<TopicLesson> getTopicLessonByTopicID(int id) {
        String GET_USER_STATUS_QUERY = "select * from `topic_lesson` where topic_id = ?";

        try {
            List<TopicLesson> result = new ArrayList<>();
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_USER_STATUS_QUERY);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(new TopicLesson(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3),
                        rs.getNString(4),
                        rs.getNString(5)));
            }
            return result;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public TopicLesson getTopicLessonByID(int id) {
        String GET_USER_STATUS_QUERY = "select * from `topic_lesson` where id = ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_USER_STATUS_QUERY);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new TopicLesson(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3),
                        rs.getNString(4),
                        rs.getNString(5));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public TopicLesson getTopicLessonByName(String name) {
        String GET_USER_STATUS_QUERY = "select * from `topic_lesson` where name LIKE ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_USER_STATUS_QUERY);
            ps.setNString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new TopicLesson(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3),
                        rs.getNString(4),
                        rs.getNString(5));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public int getTheLastestDiscussionId() {
        String GET_THE_FINAL_DISCUSSION_ID = "select id+1 from `discussion` order by id desc limit 1";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_THE_FINAL_DISCUSSION_ID);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (!checkIfDiscussionExist(rs.getInt(1))) {
                    return rs.getInt(1);
                } else {
                    getTheLastestDiscussionId();
                }
            }
            throw new Exception("discussion empty");
        } catch (Exception e) {
            System.out.println(e);
        }
        return getTheAutoIncrementIdWhenTheTableIsEmpty("elearning", "discussion");
    }

    public int getTheAutoIncrementIdWhenTheTableIsEmpty(String databaseName, String table) {
        String GET_THE_LASTEST_ID_IN_TABLE_QUERY = "select `auto_increment`\n"
                + "from  information_schema.tables\n"
                + "where table_schema = ?\n"
                + "and   table_name   = ?;";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_THE_LASTEST_ID_IN_TABLE_QUERY);
            ps.setNString(1, databaseName);
            ps.setNString(2, table);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            throw new Exception("discussion empty");
        } catch (Exception e) {
            System.out.println(e);
        }
        return -1;
    }

    public List<Discussion> getDiscussion() {
        String GET_DISCUSSION_QUERY = "select * from `discussion`";
        List<Discussion> result = new ArrayList<>();

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_DISCUSSION_QUERY);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(new Discussion(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(4),
                        rs.getNString(3),
                        null));
            }
            int i = 0;
            while (i != result.size()) {
                result.get(i).setDiscussionReplyList(getDiscussionReplyByDiscussionId(result.get(i).getId()));
                i++;
            }
            return reverseList(result);
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public List<DiscussionReply> getDiscussionReplyByDiscussionId(int discussionId) {
        String GET_DISCUSSION_REPLY_BY_DISCUSSION_ID = "select * from `discussion_reply` where discussion_id = ?";
        List<DiscussionReply> result = new ArrayList<>();

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_DISCUSSION_REPLY_BY_DISCUSSION_ID);
            ps.setInt(1, discussionId);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(new DiscussionReply(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3)));
            }
            return reverseList(result);
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public List<DiscussionReply> getDiscussionReply() {
        String GET_DISCUSSION_REPLY_QUERY = "select * from `topic_lesson`";
        List<DiscussionReply> result = new ArrayList<>();

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_DISCUSSION_REPLY_QUERY);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(new DiscussionReply(rs.getInt(1),
                        rs.getInt(2),
                        rs.getNString(3)));
            }
            return reverseList(result);
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean insertDiscussion(int lessonId, String content, int userId) {
        List<Course> list = new ArrayList<>();
        String INSERT_NEW_DISCUSSION = "insert into discussion (lesson_id, content, user_id) VALUES (?, ?, ?)";
        try {
            if (!checkIfLessonIdExist(lessonId) || !checkIfUserIdExist(userId)) {
                throw new Exception("lesson_id or user_id not found!");
            }
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(INSERT_NEW_DISCUSSION);
            ps.setInt(1, lessonId);
            ps.setNString(2, content);
            ps.setInt(3, userId);
            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;

    }

    public boolean insertDiscussionReply(int discussionId, String content) {
        List<Course> list = new ArrayList<>();
        String INSERT_NEW_DISCUSSION_REPLY_QUERY = "insert into discussion_reply (discussion_id, content) VALUES (?, ?)";
        try {
            if (!checkIfDiscussionExist(discussionId)) {
                throw new Exception("discussion not found");
            }
            //mo ket noi voi sql
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(INSERT_NEW_DISCUSSION_REPLY_QUERY);
            ps.setInt(1, discussionId);
            ps.setNString(2, content);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean checkIfDiscussionExist(int id) {
        String CHECK_DISCUSSION_EXIST_QUERY = "select id from `discussion` where id = ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(CHECK_DISCUSSION_EXIST_QUERY);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean checkIfLessonIdExist(int lessonId) {
        String CHECK_LESSON_EXIST_QUERY = "select id from `topic_lesson` where id = ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(CHECK_LESSON_EXIST_QUERY);
            ps.setInt(1, lessonId);
            rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean checkIfUserIdExist(int userId) {
        String CHECK_USER_EXIST_QUERY = "select id from `user` where id = ?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(CHECK_USER_EXIST_QUERY);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public List<CourseReview> getCourseReviewByCourseId(int courseId) {
        String GET_COURSE_REVIEW_REPLY_QUERY = "select * from `course_review` where course_id  = ?";
        List<CourseReview> result = new ArrayList<>();

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(GET_COURSE_REVIEW_REPLY_QUERY);
            ps.setInt(1, courseId);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(new CourseReview(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getNString(4),
                        rs.getInt(5)));
            }
            return reverseList(result);
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean insertCourseReview(int userId, int courseId, String content, int rating) {
        String INSERT_NEW_COURSE = "insert into course_review (user_id, course_id, content, rating) values (?, ?, ?, ?)";
        try {
            //mo ket noi voi sql
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(INSERT_NEW_COURSE);
            ps.setInt(1, userId);
            ps.setInt(2, courseId);
            ps.setNString(3, content);
            ps.setInt(4, rating);
            ps.executeUpdate();

            return true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public static <T> List<T> reverseList(List<T> list) {
        List<T> reverse = new ArrayList<>(list);
        Collections.reverse(reverse);
        return reverse;
    }

    public static void main(String[] args) {
        System.out.println(new CourseDAO().getCourseById(1));
    }
}
