/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author tddaij
 */
public class UserLoginFilter implements Filter {

        @Override
        public void init(FilterConfig filterConfig) throws ServletException {}

        @Override
        public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) res;
            HttpSession session = request.getSession(false);
            String url = request.getRequestURI();
            if (session == null || session.getAttribute("AuthenticationUser") == null) {
                response.sendRedirect("auth?action=login");
            } else {
                Account a = (Account) session.getAttribute("AuthenticationUser");
                if (a.getStatus() == -1){
                    session.invalidate();
                    response.getWriter().write("Banned!");
                } else if(a.getStatus() == 0){
                    session.invalidate();
                    response.getWriter().write("This account is not activate.");
                } else {
                chain.doFilter(request, response);
                }
            }
        }

        @Override
        public void destroy() {}

    

    }
