/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author dai8p
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class TopicLesson {

    private int id;
    private int topicId;
    private String name;
    private String description;
    private String material;
}
