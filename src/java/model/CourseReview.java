/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author dai8p
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class CourseReview {

    private int id;
    private int userId;
    private int courseId;
    private String content;
    private int rating;
}
