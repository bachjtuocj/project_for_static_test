/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author tddaij
 */
public class WishList {
    private int id;
    private int uid; 
    private Course course;
    private String created_date;

    public WishList() {
    }

    public WishList(int id, int uid, Course course, String created_date) {
        this.id = id;
        this.uid = uid;
        this.course = course;
        this.created_date = created_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    @Override
    public String toString() {
        return "WishList{" + "id=" + id + ", uid=" + uid + ", course=" + course + ", created_date=" + created_date + '}';
    }

  
    
    
}
