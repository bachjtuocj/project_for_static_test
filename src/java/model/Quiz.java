/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author tddaij
 */
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Quiz {
    private int id;
    private String name;
    private String description;
    private String thumbnail;
    private int category;
    private int courseid;
    private int topicid;
    private int lessonid;
    private int numofquest;
    private int timelimit;
    private int level;
    private String created_date;
    private String modified_date;
    private boolean publish;

 
     
}
