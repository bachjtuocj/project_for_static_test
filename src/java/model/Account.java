/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author tddaij
 */


public class Account {
    private int ID;
    private String username;
    private String email;
    private String name;
    private String dob;
    private int gender;
    private String phone;
    private String bio;
    private String avatar;
    private int level;
    private int role;
    private int status;
    private String password;
    private String created;
    private List<WishList> wishlist;

    public Account() {
    }

    public Account(int ID, String username, String email, String name, String dob, int gender, String phone, String bio, String avatar, int level, int role, int status, String password, String created, List<WishList> wishlist) {
        this.ID = ID;
        this.username = username;
        this.email = email;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.phone = phone;
        this.bio = bio;
        this.avatar = avatar;
        this.level = level;
        this.role = role;
        this.status = status;
        this.password = password;
        this.created = created;
        this.wishlist = wishlist;
    }
    
    public Account(int ID, String username, String email, String name, String dob, int gender, String phone, String bio, String avatar, int level, int role, int status, String password, String created) {
        this.ID = ID;
        this.username = username;
        this.email = email;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.phone = phone;
        this.bio = bio;
        this.avatar = avatar;
        this.level = level;
        this.role = role;
        this.status = status;
        this.password = password;
        this.created = created;
    
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public List<WishList> getWishlist() {
        return wishlist;
    }

    public void setWishlist(List<WishList> wishlist) {
        this.wishlist = wishlist;
    }

    @Override
    public String toString() {
        return "Account{" + "ID=" + ID + ", username=" + username + ", email=" + email + ", name=" + name + ", dob=" + dob + ", gender=" + gender + ", phone=" + phone + ", bio=" + bio + ", avatar=" + avatar + ", level=" + level + ", role=" + role + ", status=" + status + ", password=" + password + ", created=" + created + ", wishlist=" + wishlist + '}';
    }
    
}
