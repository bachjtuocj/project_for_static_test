/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author dai8p
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Discussion {

    private int id;
    private int lesson_id;
    private int user_id;
    private String content;
    private List<DiscussionReply> discussionReplyList = new ArrayList<>();

}
