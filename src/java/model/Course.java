/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Course {

    private int id;//1
    private int category_id;//2
    private String name;//3
    private String description;//4
    private String thumbnail;//5
    private float price;//6
    private int rating;//7
    private String content;//8
    private int level;//9
    private int duration;//10
    private List<CourseReview> courseReviewList = new ArrayList<>();

}
