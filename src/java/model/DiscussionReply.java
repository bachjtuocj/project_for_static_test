/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author dai8p
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class DiscussionReply {

    private int id;
    private int discussionId;
    private String content;
}
