/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.AccountDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import utility.GoogleCaptcha;
import utility.Mail;
import utility.Utility;

/**
 *
 * @author tddaij
 */
@WebServlet(name = "Authentication", urlPatterns = {"/auth"})
public class AuthenticationServlet extends HttpServlet {

    AccountDAO daoAc = new AccountDAO();
    GoogleCaptcha captcha = new GoogleCaptcha();
    Mail mail = new Mail();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (session.getAttribute("AuthenticationUser") != null) {
            response.sendRedirect("home");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String action = request.getParameter("action");
        if (action == null) {
            action = "login";
        }
        if (action.equalsIgnoreCase("login")) {
            request.getRequestDispatcher("resources/signin.jsp").forward(request, response);
        }
        if (action.equalsIgnoreCase("register")) {
            request.getRequestDispatcher("resources/signup.jsp").forward(request, response);
        }
        if (action.equalsIgnoreCase("forgot")) {
            request.getRequestDispatcher("resources/forgotPassword.jsp").forward(request, response);
        }
        if (action.equalsIgnoreCase("logout")) {
            request.getSession().invalidate();
            response.sendRedirect(request.getContextPath());
        }
        if (action.equalsIgnoreCase("resetpw")) {
            String token = request.getParameter("token");
            if (token != null) {
                if (daoAc.checkTokens(token)) {
                    request.getRequestDispatcher("resources/resetPassword.jsp").forward(request, response);
                } else {
                    response.sendRedirect("home");
                }
            } else {
                request.getRequestDispatcher("resources/forgotPassword.jsp").forward(request, response);
            }
        }
        if (action.equalsIgnoreCase("sent")) {
            response.getWriter().write("Email sent. Please check your inbox");
        }
        if (action.equalsIgnoreCase("verify")) {
            try {
                String token = request.getParameter("token");

                if (daoAc.checkTokens(token)) {
                    if (daoAc.updateStatus("verified")) {
                        response.sendRedirect("auth?action=login");
                        daoAc.invalidateToken(token);
                    } else {
                        throw new Exception("Update status fail");
                    }
                } else {
                    response.getWriter().write("Token invalid");
                }
            } catch (Exception e) {
                e.printStackTrace();
                response.getWriter().write("Token invalid");
            }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String action = request.getParameter("action");
        if (action == null) {
            response.getWriter().write("Invalid parameter.");
        }

        // ======================= ĐĂNG NHẬP ==================================
        if (action.equalsIgnoreCase("login")) {
            try {
                String email = request.getParameter("email");
                String password = request.getParameter("password");
                Account currentUser = daoAc.login(email, password);
                if (currentUser != null) {
                    HttpSession session = request.getSession();
                    if (currentUser.getRole() == 1) {
                        session.setAttribute("role", "admin");
                        session.setAttribute("AuthenticationUser", currentUser);
                        response.getWriter().write("Success");
                    } else {
                        if (currentUser.getStatus() == -1) {
                            session.invalidate();
                            response.getWriter().write("Blocked!");
                        } else if (currentUser.getStatus() == 0) {
                            session.invalidate();
                            response.getWriter().write("Not verify!");
                        } else {
                            session.setAttribute("role", "user");
                            session.setAttribute("AuthenticationUser", currentUser);
                            response.getWriter().write("Success");
                        }
                    }

                } else {
                    response.getWriter().write("Failed");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // ====================HẾT PHẦN ĐĂNG NHẬP==============================

        // ======================= ĐĂNG KÝ ====================================
        if (action.equalsIgnoreCase("register")) {
            try {
                String name = request.getParameter("yourname");
                String username = request.getParameter("username");
                String email = request.getParameter("email");
                String password = request.getParameter("password");
                String repass = request.getParameter("repass");
                String gRecaptchaResponse = request.getParameter("captchaResponse");
                String agree = request.getParameter("check");
                HttpSession session = request.getSession();

                if (!daoAc.checkEmailExisted(email)) {
                    if (!daoAc.checkUsernameExisted(username)) {
                        if (repass.equals(password) && agree.equals("true")) {
                            if (captcha.verify(gRecaptchaResponse)) {
                                if (daoAc.register(name, username, email, password)) {

                                    // Get the register user's id for storing token to database
                                    int userId = daoAc.getUserIdByEmail(email);
                                    String token = new Utility().generateToken();

                                    // Store token into database
                                    daoAc.insertToken(userId, token, "register");
                                    mail.sendRegistrationMail(email, token, session, username);
                                    response.getWriter().write("Success");
                                    return;
                                } else {
                                    response.getWriter().write("Failed");
                                    return;
                                }
                            } else {
                                response.getWriter().write("Captcha failed!");
                                return;
                            }
                        } else {
                            response.getWriter().write("Failed");
                            return;
                        }
                    } else {
                        response.getWriter().write("Username existed");
                        return;
                    }
                } else {
                    response.getWriter().write("Email existed");
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // ====================HẾT PHẦN ĐĂNG KÝ================================

        // =========================== ĐĂNG XUẤT ==============================
        if (action.equalsIgnoreCase("logout")) {

            request.getSession().invalidate();
            response.sendRedirect(request.getContextPath());

        }
        // ====================HẾT PHẦN ĐĂNG XUẤT==============================

        // ========================= QUÊN MẬT KHẨU ============================
        if (action.equalsIgnoreCase("forgot")) {

            try {

                HttpSession session = request.getSession();
                String forgotEmail = request.getParameter("email");
                session.setAttribute("email", forgotEmail);
                int status = daoAc.getUserStatusByEmail(forgotEmail);

                // If account exists
                if (daoAc.checkEmailExisted(forgotEmail)) {

                    // If user forgets the account already verified
                    if (status == 1) {

                        // Get the register user's id for storing token to database
                        int userId = daoAc.getUserIdByEmail(forgotEmail);
                        String token = new Utility().generateToken();
                        session.setAttribute("token", token);

                        // Store token into database
                        daoAc.insertToken(userId, token, "forgot");
                        mail.sendMailForgotPassword(forgotEmail, session);
                        response.getWriter().write("1");

                        // If user forgets unverified account
                    } else if (status == 0) {
                        response.getWriter().write("0");

                        // If user forgets a banned account
                    } else {
                        response.getWriter().write("-1");
                    }
                    // If account doesn't exists
                } else {
                    response.getWriter().write("-2");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        // ====================HẾT PHẦN QUÊN MẬT KHẨU===========================

        // =========================== ĐẶT LẠI MẬT KHẨU =======================
        if (action.equalsIgnoreCase("resetpw")) {

            try {

                // Get client's parameter
                String token = request.getParameter("token");
                String password = request.getParameter("password");
                String verifyPassword = request.getParameter("verifyPassword");
                if (!daoAc.checkTokens(token)) {
                    response.getWriter().write("0");
                } else {
                    // If password and verify password not equals then return -1
                    if (!password.equals(verifyPassword)) {
                        response.getWriter().write("-1");
                    } else {

                        int accountId = daoAc.getUserIdByToken(token);

                        daoAc.changePassword(accountId, password);
                        daoAc.invalidateToken(token);
                        // Return 1 if everthing works fine
                        response.getWriter().write("1");
                    }
                }
            } catch (Exception e) {

                // Return 0 if there are any error
                response.getWriter().write("0");
                e.printStackTrace();
            }

        }
        // ====================HẾT PHẦN ĐẶT LẠI MẬT KHẨU==============================

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
