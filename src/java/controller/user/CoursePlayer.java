/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user;

import dao.CourseDAO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.CourseTopic;
import model.TopicLesson;

/**
 *
 * @author dai8p
 */
@WebServlet(name = "CoursePlayer", urlPatterns = {"/course/player"})
public class CoursePlayer extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void getLearningView(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        CourseDAO cd = new CourseDAO();
        TopicLesson topicLesson = new TopicLesson();
        try {
            List<CourseTopic> courseTopicList = cd.getAllCourseTopic();

            String lessonName = request.getParameter("lesson");
            if (lessonName == null || cd.getTopicLessonByName(lessonName) == null) {
                response.sendRedirect("/course/player?lesson=Candy+cake+gummi+bears");
            } else {
                topicLesson = cd.getTopicLessonByName(lessonName);
            }

            // Load lesson material and encoding the course follow the URL's encode
            String MaterialLink = topicLesson.getMaterial();

            request.setAttribute("courseTopicList", courseTopicList);

            request.setAttribute("materialLink", MaterialLink);

            request.getRequestDispatcher("/resources/coursePlayer.jsp").forward(request, response);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getLearningView(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
