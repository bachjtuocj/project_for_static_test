/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user;

import dao.CourseDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import model.Course;

/**
 *
 * @author FOR GAMER
 */
@WebServlet(name = "CourseDetail", urlPatterns = {"/course/detail"})
public class CourseDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private HttpSession session = null;
    private final CourseDAO cd = new CourseDAO();

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String courseId = request.getParameter("cid");
            if (courseId == null) {
                response.sendRedirect("/course/detail?cid=1");
            }
            request.setAttribute("courseReviewList", cd.getCourseReviewByCourseId(Integer.parseInt(courseId)));
            Course courseDetail = cd.getCourseById(Integer.parseInt(courseId));
            request.setAttribute("course", courseDetail);
            request.getRequestDispatcher("/resources/courseDetail.jsp").forward(request, response);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String action = request.getParameter("action");
            String content = request.getParameter("content");
            String ratingValue = request.getParameter("ratingValue");

            if (ratingValue == null || ratingValue.equalsIgnoreCase("0")) {
                ratingValue = "3";
            }
            session = request.getSession();

            if (action == null) {
                response.sendRedirect("/course/detail");
            } else if (action.equalsIgnoreCase("addReview")) {
                Account loggedUser = (Account) session.getAttribute("AuthenticationUser");
                String courseId = request.getParameter("cid");
                int userId = -1;
                if (loggedUser != null) {
                    userId = loggedUser.getID();
                } else {
                    userId = 1;
                }
                if (cd.getCourseById(Integer.parseInt(courseId)) != null && !courseId.equalsIgnoreCase("false")) {
                    if (cd.insertCourseReview(userId, Integer.parseInt(courseId), content, Integer.parseInt(ratingValue))) {
                        response.getWriter().write("<div class=\"d-flex align-items-center border-bottom border-separator-light pb-3\">\n"
                                + "    <div class=\"row g-0 w-100\">\n"
                                + "        <div class=\"col-auto\">\n"
                                + "            <div class=\"sw-5 me-3\">\n"
                                + "                <img src=\"" + request.getContextPath() + "/resources/img/profile/profile-1.jpg\" class=\"img-fluid rounded-xl\" alt=\"thumb\">\n"
                                + "            </div>\n"
                                + "        </div>\n"
                                + "        <div class=\"col pe-3\">\n"
                                + "            <div>Cherish Kerr</div>\n"
                                + "            <div class=\"text-muted text-small mb-2\">2 days ago</div>\n"
                                + "            <div class=\"br-wrapper br-theme-cs-icon d-inline-block mb-2\">\n"
                                + "                <div class=\"br-wrapper\">\n"
                                + "                    <select class=\"rating\" name=\"rating\" autocomplete=\"off\" data-readonly=\"true\" data-initial-rating=\"" + ratingValue + "\"\n"
                                + "                        style=\"display: none;\">\n"
                                + "                        <option value=\"1\">1</option>\n"
                                + "                        <option value=\"2\">2</option>\n"
                                + "                        <option value=\"3\">3</option>\n"
                                + "                        <option value=\"4\">4</option>\n"
                                + "                        <option value=\"5\">5</option>\n"
                                + "                    </select>\n"
                                + "                    <div class=\"br-widget br-readonly\">\n"
                                + barratingGenerate(Integer.parseInt(ratingValue))
                                + "                        <div class=\"br-current-rating\">3</div>\n"
                                + "                    </div>\n"
                                + "                </div>\n"
                                + "            </div>\n"
                                + "            <div class=\"text-medium text-alternate lh-1-25\">" + content + "</div>\n"
                                + "        </div>\n"
                                + "    </div>\n"
                                + "</div>");
                    } else {
                        response.getWriter().write("Add review fail");
                    }
                } else {
                    response.getWriter().write("Course do not exist");
                }
            } else {
                response.sendRedirect("/course/detail");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private String barratingGenerate(int number) {
        StringBuilder s = new StringBuilder();
        for (int i = 1; i <= 5; i++) {
            if (i < number) {
                s.append("<a href=\"#\" data-rating-value=\"" + i + "\" data-rating-text=\"" + i + "\" class=\"br-selected\"></a>");
            } else if (i == number) {
                s.append("<a href=\"#\" data-rating-value=\"" + i + "\" data-rating-text=\"" + i + "\" class=\"br-selected br-current\"></a>");
            } else {
                s.append("<a href=\"#\" data-rating-value=\"" + i + "\" data-rating-text=\"" + i + "\"></a>");
            }
        }
        return s.toString();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
