/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user;

import dao.CourseDAO;
import dao.QuizDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Category;
import model.Quiz;
import utility.Utility;

/**
 *
 * @author tddaij
 */
@WebServlet(name = "QuizServlet", urlPatterns = {"/quiz"})
public class QuizServlet extends HttpServlet {

    QuizDAO daoQ = new QuizDAO();
    Utility util = new Utility();
    CourseDAO daoCo = new CourseDAO();
    List<Quiz> listQ = new ArrayList<>();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String action)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = util.getParam("action", "list", request);
        request.setAttribute("action", action);
        if (action.equalsIgnoreCase("list")) {
            listQ = daoQ.getAllQuiz();
            loadListQuiz(request, response);
            request.getRequestDispatcher("/resources/listquiz.jsp").forward(request, response);
        }
        if (action.equalsIgnoreCase("list")) {
            loadListQuiz(request, response);
            request.getRequestDispatcher("/resources/listquiz.jsp").forward(request, response);
        }
    }

    protected void loadListQuiz(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         List<Category> listCategory = daoCo.getAllCategory();
        String page_raw = util.getParam("pageid", "1", request);
            System.out.println(page_raw);
            List<Integer> page = util.page(listQ, page_raw, 10);
            List<Quiz> ListQuizByPage = daoQ.getQuizListByPage(listQ, page.get(1), page.get(4));
                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listQ", ListQuizByPage);
                request.setAttribute("listCategory", listCategory);
            
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String action = request.getParameter("action");
       if (action.equalsIgnoreCase("search")){
           String name = request.getParameter("sname");
           String category = request.getParameter("scategory");
           String level = request.getParameter("slevel");
           listQ = daoQ.searchQuiz(name, category, level);
           loadListQuiz(request, response);
           request.getRequestDispatcher("/resources/listquiz.jsp").forward(request, response);
       }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
