/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user;

import dao.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import utility.Utility;

/**
 *
 * @author tddaij
 */
public class ProfileServlet extends HttpServlet {

    AccountDAO daoAc = new AccountDAO();
    Utility util = new Utility();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("general") || action == null) {
            request.getRequestDispatcher("/resources/profile.jsp").forward(request, response);
        }
        if (action.equalsIgnoreCase("chgpass")) {
            request.getRequestDispatcher("/resources/changepassword.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String action = request.getParameter("action");
        
        // ======================= CHỈNH SỬA THÔNG TIN PROFILE ==================================
        
        if (action.equalsIgnoreCase("general") || action == null) {
            try {
                HttpSession session = request.getSession();
                Account currentUser = (Account) session.getAttribute("AuthenticationUser");
                int id = currentUser.getID();
                String name = request.getParameter("name");
                int gender = Integer.parseInt(request.getParameter("gender"));
                String dob = request.getParameter("dob");
                String phone = request.getParameter("phone");
                String bio = request.getParameter("bio");
                if (daoAc.updateInformation(id, name, gender, dob, phone, bio)) {
                    currentUser.setName(name);
                    currentUser.setGender(gender);
                    currentUser.setDob(dob);
                    currentUser.setPhone(phone);
                    currentUser.setBio(bio);
                    response.getWriter().write("Success");
                } else {
                    response.getWriter().write("Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        // ======================= HẾT PHẦN CHỈNH SỬA THÔNG TIN PROFILE ===========================
        
        // ============================= ĐỔI MẬT KHẨU ================================
        
        if (action.equalsIgnoreCase("chgpass")) {
            try {
                HttpSession session = request.getSession();
                Account currentUser = (Account) session.getAttribute("AuthenticationUser");
                String password = request.getParameter("password");
                String newpass = request.getParameter("newpass");
                String repass = request.getParameter("repass");
                String currentPass = currentUser.getPassword();
                int id = currentUser.getID();
                if (util.passwordDecode(password, currentPass)) {
                    if (repass.equals(newpass)) {
                        String newpassEncoded = util.passwordEncode(newpass);
                        if (daoAc.changePassword(id, newpassEncoded)) {
                            currentUser.setPassword(newpassEncoded);
                            response.getWriter().write("Success");

                        } else {
                            response.getWriter().write("Failed");
                        }
                    } else {
                        response.getWriter().write("Not match");
                    }
                } else {
                    response.getWriter().write("Wrong old pass");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        // ============================= HẾT PHẦN ĐỔI MẬT KHẨU ============================
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
