/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user;

import dao.CourseDAO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import model.Discussion;

/**
 *
 * @author dai8p
 */
@WebServlet(name = "LessonDiscussion", urlPatterns = {"/lesson/discussion"})
public class LessonDiscussion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private HttpSession session = null;
    private final CourseDAO cd = new CourseDAO();

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            List<Discussion> discussionList = cd.getDiscussion();

            request.setAttribute("discussionList", discussionList);

            request.getRequestDispatcher("/resources/lessonDiscussion.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
//        }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        session = request.getSession();
        response.setContentType("text/html;charset=UTF-8");

        try {
            String action = request.getParameter("action");
            if (action.equalsIgnoreCase("discussion")) {
                String content = request.getParameter("content");
                int lessonId = Integer.parseInt(request.getParameter("lessonId"));
                Account currentLoggedAccount = (Account) session.getAttribute("AuthenticationUser");
                int lastDiscussionId = cd.getTheLastestDiscussionId();
                int userId = 0;

                if (currentLoggedAccount == null) {
                    userId = 1;
                } else {
                    userId = currentLoggedAccount.getID();
                }

                // Insert discussion to the database
                if (cd.insertDiscussion(lessonId, content, userId)) {
                    response.getWriter().write("<div class=\"d-flex align-items-center border-top border-separator-light pb-3 pt-3 mt-3\">\n"
                            + "                                        <div class=\"row g-0 w-100\">\n"
                            + "                                            <div class=\"col-auto\">\n"
                            + "                                                <div class=\"sw-5 me-3\">\n"
                            + "                                                    <img src=\"/resources/img/profile/profile-3.jpg\" class=\"img-fluid rounded-xl\" alt=\"thumb\">\n"
                            + "                                                </div>\n"
                            + "                                            </div>\n"
                            + "                                            <div class=\"col pe-3 reply\" id=\"comment-#" + lastDiscussionId + "\" name=\"Kirby Peters\">\n"
                            + "                                                <div> xin đấy" + "</div>\n"
                            + "                                                <div class=\"text-muted text-small mb-2\">3 days ago</div>\n"
                            + "                                                <div class=\"text-alternate lh-1-25 mb-1\">" + content + "</div>\n"
                            + "                                                <a href=\"#\" class=\"text-medium like-btn\">like</a>\n"
                            + "                                                <a href=\"#\" class=\"text-medium reply-btn\">reply</a>\n"
                            + "                                                <div class=\"insertHere\"></div>\n"
                            + "                                            </div>\n"
                            + "                                        </div>\n"
                            + "                                    </div>");
                } else {
                    response.getWriter().write("Add discussion fail");
                }
            } else if (action.equalsIgnoreCase("reply-discussion")) {
                int dicussionId = Integer.parseInt(request.getParameter("commentId").split("#")[1]);
                String content = request.getParameter("replyContent");

                if (cd.insertDiscussionReply(dicussionId, content)) {
                    response.getWriter().write("<div class=\"d-flex align-items-center mt-3\">\n"
                            + "                                                            <div class=\"row g-0 w-100\">\n"
                            + "                                                                <div class=\"col-auto\">\n"
                            + "                                                                    <div class=\"sw-5 me-3\">\n"
                            + "                                                                        <img src=\"/resources/img/profile/profile-3.jpg\" class=\"img-fluid rounded-xl\" alt=\"thumb\">\n"
                            + "                                                                    </div>\n"
                            + "                                                                </div>\n"
                            + "                                                                <div class=\"col pe-3 reply\" id=\"comment-#" + dicussionId + "\" name=\"Cherish Kerr\">\n"
                            + "                                                                    <div>Cherish Kerr</div>\n"
                            + "                                                                    <div class=\"text-muted text-small mb-2\">2 days ago</div>\n"
                            + "                                                                    <div class=\"text-alternate lh-1-25 mb-1\">" + content + "</div>\n"
                            + "                                                                    <a href=\"#\" class=\"text-medium like-btn\">like</a>\n"
                            + "                                                                    <a href=\"#\" class=\"text-medium reply-btn reply-main-btn\">reply</a>\n"
                            + "                                                                    <div class=\"insertReplyHere\"></div>\n"
                            + "                                                                </div>\n"
                            + "                                                            </div>\n"
                            + "                                                        </div>"
                    );
                } else {
                    response.getWriter().write("Add reply-discussion fail");
                }
            } else {
                response.getWriter().write("Discussion error");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
