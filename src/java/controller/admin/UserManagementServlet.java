/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import dao.AdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import utility.Utility;

/**
 *
 * @author tddaij
 */
@WebServlet(name = "UserManagementServlet", urlPatterns = {"/admin/users"})
public class UserManagementServlet extends HttpServlet {

    AdminDAO daoAd = new AdminDAO();
    Utility util = new Utility();
    List<Account> listSearchAccount = new ArrayList<Account>();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String action = request.getParameter("action");
        request.setAttribute("action", action);
        HttpSession session = request.getSession();

        // ====================== LOAD DỮ LIỆU USER ==================================
        try {
            if (action.equalsIgnoreCase("load") || action == null) {
                List<Account> listAccount = daoAd.getListAccount();
                String page_raw = request.getParameter("pageid");
                List<Integer> page = util.page(listAccount, page_raw, 6);

                List<Account> ListAccountByPage = daoAd.getAccountByPage(listAccount, page.get(1), page.get(4));
                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listAccount", ListAccountByPage);

                request.getRequestDispatcher("/resources/adminUserManagement.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // ====================== HẾT PHẦN LOAD DỮ LIỆU USER ==========================
        try {
            if (action.equalsIgnoreCase("edit")) {
                String id = request.getParameter("id");
                Account accountInfo = daoAd.getAccount(id);
                session.setAttribute("accountInfo", accountInfo);
                request.getRequestDispatcher("/resources/userEditInfo.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (action.equalsIgnoreCase("search")) {
                String page_raw = request.getParameter("pageid");
                List<Integer> page = util.page(listSearchAccount, page_raw, 10);
                List<Account> ListAccountByPage = daoAd.getAccountByPage(listSearchAccount, page.get(1), page.get(4));

                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listAccount", ListAccountByPage);
                request.getRequestDispatcher("/resources/adminUserManagement.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (action.equalsIgnoreCase("add")) {
                request.getRequestDispatcher("/resources/adminAddUser.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String action = request.getParameter("action");
        request.setAttribute("action", action);
        // ====================== XOÁ NGƯỜI DÙNG ==================================
        try {
            if (action.equalsIgnoreCase("delete")) {
                String id = request.getParameter("id");
                if (daoAd.deleteAllTokenByUserID(id)) {
                    if (daoAd.deleteAccount(id)) {
                        response.getWriter().write("Success");
                    } else {
                        response.getWriter().write("Failed");
                    }
                } else {
                    response.getWriter().write("Failed");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // ====================== HẾT PHẦN XOÁ NGƯỜI DÙNG =========================

        // ====================== CHỈNH SỬA PROFILE NGƯỜI DÙNG ==================================
        try {
            if (action.equalsIgnoreCase("edit")) {
                String id = request.getParameter("id");
                String username = request.getParameter("username");
                String name = request.getParameter("name");
                String gender = request.getParameter("gender");
                String dob = request.getParameter("dob");
                String phone = request.getParameter("phone");
                String bio = request.getParameter("bio");
                String role = request.getParameter("role");
                String status = request.getParameter("status");
                if (daoAd.updateInformation(id, username, name, gender, dob, phone, bio, role, status)) {
                    response.sendRedirect("/admin/users?action=load");
                } else {
                    response.getWriter().write("Failed");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // ====================== HẾT PHẦN CHỈNH SỬA PROFILE NGƯỜI DÙNG =========================
        // ====================== TÌM KIẾM NGƯỜI DÙNG =========================
        try {
            if (action.equalsIgnoreCase("search")) {
                String sid = request.getParameter("sid");
                String sname = request.getParameter("sname");
                String semail = request.getParameter("semail");
                String sgender = request.getParameter("sgender");
                String sphone = request.getParameter("sphone");
                String srole = request.getParameter("srole");
                String sstatus = request.getParameter("sstatus");

                listSearchAccount = daoAd.searchAccount(sid, sname, semail, sgender, sphone, srole, sstatus);
                String page_raw = request.getParameter("pageid");
                List<Integer> page = util.page(listSearchAccount, page_raw, 10);
                List<Account> ListAccountByPage = daoAd.getAccountByPage(listSearchAccount, page.get(1), page.get(4));

                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listAccount", ListAccountByPage);
                request.getRequestDispatcher("/resources/adminUserManagement.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // ====================== HẾT PHẦN TÌM KIẾM NGƯỜI DÙNG =========================

        // ====================== THÊM NGƯỜI DÙNG ===============================
        if (action.equalsIgnoreCase("add")) {
            try {
                String username = request.getParameter("username");
                String email = request.getParameter("email");
                String password = request.getParameter("password");
                String name = request.getParameter("yourname");
                String gender = request.getParameter("gender");
                String dob = request.getParameter("dob");
                String phone = request.getParameter("phone");
                String bio = request.getParameter("bio");
                String role = request.getParameter("role");
                String status = request.getParameter("status");

                if (daoAd.addAccount(username, email, password, name, gender, dob, phone, bio, role, status)) {
                    response.sendRedirect("/admin/users?action=load");
                } else {
                    response.getWriter().write("Something went wrong!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // ====================== HẾT PHẦN THÊM NGƯỜI DÙNG =========================
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
