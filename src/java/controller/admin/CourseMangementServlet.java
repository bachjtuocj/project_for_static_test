/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import dao.CourseDAO;

//import entity.Account;
import model.Category;
import model.Course;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utility.Utility;

/**
 *
 * @author trinh
 */
@WebServlet(name = "CourseManagement", urlPatterns = {"/admin/course"})
public class CourseMangementServlet extends HttpServlet {

    CourseDAO dao = new CourseDAO();
    Utility util = new Utility();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        String action = request.getParameter("action");
        String page_raw = request.getParameter("pageid");

        try {
            if (action == null) {
                action = "load";
            } else if (page_raw == null || page_raw.equals("0")) {
                page_raw = "1";
            }
            if (action.equalsIgnoreCase("load")) {
                CourseDAO dao = new CourseDAO();
                List<Course> listAllCourse = dao.getAllCourse();
                List<Category> listAllCategory = dao.getAllCategory();

                List<Integer> page = util.page(listAllCourse, page_raw, 10);
                List<Course> ListCourseByPage = dao.getCourseByPage(listAllCourse, page.get(1), page.get(4));
                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listCourse", ListCourseByPage);
                request.setAttribute("listP", listAllCourse);
                request.setAttribute("listCC", listAllCategory);

                request.getRequestDispatcher("/resources/courseManagement.jsp").forward(request, response);
            }
//        -----------------------Load course to website-------------------------------------------------
            if (action.equalsIgnoreCase("edit")) {
                List<Category> listC = dao.getAllCategory();
                int id = Integer.parseInt(request.getParameter("pid"));
                if (String.valueOf(id) == null || String.valueOf(id).equals("0")) {
                    response.sendRedirect("/admin/course");
                }
                Course p = dao.getCourseById(id);
                request.setAttribute("detail", p);
                request.setAttribute("listCC", listC);

                request.setAttribute("action", action);
                request.getRequestDispatcher("/resources/editCourse.jsp").forward(request, response);
            }
//        --------------------------Delete a course-------------------------------------------------------
            if (action.equalsIgnoreCase("delete")) {
                String pid = request.getParameter("pid");
                dao.deleteCourse(pid);
                response.sendRedirect("/admin/course");
            }
            if (action.equalsIgnoreCase("loadaddCourse")) {
                List<Category> listC = dao.getAllCategory();
                request.setAttribute("listCC", listC);

                request.setAttribute("action", action);
                request.getRequestDispatcher("/resources/addCourse.jsp").forward(request, response);
            }

//         -----------------------------------Search somthing-------------------------------------------------
            if (action.equalsIgnoreCase("searchCourse")) {
                String query = request.getParameter("query").replaceAll("\\s+", " ").trim();
                List<Course> listSearchCourse = dao.searchCourse(query);

                List<Integer> page = util.page(listSearchCourse, page_raw, 10);
                List<Course> ListCourseByPage = dao.getCourseByPage(listSearchCourse, page.get(1), page.get(4));
                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listCourse", ListCourseByPage);

                request.setAttribute("action", action);
                request.getRequestDispatcher("/resources/courseManagement.jsp").forward(request, response);
            }

//         -----------------------------------Sort course by ID-------------------------------------------------
            if (action.equalsIgnoreCase("sortID")) {

                List<Course> orderCourseID = dao.getCourseOrderBy(action);

                List<Integer> page = util.page(orderCourseID, page_raw, 10);
                List<Course> ListCourseByPage = dao.getCourseByPage(orderCourseID, page.get(1), page.get(4));
                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listCourse", ListCourseByPage);

                request.setAttribute("action", action);
                request.getRequestDispatcher("/resources/courseManagement.jsp").forward(request, response);
            }

//         -----------------------------------Sort course by Name-------------------------------------------------
            if (action.equalsIgnoreCase("sortName")) {

                List<Course> orderCourseName = dao.getCourseOrderBy(action);

                List<Integer> page = util.page(orderCourseName, page_raw, 10);
                List<Course> ListCourseByPage = dao.getCourseByPage(orderCourseName, page.get(1), page.get(4));
                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listCourse", ListCourseByPage);

                request.setAttribute("action", action);
                request.getRequestDispatcher("/resources/courseManagement.jsp").forward(request, response);
            }

//         -----------------------------------Sort course by Price-------------------------------------------------
            if (action.equalsIgnoreCase("sortPrice")) {
                List<Course> orderCoursePrice = dao.getCourseOrderBy(action);

                List<Integer> page = util.page(orderCoursePrice, page_raw, 10);
                List<Course> ListCourseByPage = dao.getCourseByPage(orderCoursePrice, page.get(1), page.get(4));
                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listCourse", ListCourseByPage);

                request.setAttribute("action", action);
                request.getRequestDispatcher("/resources/courseManagement.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        String action = request.getParameter("action");
//        ---------------------------------Add a course-------------------------------------------------
        try {
            if (action == null) {
                action = "add";
            }
//         -----------------------------------Search somthing-------------------------------------------------
            if (action.equalsIgnoreCase("searchCourse")) {
                String query = request.getParameter("query").replaceAll("\\s+", " ").trim();
                List<Course> listSearchCourse = dao.searchCourse(query);

                String page_raw = request.getParameter("pageid");
                if (page_raw == null || page_raw == "0") {
                    page_raw = "1";
                }
                List<Integer> page = util.page(listSearchCourse, page_raw, 10);
                List<Course> ListCourseByPage = dao.getCourseByPage(listSearchCourse, page.get(1), page.get(4));
                request.setAttribute("page", page.get(0));
                request.setAttribute("pagenext", page.get(3));
                request.setAttribute("pageprev", page.get(2));
                request.setAttribute("num", page.get(5));
                request.setAttribute("listCourse", ListCourseByPage);

                request.setAttribute("action", action);
                request.getRequestDispatcher("/resources/courseManagement.jsp").forward(request, response);
            }
            if (action.equalsIgnoreCase("add")) {
                String thumbnail = request.getParameter("thumbnail");
                String name = request.getParameter("name");
                String price = request.getParameter("price");
                String des = request.getParameter("description");
                String content = request.getParameter("content");
                String level = request.getParameter("level");
                String duration = request.getParameter("duration");
                String cate = request.getParameter("category");
                Course a = dao.checkAccountExist(name);
                boolean checkPrice = dao.checkPrice(price);
                if (a == null && checkPrice == true) {
                    dao.insert(name, des, thumbnail, price, content, level, duration, cate);
                    response.sendRedirect("/admin/course");
                } else {
                    List<Category> listC = dao.getAllCategory();
                    request.setAttribute("listCC", listC);
                    request.setAttribute("alert", "Course name cannot duplicate and price must be more than 0 ! Please re-enter !");

//                    request.setAttribute("action", action);
                    request.getRequestDispatcher("/resources/addCourse.jsp").forward(request, response);
                }

            }

//          ----------------------------------Edit a course----------------------------------------------
            if (action.equalsIgnoreCase("edit")) {
                String pid = request.getParameter("id");
                String thumbnail = request.getParameter("thumbnail");
                String name = request.getParameter("name");
                String price = request.getParameter("price");
                String des = request.getParameter("description");
                String content = request.getParameter("content");
                String level = request.getParameter("level");
                String duration = request.getParameter("duration");
                String cate = request.getParameter("category");
                CourseDAO dao = new CourseDAO();
                dao.update(name, des, thumbnail, price, content, level, duration, cate, pid);
                response.sendRedirect("/admin/course");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
