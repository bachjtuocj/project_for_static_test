<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en" data-footer="true">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title>Quản lý người dùng</title>
  <meta name="description"
    content="Home screen that contains stats, charts, call to action buttons and various listing elements." />
  <!-- Favicon Tags Start -->

  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-152x152.png" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-196x196.png" sizes="196x196" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-96x96.png" sizes="96x96" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-16x16.png" sizes="16x16" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-128.png" sizes="128x128" />
  <meta name="application-name" content="&nbsp;" />
  <meta name="msapplication-TileColor" content="#FFFFFF" />
  <meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-144x144.png" />
  <meta name="msapplication-square70x70logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-70x70.png" />
  <meta name="msapplication-square150x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-150x150.png" />
  <meta name="msapplication-wide310x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x150.png" />
  <meta name="msapplication-square310x310logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x310.png" />
  <!-- Favicon Tags End -->
  <!-- Font Tags Start -->
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font/CS-Interface/style.css" />
  <!-- Font Tags End -->
  <!-- Vendor Styles Start -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/bootstrap.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/OverlayScrollbars.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/glide.core.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/glide.core.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/introjs.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/select2.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/select2-bootstrap4.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/plyr.css" />
  <!-- Vendor Styles End -->
  <!-- Template Base Styles Start -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" />
  <!-- Template Base Styles End -->

  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
  <script src="${pageContext.request.contextPath}/resources/js/base/loader.js"></script>
</head>

<body>
  <%@include file="adminHeader.jsp" %>

    <main>
      <div class="container">
        <!-- Title and Top Buttons Start -->
        <div class="page-title-container">
          <div class="row">
            <!-- Title Start -->
            <div class="col-12 col-sm-6">
              <h1 class="mb-0 pb-0 display-4" id="title">Quản lý thành viên</h1>
              <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
                <ul class="breadcrumb pt-0">
                  <li class="breadcrumb-item"><a href="./">Bảng quản trị</a></li>
                  <li class="breadcrumb-item">Quản lý thành viên</li>
                </ul>
              </nav>
            </div>
            <!-- Title End -->

            <!-- Top Buttons Start -->
         <div class="col-12 col-sm-6">
                <button onclick="location.href='${pageContext.request.contextPath}/admin/users?action=add';" type="button" class="btn btn-outline-primary btn-icon btn-icon-start ms-0 ms-sm- w-100 w-md-auto" id="newContactButton">
                  <i data-cs-icon="plus"></i>
                  <span>Thêm người dùng</span>
                </button>
                
                
              </div>
            <!-- Top Buttons End -->
          </div>
        </div>
        <!-- Title and Top Buttons End -->
        <!-- Modal for add a member -->
       
      </div>

      <!-- End model for add a member -->
     
      <div class="row">
        <div class="col-12 col-xl-12">
          <!-- Sales & Stocks Charts Start -->
          <section class="scroll-section" id="basic">
            <h2 class="big-title">Danh sách thành viên</h2>
            <div class="card mb-5">
              <div class="card-body">
                  <form action="${pageContext.request.contextPath}/admin/users" method="post">
                  <div style="display: flex" class="autocomplete-container">
                      <input type ="hidden" name ="action" value ="search">
                    <input type="text" name="sid" class="form-control" autocomplete="off" placeholder="ID"/>&nbsp;
                    <input type="text" name="sname" class="form-control" autocomplete="off" placeholder="Tên" />&nbsp;
                    <input type="text" name="semail" class="form-control" autocomplete="off" placeholder="Email" />&nbsp;
                    <input type="text" name="phone" class="form-control" autocomplete="off" placeholder="Điện thoại" />&nbsp;
                    <select name="sgender" class="form-select">
                            <option value="" selected="">Giới tính</option>
                            <option value="0">Nam</option>
                            <option value="1">Nữ</option>
                            <option value="2">Khác</option>
                          </select> &nbsp;
                    <select name="srole" class="form-select">
                            <option value=""selected="">Vai trò</option>
                            <option value="0">User</option>
                            <option value="1">Admin</option>
                     
                          </select> &nbsp;
                    <select name="sstatus" class="form-select">
                            <option value="" selected="">Trạng thái</option>
                            <option value="0">Chưa xác nhận</option>
                            <option value="1">Đã xác nhận</option>
                            <option value="-1">Đã khoá</option>
                     
                          </select> &nbsp;
                     <button style="" class="btn btn-icon btn-icon-end btn-primary mb-1" id="searchButton" type="submit">
                          <span>Tìm kiếm</span>
                          <i data-cs-icon="arrow-bottom-right"></i>
                        </button>
                
                  </div>
                </form>
                <div class="clearfix visible-sm-block"> 
                    <br></br>
                </div>
            
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Tên</th>
                      <th scope="col">Email</th>
                      <th scope="col">Giới tính</th>
                      <th scope="col">Ngày sinh</th>
                      <th scope="col">Điện thoại</th>
                      <th scope="col">Vài trò</th>
                      <th scope="col">Trạng thái</th>
                      <th scope="col">Ngày tạo</th>
                      <th scope="col">Hành động</th>
                    
                    </tr>
                  </thead>
                  <tbody>
                      <c:forEach items="${listAccount}" var="o">
                    <tr id="${o.ID}">
                      <th scope="row">${o.ID}</th>
                      <td>${o.name}</td>
                      <td>${o.email}</td>
                      <c:if test="${o.gender == 0}"><td>Nam</td></c:if>
                      <c:if test="${o.gender == 1}"><td>Nữ</td></c:if>
                      <c:if test="${o.gender == 2}"><td>Khác</td></c:if>
                      <td>${o.dob}</td>
                      <td>${o.phone}</td>
                      <c:if test="${o.role == 0}"><td>User</td></c:if>
                      <c:if test="${o.role == 1}"><td>Admin</td></c:if>
                      <c:if test="${o.status == 0}"><td>Chưa xác nhận</td></c:if>
                      <c:if test="${o.status == 1}"><td>Đã xác nhận</td></c:if>
                      <c:if test="${o.status == -1}"><td>Đã bị khoá</td></c:if>
                      <td>${o.created}</td>
                      <td>
                          <button class="btn btn-icon btn-icon-end btn-primary mb-1" onclick="location.href='users?action=edit&id=${o.ID}';" type="button">
                          <span>Edit</span>
                          <i data-cs-icon="arrow-bottom-right"></i>
                        </button>
                          
                          <button  class="btn btn-icon btn-icon-end btn-primary mb-1" type="button" onclick="deleteUser(${o.ID});" >
                          <span>Delete</span>
                          <i data-cs-icon="bin"></i>
                        </button>
                           
                      </td>
                     
                    </tr>
                    </c:forEach>
                   
                  </tbody>
                </table>
              </div>
            </div>
          </section>
          <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
                  <c:if test="${page!=1}">
              <li class="page-item">
                <a class="page-link" href="./users?action=${action}&pageid=1" aria-label="First">
                  <i data-cs-icon="arrow-double-left"></i>
                </a>
              </li>
              <li class="page-item">
                
                <a class="page-link" href="./users?action=${action}&pageid=${pageprev}" aria-label="Trang trước">
                  <i data-cs-icon="chevron-left"></i>
                </a>
                 
              </li>
                </c:if>
               <c:forEach begin="${1}" end="${num}" var="i">
              <li class="page-item ${i==page?"active":""}"><a class="page-link" href="./users?action=${action}&pageid=${i}">${i}</a></li>
               </c:forEach>
              <li class="page-item">
                  <c:if test="${page!=num}">
                <a class="page-link" href="./users?action=${action}&pageid=${pagenext}" aria-label="Trang sau">
                  <i data-cs-icon="chevron-right"></i>
                </a>
        
              </li>
               <li class="page-item">
                <a class="page-link" href="./users?action=${action}&pageid=${num}" aria-label="Last">
                  <i data-cs-icon="arrow-double-right"></i>
                </a>
              </li>
                </c:if>
            </ul>
          </nav>
                  </div>
                      
                  </div>
    </main>



  

  <script>
      
      function deleteUser(id){
    if (confirm('Bạn chắc chắn muốn xoá người dùng này?')) {
   const formValues = {
                    action: "delete",
                    id: id,
                };
 
                $.ajax({
                    url: "users",
                    type: "POST",
                    data: formValues,
                    success: function (response) {
                        if (response == "Success") {
                           location.reload();
                        } else {
                          location.reload();
                        }
                    }, error: function(xhr){
                        console.log(xhr);
                    }

                });  
} else {
  console.log('Huỷ xoá');
}
       
      }
      
  </script>
  <!-- Vendor Scripts Start -->
  <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery-3.5.1.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap.bundle.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/OverlayScrollbars.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/autoComplete.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/clamp.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/Chart.bundle.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/chartjs-plugin-datalabels.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/chartjs-plugin-rounded-bar.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/glide.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/intro.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/select2.full.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/plyr.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/cs/scrollspy.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap-submenu.js"></script>
  <!-- Vendor Scripts End -->

  <!-- Template Base Scripts Start -->
  <script src="${pageContext.request.contextPath}/resources/font/CS-Line/csicons.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/helpers.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/globals.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/nav.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/search.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/settings.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/init.js"></script>
  <!-- Template Base Scripts End -->
  <!-- Page Specific Scripts Start -->
  <script src="${pageContext.request.contextPath}/resources/js/cs/glide.custom.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/cs/charts.extend.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/pages/dashboard.default.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/common.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/cs/autocomplete.custom.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/forms/controls.autocomplete.js"></script>
  <!-- Page Specific Scripts End -->
</body>

</html>