<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


  <div id="root">
    <div id="nav" class="nav-container d-flex">
      <div class="nav-content d-flex">
        <!-- Logo Start -->
        <div class="logo position-relative">
          <a href="${pageContext.request.contextPath}/home">
            <!-- Logo can be added directly -->
            <!-- <img src="img/logo/logo-white.svg" alt="logo" /> -->

            <!-- Or added via css to provide different ones for different color themes -->
            <div class="img"></div>
          </a>
        </div>
        <!-- Logo End -->

        <!-- Language Switch Start -->
        
        <!-- Language Switch End -->

        <!-- User Menu Start -->
        <div class="user-container d-flex">
          <a href="#" class="d-flex user position-relative" data-bs-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <img class="profile" alt="profile" src="${pageContext.request.contextPath}/resources/img/profile/${sessionScope.AuthenticationUser.avatar}" />
            <div class="name">${sessionScope.AuthenticationUser.name}</div>
          </a>
          <div class="dropdown-menu dropdown-menu-end user-menu wide">
            <div class="row mb-3 ms-0 me-0">
              <div class="col-12 ps-1 mb-2">
                <div class="text-extra-small text-primary">TÀI KHOẢN</div>
              </div>
              <div class="col-6 ps-1 pe-1">
                <ul class="list-unstyled">
                  <li>
                    <a href="${pageContext.request.contextPath}/profile?action=general">Thông tin</a>
                  </li>
                 
                </ul>
              </div>
             
            </div>
            <div class="row mb-1 ms-0 me-0">
              <div class="col-12 p-1 mb-2 pt-2">
                <div class="text-extra-small text-primary">KHOÁ HỌC</div>
              </div>
              <div class="col-6 ps-1 pe-1">
                <ul class="list-unstyled">
                  <li>
                    <a href="#">Đang học</a>
                  </li>
                  <li>
                    <a href="#">Chứng chỉ</a>
                  </li>
                </ul>
              </div>
              <div class="col-6 pe-1 ps-1">
                <ul class="list-unstyled">
                  <li>
                    <a href="#">Wishlist</a>
                  </li>
                  <li>
                    <a href="#">Đã học</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="row mb-1 ms-0 me-0">
              <div class="col-12 p-1 mb-3 pt-3">
                <div class="separator-light"></div>
              </div>
              <div class="col-6 ps-1 pe-1">
                <ul class="list-unstyled">
                  <li>
                    <a href="#">
                      <i data-cs-icon="help" class="me-2" data-cs-size="17"></i>
                      <span class="align-middle">Trợ giúp</span>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i data-cs-icon="file-text" class="me-2" data-cs-size="17"></i>
                      <span class="align-middle">Tài liệu</span>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-6 pe-1 ps-1">
                <ul class="list-unstyled">
                  <li>
                    <a href="#">
                      <i data-cs-icon="gear" class="me-2" data-cs-size="17"></i>
                      <span class="align-middle">Cài đặt</span>
                    </a>
                  </li>
                  <li>
                    <a href="${pageContext.request.contextPath}/auth?action=logout">
                      <i data-cs-icon="logout" class="me-2" data-cs-size="17"></i>
                      <span class="align-middle">Logout</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- User Menu End -->

        <!-- Icons Menu Start -->
        <ul class="list-unstyled list-inline text-center menu-icons">
          
          <li class="list-inline-item">
            <a href="#" id="pinButton" class="pin-button">
              <i data-cs-icon="lock-on" class="unpin" data-cs-size="18"></i>
              <i data-cs-icon="lock-off" class="pin" data-cs-size="18"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a href="#" id="colorButton">
              <i data-cs-icon="light-on" class="light" data-cs-size="18"></i>
              <i data-cs-icon="light-off" class="dark" data-cs-size="18"></i>
            </a>
          </li>
       
        </ul>
        <!-- Icons Menu End -->

        <!-- Menu Start -->
        <div class="menu-container flex-grow-1">
          <ul id="menu" class="menu">
            <li>
              <a href="../admin" data-href="#admin">
                <i data-cs-icon="home" class="icon" data-cs-size="18"></i>
                <span class="label">Bảng quản trị</span>
              </a>
              
            </li>
            <li>
              <a href="#course" data-href="#course">
                <i data-cs-icon="screen" class="icon" data-cs-size="18"></i>
                <span class="label">Khoá học</span>
              </a>
              <ul id="apps">
                <li>
                  <a href="#coursemanagement">
                    <span class="label">Quản lý khoá học</span>
                  </a>
                </li>
               
              </ul>
            </li>
            <li>
              <a href="#user" data-href="#users">
                <i data-cs-icon="user" class="icon" data-cs-size="18"></i>
                <span class="label">Thành viên</span>
              </a>
              <ul id="pages">
                <li>
                  <a href="${pageContext.request.contextPath}/admin/users?action=load" data-href="#users">
                    <span class="label">Quản lý thành viên</span>
                  </a>
                  
                </li>
                 </ul>
            </li>
                
         
        </div>
        <!-- Menu End -->

        <!-- Mobile Buttons Start -->
        <div class="mobile-buttons-container">
          <!-- Scrollspy Mobile Button Start -->
          <a href="#" id="scrollSpyButton" class="spy-button" data-bs-toggle="dropdown">
            <i data-cs-icon="menu-dropdown"></i>
          </a>
          <!-- Scrollspy Mobile Button End -->

          <!-- Scrollspy Mobile Dropdown Start -->
          <div class="dropdown-menu dropdown-menu-end" id="scrollSpyDropdown"></div>
          <!-- Scrollspy Mobile Dropdown End -->

          <!-- Menu Button Start -->
          <a href="#" id="mobileMenuButton" class="menu-button">
            <i data-cs-icon="menu"></i>
          </a>
          <!-- Menu Button End -->
        </div>
        <!-- Mobile Buttons End -->
      </div>
      <div class="nav-shadow"></div>
    </div>

 
    <!-- Layout Footer Start -->
    <footer>
      <div class="footer-content">
        <div class="container">
          <div class="row">
            <div class="col-12 col-sm-6">
              <p class="mb-0 text-muted text-medium">Acorn 2021</p>
            </div>
            <div class="col-sm-6 d-none d-sm-block">
              <ul class="breadcrumb pt-0 pe-0 mb-0 float-end">
                <li class="breadcrumb-item mb-0 text-medium">
                  <a href="#" target="_blank" class="btn-link">Facebook</a>
                </li>
                <li class="breadcrumb-item mb-0 text-medium">
                  <a href="#" target="_blank" class="btn-link">Terms</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- Layout Footer End -->
  </div>

  
