<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en" data-footer="true">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title>Acorn Admin Template | Default Dashboard</title>
  <meta name="description"
    content="Home screen that contains stats, charts, call to action buttons and various listing elements." />
  <!-- Favicon Tags Start -->
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-152x152.png" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-196x196.png" sizes="196x196" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-96x96.png" sizes="96x96" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-16x16.png" sizes="16x16" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-128.png" sizes="128x128" />
  <meta name="application-name" content="&nbsp;" />
  <meta name="msapplication-TileColor" content="#FFFFFF" />
  <meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-144x144.png" />
  <meta name="msapplication-square70x70logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-70x70.png" />
  <meta name="msapplication-square150x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-150x150.png" />
  <meta name="msapplication-wide310x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x150.png" />
  <meta name="msapplication-square310x310logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x310.png" />
  <!-- Favicon Tags End -->
  <!-- Font Tags Start -->
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font/CS-Interface/style.css" />
  <!-- Font Tags End -->
  <!-- Vendor Styles Start -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/bootstrap.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/OverlayScrollbars.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/glide.core.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/glide.core.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/introjs.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/select2.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/select2-bootstrap4.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/plyr.css" />
  <!-- Vendor Styles End -->
  <!-- Template Base Styles Start -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" />
  <!-- Template Base Styles End -->

  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
  <script src="${pageContext.request.contextPath}/resources/js/base/loader.js"></script>
</head>
      <body>
          <!-- Contents for edit button -->
          <div class="modal " id="edits" style="display: block;" >
            <div class="modal-dialog modal-lg" >
              <div class="modal-content " >
                <div class="modal-body">
                  <div class="scroll-track-visible">
                    <section class="scroll-section" id="personal">
                      <h2 class="small-title">THÊM KHÓA HỌC</h2>
                      <form action="${pageContext.request.contextPath}/admin/course?action=add" method="post"  id="personalForm" novalidate>
                        <div class="card-body"> 
                            <div class="row g-3">
                              <div class="col-md-12">
                                <label class="mb-3 top-label">
                                    <input class="form-control" name="name"  />
                                  <span>TÊN KHÓA HỌC</span>
                                </label>
                                  <p   class="text-danger">${alert}</p>
                              </div>
                                
                            <div class="col-md-12">
                              <label class="mb-3 top-label">
                                  <input class="form-control" name="price" type="number" min="0.0" max="10000.0" pattern="[0-9]{2,4}" required=""  />
                                <span>GIÁ</span>
                              </label>
                                
                            </div>
                          </div>
                          <div class="row g-3">
                            <div class="col-md-12">
                              <label class="mb-3 top-label">
                                <input class="form-control" name="description"  />
                                <span>MÔ TẢ</span>
                              </label>
                            </div>
                            <div class="col-md-12">
                              <label class="mb-3 top-label">
                                <input class="form-control" name="content"  />
                                <span>NỘI DUNG</span>
                              </label>
                            </div>
                          </div>
                          <div class="row g-3">
                            <div class="col-md-12">
                              <label class="mb-3 top-label">
                                  <input class="form-control" type="number" name="level"  />
                                <span>MỨC ĐỘ</span>
                              </label>
                            </div>
                            <div class="col-md-12">
                              <label class="mb-3 top-label">
                                <input class="form-control" name="thumbnail"  />
                                <span>VIDEO GIỚI THIỆU</span>
                              </label>
                            </div>
                          </div>
                          <div class="row g-3">
                              <div class="col-md-12">
                                  <label class="mb-3 top-label">
                                      <input class="form-control" type="number" name="duration" required   />
                                      <span>THỜI GIAN HỌC</span>
                                  </label>
                              </div> 
                              <div class="col-md-12">
                                  <label class="mb-3 top-label">
                                      <select name="category" class="form-select" aria-label="Default select example">
                                    <c:forEach items="${listCC}" var="o">
                                        <option value="${o.cid}">${o.cname}</option>
                                    </c:forEach>
                                </select>
                                  </label>
                              </div>
                          </div>      
                        </div>
                        <div class=" border-0 pt-0 d-flex justify-content-end align-items-center">
                          <div>
                            <button class="btn btn-icon btn-icon-end btn-outline-primary " type="submit">
                            <span><a href="" onclick="return submitform()">Thêm</a></span>
                            <i data-cs-icon="chevron-right"></i>
                            </button>
                          </div>
                        </div>
                  </form>
                  </section> 
              </div>
            </div>
    </main>
  </div>  
     <script>
     function submitform() {
  // Get first form element
  var $form = $('form')[0];

  // Check if valid using HTML5 checkValidity() builtin function
  if ($form.checkValidity()) {
    
    $form.submit();
  } 
  
  else {
    window.alert('Please enter your information');
  }
  
  return false
}
</script>      


  <!-- Vendor Scripts Start -->
  <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery-3.5.1.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap.bundle.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/OverlayScrollbars.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/autoComplete.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/clamp.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/Chart.bundle.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/chartjs-plugin-datalabels.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/chartjs-plugin-rounded-bar.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/glide.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/intro.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/select2.full.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/plyr.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/cs/scrollspy.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap-submenu.js"></script>
  <!-- Vendor Scripts End -->

  <!-- Template Base Scripts Start -->
  <script src="${pageContext.request.contextPath}/resources/font/CS-Line/csicons.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/helpers.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/globals.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/nav.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/search.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/settings.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/base/init.js"></script>
  <!-- Template Base Scripts End -->
  <!-- Page Specific Scripts Start -->
  <script src="${pageContext.request.contextPath}/resources/js/cs/glide.custom.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/cs/charts.extend.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/pages/dashboard.default.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/common.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/cs/autocomplete.custom.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/forms/controls.autocomplete.js"></script>
  <!-- Page Specific Scripts End -->
    </body>
</html>