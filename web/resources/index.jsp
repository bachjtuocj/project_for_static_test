<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en" data-footer="true" data-override='{"attributes":{"layout": "boxed"}}'>
    <c:set var="context" value="${pageContext.request.contextPath}"/>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>Trang chủ</title>
        <meta name="description" content="Home" />
        <!-- Favicon Tags Start -->

        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${context}/resources/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${context}/resources/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${context}/resources/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${context}/resources/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="${context}/resources/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="${context}/resources/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="${context}/resources/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="${context}/resources/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="${context}/resources/img/favicon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="${context}/resources/img/favicon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="${context}/resources/img/favicon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="${context}/resources/img/favicon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="${context}/resources/img/favicon/mstile-310x310.png" />
        <!-- Favicon Tags End -->
        <!-- Font Tags Start -->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="${context}/resources/font/CS-Interface/style.css" />
        <!-- Font Tags End -->
        <!-- Vendor Styles Start -->
        <link rel="stylesheet" href="${context}/resources/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="${context}/resources/css/vendor/OverlayScrollbars.min.css" />
        <link rel="stylesheet" href="${context}/resources/css/vendor/glide.core.min.css" />
        <!-- Vendor Styles End -->
        <!-- Template Base Styles Start -->
        <link rel="stylesheet" href="${context}/resources/css/styles.css" />
        <!-- Template Base Styles End -->

        <link rel="stylesheet" href="${context}/resources/css/main.css" />
        <script src="${context}/resources/js/base/loader.js"></script>
    </head>

    <body>
        <%@include file="header.jsp" %>


        <main>
            <div class="container">
                <!-- Title Start -->
                <!-- <div class="row">
                  <div class="col-12">
                    <div class="page-title-container">
                      <h1 class="mb-0 pb-0 display-4" id="title">Blog Home</h1>
                      <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
                        <ul class="breadcrumb pt-0">
                          <li class="breadcrumb-item"><a href="Dashboards.Default.html">Home</a></li>
                          <li class="breadcrumb-item"><a href="Pages.html">Pages</a></li>
                          <li class="breadcrumb-item"><a href="Pages.Blog.html">Blog</a></li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div> -->
                <!-- Title End -->

                <!-- Top Stories Start -->
                <!--          <h2 class="small-title">Top Stories</h2>-->
                <div class="row g-4 mb-5">
                    <div class="col-12 col-lg-12 col-xxl-12 ">
                        <div class="card h-150">
                            <img src="${context}/resources/img/product/large/hands.jpg" class="card-img sh-40" alt="card image" />

                            <!--                <div class="card-body pb-0">-->
                            <!--                  <a href="Pages.Blog.Detail.html" class="h5 heading body-link stretched-link">-->
                            <!--                    <div class="mb-0 lh-1-5 sh-8 sh-md-6 clamp-line" data-line="2">A Complete Guide to Mix Dough for the Molds</div>-->
                            <!--                  </a>-->
                            <!--                </div>-->
                            <!--                <div class="card-footer border-0 pt-0">-->
                            <!--                  <div class="row g-0">-->
                            <!--                    <div class="col-auto pe-3">-->
                            <!--                      <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>-->
                            <!--                      <span class="align-middle">5</span>-->
                            <!--                    </div>-->
                            <!--                    <div class="col">-->
                            <!--                      <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>-->
                            <!--                      <span class="align-middle">20 Min</span>-->
                            <!--                    </div>-->
                            <!--                  </div>-->
                            <!--                </div>-->
                        </div>
                    </div>
                    <div class="container text-center w-80 " style="height: 120px">
                        <h1>Trending Course</h1>

                        <p> Trending courseeeeeeeeeeeeee </p>

                    </div>
                    <div class="col-sm-6 col-lg-3 col-xxl-3 sh-40">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/pane-sciocco.jpg" class="card-img-top sh-22" alt="card image" />
                            <div class="card-body pb-0">
                                <a href="#" class="h5 heading body-link stretched-link">
                                    <div class="mb-0 lh-1-5 sh-8 sh-md-6 clamp-line" data-line="2">Course 1</div>
                                </a>
                            </div>
                            <div class="card-footer border-0 pt-0">
                                <div class="row g-0">
                                    <div class="col-auto pe-3">
                                        <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">148</span>
                                    </div>
                                    <div class="col">
                                        <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">10 Min</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 col-xxl-3 sh-40">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/rugbraud.jpg" class="card-img-top sh-22" alt="card image" />
                            <div class="card-body pb-0">
                                <a href="#" class="h5 heading body-link stretched-link">
                                    <div class="mb-0 lh-1-5 sh-8 sh-md-6 clamp-line" data-line="2">Course 2</div>
                                </a>
                            </div>
                            <div class="card-footer border-0 pt-0">
                                <div class="row g-0">
                                    <div class="col-auto pe-3">
                                        <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">19</span>
                                    </div>
                                    <div class="col">
                                        <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">30 Min</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 col-xxl-3 sh-40">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/boule.jpg" class="card-img-top sh-22" alt="card image" />
                            <div class="card-body pb-0">
                                <a href="#" class="h5 heading body-link stretched-link">
                                    <div class="mb-0 lh-1-5 sh-8 sh-md-6 clamp-line" data-line="2">Course 3</div>
                                </a>
                            </div>
                            <div class="card-footer border-0 pt-0">
                                <div class="row g-0">
                                    <div class="col-auto pe-3">
                                        <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">42</span>
                                    </div>
                                    <div class="col">
                                        <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">15 Min</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 col-xxl-3 sh-40">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/pain-de-campagne.jpg" class="card-img-top sh-22" alt="card image" />
                            <div class="card-body pb-0">
                                <a href="#" class="h5 heading body-link stretched-link">
                                    <div class="mb-0 lh-1-5 sh-8 sh-md-6 clamp-line" data-line="2">Course 4</div>
                                </a>
                            </div>
                            <div class="card-footer border-0 pt-0">
                                <div class="row g-0">
                                    <div class="col-auto pe-3">
                                        <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">241</span>
                                    </div>
                                    <div class="col">
                                        <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">25 Min</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 col-xxl-3 sh-40">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/michetta.jpg" class="card-img-top sh-22" alt="card image" />
                            <div class="card-body pb-0">
                                <a href="#" class="h5 heading body-link stretched-link">
                                    <div class="mb-0 lh-1-5 sh-8 sh-md-6 clamp-line" data-line="2">Course 5</div>
                                </a>
                            </div>
                            <div class="card-footer border-0 pt-0">
                                <div class="row g-0">
                                    <div class="col-auto pe-3">
                                        <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">194</span>
                                    </div>
                                    <div class="col">
                                        <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">40 Min</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 col-xxl-3 sh-40">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/cornbread.jpg" class="card-img-top sh-22" alt="card image" />
                            <div class="card-body pb-0">
                                <a href="#" class="h5 heading body-link stretched-link">
                                    <div class="mb-0 lh-1-5 sh-8 sh-md-6 clamp-line" data-line="2">Course 6</div>
                                </a>
                            </div>
                            <div class="card-footer border-0 pt-0">
                                <div class="row g-0">
                                    <div class="col-auto pe-3">
                                        <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">67</span>
                                    </div>
                                    <div class="col">
                                        <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                        <span class="align-middle">10 Min</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Top Stories End -->




                <!-- Popular Articles Start -->
                <div class="row">
                    <div class="col-12 mb-5">
                        <h2 class="small-title">Feedback</h2>
                        <div class="glide glide-blog">
                            <div class="glide__track" data-glide-el="track">
                                <div class="glide__slides">
                                    <div class="glide__slide">
                                        <div class="card w-100 sh-25 hover-img-scale-up">
                                            <img src="${context}/resources/img/banner/cta-standard-1.jpg" class="card-img h-100 scale" alt="card image" />
                                            <div class="card-img-overlay d-flex flex-column justify-content-end bg-transparent">
                                                <a href="#" class="stretched-link">
                                                    <div class="cta-3 mb-3 text-black w-100 w-sm-50">Mark Zuckerberg</div>
                                                    <div class="w-50 text-black d-none d-md-block">
                                                        <div class="clamp-line sh-8" data-line="3">
                                                            Lollipop chocolate marzipan marshmallow gummi bears. Tootsie roll liquorice cake jelly beans. Lollipop chocolate marzipan
                                                            marshmallow gummi bears. Tootsie roll liquorice cake jelly beans.
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="glide__slide">
                                        <div class="card w-100 sh-25 hover-img-scale-up">
                                            <img src="${context}/resources/img/banner/cta-standard-2.jpg" class="card-img h-100 scale" alt="card image" />
                                            <div class="card-img-overlay d-flex flex-column justify-content-end bg-transparent">
                                                <a href="#" class="stretched-link">
                                                    <div class="cta-3 mb-3 text-black w-100 w-sm-50">14 Facts About Sugar</div>
                                                    <div class="w-50 text-black d-none d-md-block">
                                                        <div class="clamp-line sh-8" data-line="3">
                                                            Sesame snaps brownie gummi bears tootsie roll caramels dragÃ©e. Powder cake gummies jelly beans toffee carrot cake bonbon powder
                                                            muffin.
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="glide__slide">
                                        <div class="card w-100 sh-25 hover-img-scale-up">
                                            <img src="${context}/resources/img/banner/cta-standard-3.jpg" class="card-img h-100 scale" alt="card image" />
                                            <div class="card-img-overlay d-flex flex-column justify-content-end bg-transparent">
                                                <a href="#" class="stretched-link">
                                                    <div class="cta-3 mb-3 text-black w-100 w-sm-50">Apple Cake Recipe for Starters</div>
                                                    <div class="w-50 text-black d-none d-md-block">
                                                        <div class="clamp-line sh-8" data-line="3">
                                                            Cupcake gingerbread cookie cookie lemon drops tootsie roll lollipop. Tiramisu toffee brownie sweet roll sesame snaps halvah.
                                                            Icing carrot cake cupcake gummi bears danish.
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Popular Articles End -->

                <!-- Quick Reads Start -->
                <h2 class="small-title">Quick Reads</h2>
                <div class="row row-cols-1 row-cols-sm-2 row-cols-xl-4">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/zopf.jpg" class="card-img-top sh-19" alt="card image" />
                            <div class="card-body">
                                <h5 class="heading mb-3">
                                    <a href="#" class="body-link stretched-link">
                                        <span class="clamp-line sh-5" data-line="2">Basic Introduction to Bread Making</span>
                                    </a>
                                </h5>
                                <div>
                                    <div class="row g-0">
                                        <div class="col-auto pe-3">
                                            <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                            <span class="align-middle">67</span>
                                        </div>
                                        <div class="col">
                                            <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                            <span class="align-middle">10 Min</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mb-5">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/toast-bread.jpg" class="card-img-top sh-19" alt="card image" />
                            <div class="card-body">
                                <h5 class="heading mb-3">
                                    <a href="#" class="body-link stretched-link">
                                        <span class="clamp-line sh-5" data-line="2">14 Facts About Sugar Products</span>
                                    </a>
                                </h5>
                                <div>
                                    <div class="row g-0">
                                        <div class="col-auto pe-3">
                                            <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                            <span class="align-middle">24</span>
                                        </div>
                                        <div class="col">
                                            <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                            <span class="align-middle">15 Min</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mb-5">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/michetta.jpg" class="card-img-top sh-19" alt="card image" />
                            <div class="card-body">
                                <h5 class="heading mb-3">
                                    <a href="#" class="body-link stretched-link">
                                        <span class="clamp-line sh-5" data-line="2">Apple Cake Recipe for Starters</span>
                                    </a>
                                </h5>
                                <div>
                                    <div class="row g-0">
                                        <div class="col-auto pe-3">
                                            <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                            <span class="align-middle">274</span>
                                        </div>
                                        <div class="col">
                                            <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                            <span class="align-middle">15 Min</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mb-5">
                        <div class="card h-100">
                            <img src="${context}/resources/img/product/small/pullman-loaf.jpg" class="card-img-top sh-19" alt="card image" />
                            <div class="card-body">
                                <h5 class="heading mb-3">
                                    <a href="#" class="body-link stretched-link">
                                        <span class="clamp-line sh-5" data-line="2">A Complete Guide to Mix Dough for the Molds</span>
                                    </a>
                                </h5>
                                <div>
                                    <div class="row g-0">
                                        <div class="col-auto pe-3">
                                            <i data-cs-icon="like" class="text-primary me-1" data-cs-size="15"></i>
                                            <span class="align-middle">144</span>
                                        </div>
                                        <div class="col">
                                            <i data-cs-icon="clock" class="text-primary me-1" data-cs-size="15"></i>
                                            <span class="align-middle">25 Min</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Quick Reads End -->
            </div>
        </main>


        <!-- Search Modal Start -->
        <div class="modal fade modal-under-nav modal-search modal-close-out" id="searchPagesModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header border-0 p-0">
                        <button type="button" class="btn-close btn btn-icon btn-icon-only btn-foreground" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body ps-5 pe-5 pb-0 border-0">
                        <input id="searchPagesInput" class="form-control form-control-xl borderless ps-0 pe-0 mb-1 auto-complete" type="text" autocomplete="off" />
                    </div>
                    <div class="modal-footer border-top justify-content-start ps-5 pe-5 pb-3 pt-3 border-0">
                        <span class="text-alternate d-inline-block m-0 me-3">
                            <i data-cs-icon="arrow-bottom" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                            <span class="align-middle text-medium">Navigate</span>
                        </span>
                        <span class="text-alternate d-inline-block m-0 me-3">
                            <i data-cs-icon="arrow-bottom-left" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                            <span class="align-middle text-medium">Select</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <!-- Vendor Scripts Start -->
        <script src="${context}/resources/js/vendor/jquery-3.5.1.min.js"></script>
        <script src="${context}/resources/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="${context}/resources/js/vendor/OverlayScrollbars.min.js"></script>
        <script src="${context}/resources/js/vendor/autoComplete.min.js"></script>
        <script src="${context}/resources/js/vendor/clamp.min.js"></script>
        <script src="${context}/resources/js/vendor/glide.min.js"></script>
        <!-- Vendor Scripts End -->

        <!-- Template Base Scripts Start -->
        <script src="${context}/resources/font/CS-Line/csicons.min.js"></script>
        <script src="${context}/resources/js/base/helpers.js"></script>
        <script src="${context}/resources/js/base/globals.js"></script>
        <script src="${context}/resources/js/base/nav.js"></script>
        <script src="${context}/resources/js/base/search.js"></script>
        <script src="${context}/resources/js/base/settings.js"></script>
        <script src="${context}/resources/js/base/init.js"></script>
        <!-- Template Base Scripts End -->
        <!-- Page Specific Scripts Start -->
        <script src="${context}/resources/js/cs/glide.custom.js"></script>
        <script src="${context}/resources/js/pages/blog.home.js"></script>
        <script src="${context}/resources/js/common.js"></script>
        <script src="${context}/resources/js/scripts.js"></script>
        <!-- Page Specific Scripts End -->
    </body>
</html>
