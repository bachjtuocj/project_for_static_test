<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en" data-footer="true">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>Bảng quản trị</title>
        <meta name="description"
              content="Home screen that contains stats, charts, call to action buttons and various listing elements." />
        <!-- Favicon Tags Start -->

        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x310.png" />
        <!-- Favicon Tags End -->
        <!-- Font Tags Start -->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font/CS-Interface/style.css" />
        <!-- Font Tags End -->
        <!-- Vendor Styles Start -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/OverlayScrollbars.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/glide.core.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/glide.core.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/introjs.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/select2.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/select2-bootstrap4.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/plyr.css" />
        <!-- Vendor Styles End -->
        <!-- Template Base Styles Start -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" />
        <!-- Template Base Styles End -->

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
        <script src="${pageContext.request.contextPath}/resources/js/base/loader.js"></script>
    </head>

    <body>
        <%@include file="adminHeader.jsp" %>

        <main>
            <div class="container">
                <div class="page-title-container">- Mới sử dụng được chức năng quản lý thành viên</div>

        </main>





        <!-- Search Modal Start -->
        <div class="modal fade modal-under-nav modal-search modal-close-out" id="searchPagesModal" tabindex="-1" role="dialog"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header border-0 p-0">
                        <button type="button" class="btn-close btn btn-icon btn-icon-only btn-foreground" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body ps-5 pe-5 pb-0 border-0">
                        <input id="searchPagesInput" class="form-control form-control-xl borderless ps-0 pe-0 mb-1 auto-complete"
                               type="text" autocomplete="off" />
                    </div>
                    <div class="modal-footer border-top justify-content-start ps-5 pe-5 pb-3 pt-3 border-0">
                        <span class="text-alternate d-inline-block m-0 me-3">
                            <i data-cs-icon="arrow-bottom" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                            <span class="align-middle text-medium">Navigate</span>
                        </span>
                        <span class="text-alternate d-inline-block m-0 me-3">
                            <i data-cs-icon="arrow-bottom-left" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                            <span class="align-middle text-medium">Select</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <!-- Vendor Scripts Start -->
        <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery-3.5.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/OverlayScrollbars.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/autoComplete.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/clamp.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/Chart.bundle.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/chartjs-plugin-datalabels.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/chartjs-plugin-rounded-bar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/glide.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/intro.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/select2.full.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/plyr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/cs/scrollspy.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap-submenu.js"></script>
        <!-- Vendor Scripts End -->

        <!-- Template Base Scripts Start -->
        <script src="${pageContext.request.contextPath}/resources/font/CS-Line/csicons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/helpers.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/globals.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/nav.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/search.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/init.js"></script>
        <!-- Template Base Scripts End -->
        <!-- Page Specific Scripts Start -->
        <script src="${pageContext.request.contextPath}/resources/js/cs/glide.custom.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/cs/charts.extend.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/pages/dashboard.default.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/common.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/cs/autocomplete.custom.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/forms/controls.autocomplete.js"></script>
        <!-- Page Specific Scripts End -->
    </body>

</html>