
/**
 *
 * AuthLogin
 *
 * Pages.Authentication.Login.html page content scripts. Initialized from scripts.js file.
 *
 *
 */

class DiscussionForm {
    constructor() {
        // Initialization of the page plugins
        this._initForm();
    }

    // Form validation
    _initForm() {
        const form = $("#discussionForm")[0];
        const validateOptions = {
            rules: {
                content: {
                    required: true,
                },
            },
            messages: {
                content: {
                    required: 'Vui lòng không để trống bình luận!',
                },
            },
        };
        $(form).validate(validateOptions);
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            event.stopPropagation();
            if (jQuery(form).valid()) {
                const formValues = {
                    action: "discussion",
                    lessonId: "1",
                    content: form.querySelector('[name="content"]').value
                };
                $.ajax({
                    type: "POST",
                    data: formValues,
                    success: function (response) {
                        if (response === "Add discussion fail") {
                            createFailed(0)
                        } else {
                            createSuccess(response);
                        }
                    },
                });
                return;
            }
        });

        function createSuccess(response) {
            $("#discussion-contain").prepend(response);
            $(':input', '#discussionForm').val('');
        }

        function createFailed(code) {
            document.getElementById("message").style.color = "red";
            $("#regBut").attr("disabled", false)
            if (code === 0) {
                document.getElementById("message").innerHTML = "Thêm bình luận không thành công!";
            }
        }
    }
}