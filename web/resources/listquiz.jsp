<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html
  lang="en"
  data-footer="true"
  data-override='{"attributes": {"placement": "horizontal","layout": "boxed", "behaviour": "unpinned" }, "storagePrefix": "elearning-portal"}'
>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Quiz List</title>
    <meta name="description" content="Acorn elearning platform quiz list." />
    <!-- Favicon Tags Start -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;" />
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x310.png" />
    <!-- Favicon Tags End -->
    <!-- Font Tags Start -->
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font/CS-Interface/style.css" />
    <!-- Font Tags End -->
    <!-- Vendor Styles Start -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/OverlayScrollbars.min.css" />

    <!-- Vendor Styles End -->
    <!-- Template Base Styles Start -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" />
    <!-- Template Base Styles End -->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
    <script src="${pageContext.request.contextPath}/resources/js/base/loader.js"></script>
  </head>

  <body>
      <%@include file="header.jsp" %>
 
      <main>
        <div class="container">
          <!-- Title and Top Buttons Start -->
          <div class="page-title-container">
            <div class="row">
              <!-- Title Start -->
              <div class="col-12 col-md-7">
                <h1 class="mb-0 pb-0 display-4" id="title">Danh sách quiz</h1>
                <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
                  <ul class="breadcrumb pt-0">
                    <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>
                  </ul>
                </nav>
              </div>
              <!-- Title End -->
            </div>
          </div>
          <!-- Title and Top Buttons End -->
          <form action ="quiz" method="post">
<div style="display: flex" class="autocomplete-container">
    
                      <input type ="hidden" name ="action" value ="search">
                    <input type="text" name="sname" class="form-control" autocomplete="off" placeholder="Tên quiz"/>&nbsp;
                    
                    <select name="scategory" class="form-select">
                            <option value="" disabled selected="">Chủ đề</option>
                            <c:forEach var="o" items="${listCategory}">
                            <option value="${o.cid}">${o.cname}</option>
                            </c:forEach>
                          </select> &nbsp;
                    <select name="slevel" class="form-select">
                            <option value=""disabled selected="" >Level</option>
                            <option value="1">Dễ</option>
                            <option value="2">Bình thường</option>
                     <option value="3">Khó</option>
                          </select> &nbsp;
               
                     <button style="" class="btn btn-icon btn-icon-end btn-primary mb-1" id="searchButton" type="submit">
                          <span>Tìm kiếm</span>
                          <i data-cs-icon="arrow-bottom-right"></i>
                        </button>
  
                  </div>
                </form>
          <!-- Content Start -->
          <br>
          <div class="row row-cols-1 row-cols-sm-2 row-cols-xl-3 row-cols-xxl-5 g-4">
              <c:forEach var="o" items="${listQ}">
            <div class="col">
              <div class="card h-100">
                <img src="${pageContext.request.contextPath}/resources/img/product/small/${o.thumbnail}" class="card-img-top sh-25" alt="card image" />
                <div class="card-body">
                  <h5 class="heading mb-2">
                    <a href="/quiz?action=detail&id=${o.id}" class="body-link">
                      <span class="clamp-line sh-6 lh-1-5" data-line="2">${o.name}</span>
                    </a>
                  </h5>
                  <div class="mb-3 text-muted sh-8 clamp-line" data-line="3">
                    ${o.description}
                  </div>
                  <div class="row g-0 align-items-center mb-1">
                    <div class="col-auto">
                      <div class="sw-3 sh-4 d-flex justify-content-center align-items-center">
                        <i data-cs-icon="form-check" class="text-primary"></i>
                      </div>
                    </div>
                    <div class="col ps-3">
                      <div class="row g-0">
                        <div class="col">
                          <div class="text-alternate sh-4 d-flex align-items-center lh-1-25">Số câu</div>
                        </div>
                        <div class="col-auto">
                          <div class="sh-4 d-flex align-items-center text-alternate">${o.numofquest}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row g-0 align-items-center mb-1">
                    <div class="col-auto">
                      <div class="sw-3 sh-4 d-flex justify-content-center align-items-center">
                        <i data-cs-icon="clock" class="text-primary"></i>
                      </div>
                    </div>
                    <div class="col ps-3">
                      <div class="row g-0">
                        <div class="col">
                          <div class="text-alternate sh-4 d-flex align-items-center lh-1-25">Thời gian làm bài</div>
                        </div>
                        <div class="col-auto">
                          <div class="sh-4 d-flex align-items-center text-alternate">${o.timelimit}m</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row g-0 align-items-center mb-4">
                    <div class="col-auto">
                      <div class="sw-3 sh-4 d-flex justify-content-center align-items-center">
                        <i data-cs-icon="graduation" class="text-primary"></i>
                      </div>
                    </div>
                    <div class="col ps-3">
                      <div class="row g-0">
                        <div class="col">
                          <div class="text-alternate sh-4 d-flex align-items-center lh-1-25">Độ khó</div>
                        </div>
                        <div class="col-auto">
                          <div class="sh-4 d-flex align-items-center text-alternate">${o.level}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="d-flex flex-row justify-content-between w-100 w-sm-50 w-xl-100">
                    <a href="Quiz.Detail.html" class="btn btn-outline-primary w-100 me-1 btn-sm">Vào làm</a>
                    
                  </div>
                </div>
              </div>
            </div>
              </c:forEach>
            
          </div>

          <!-- Content End -->
        </div>
          <br>
          <nav aria-label="Page navigation example">
             <ul class="pagination justify-content-end">
                  <c:if test="${page!=1}">
              <li class="page-item">
                <a class="page-link" href="./quiz?action=${action}&pageid=1" aria-label="First">
                  <i data-cs-icon="arrow-double-left"></i>
                </a>
              </li>
              <li class="page-item">
                
                <a class="page-link" href="./quiz?action=${action}&pageid=${pageprev}" aria-label="Trang trước">
                  <i data-cs-icon="chevron-left"></i>
                </a>
                 
              </li>
                </c:if>
               <c:forEach begin="${1}" end="${num}" var="i">
              <li class="page-item ${i==page?"active":""}"><a class="page-link" href="./quiz?action=${action}&pageid=${i}">${i}</a></li>
               </c:forEach>
              <li class="page-item">
                  <c:if test="${page!=num}">
                <a class="page-link" href="./quiz?action=${action}&pageid=${pagenext}" aria-label="Trang sau">
                  <i data-cs-icon="chevron-right"></i>
                </a>
        
              </li>
               <li class="page-item">
                <a class="page-link" href="./quiz?action=${action}&pageid=${num}" aria-label="Last">
                  <i data-cs-icon="arrow-double-right"></i>
                </a>
              </li>
                </c:if>
            </ul>          </nav>
      </main>
      <!-- Layout Footer Start -->
      

    <!-- Vendor Scripts Start -->
    <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery-3.5.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/vendor/OverlayScrollbars.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/vendor/autoComplete.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/vendor/clamp.min.js"></script>

    <!-- Vendor Scripts End -->

    <!-- Template Base Scripts Start -->
    <script src="${pageContext.request.contextPath}/resources/font/CS-Line/csicons.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/base/helpers.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/base/globals.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/base/nav.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/base/search.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/base/settings.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/base/init.js"></script>
    <!-- Template Base Scripts End -->
    <!-- Page Specific Scripts Start -->

    <script src="${pageContext.request.contextPath}/resources/js/common.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
    <!-- Page Specific Scripts End -->
  </body>
</html>
