<%-- 
    Document   : verifyEmail
    Created on : Sep 22, 2021, 1:46:57 PM
    Author     : dai8p
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Sent Mail!</h1>
        <br>
        <div id="message">
            <p class="h6" id="message"></p>
        </div>
    </body>


    <!-- Vendor Scripts Start -->
    <script src="${context}/resources/js/vendor/jquery-3.5.1.min.js"></script>
    <script src="${context}/resources/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="${context}/resources/js/vendor/OverlayScrollbars.min.js"></script>
    <script src="${context}/resources/js/vendor/autoComplete.min.js"></script>
    <script src="${context}/resources/js/vendor/clamp.min.js"></script>
    <script src="${context}/resources/js/vendor/jquery.validate/jquery.validate.min.js"></script>
    <script src="${context}/resources/js/vendor/jquery.validate/additional-methods.min.js"></script>
    <!-- Vendor Scripts End -->

    <script>

        $(function () {
            $.ajax({
                type: "POST",
                success: function (response) {
                    console.log("send mail success");
                }

            });
            return;
        });
    </script>
</html>
