<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en" data-footer="true">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>Acorn Admin Template | Default Dashboard</title>
        <meta name="description"
              content="Home screen that contains stats, charts, call to action buttons and various listing elements." />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Favicon Tags Start -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x310.png" />
        <!-- Favicon Tags End -->
        <!-- Font Tags Start -->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font/CS-Interface/style.css" />
        <!-- Font Tags End -->
        <!-- Vendor Styles Start -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/OverlayScrollbars.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/glide.core.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/glide.core.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/introjs.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/select2.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/select2-bootstrap4.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/plyr.css" />
        <!-- Vendor Styles End -->
        <!-- Template Base Styles Start -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" />
        <!-- Template Base Styles End -->

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
        <script src="${pageContext.request.contextPath}/resources/js/base/loader.js"></script>
    </head>

    <body>
        <div id="root">
            <div id="nav" class="nav-container d-flex">
                <div class="nav-content d-flex">
                    <!-- Logo Start -->
                    <div class="logo position-relative">
                        <a href="Dashboards.Default.html">
                            <!-- Logo can be added directly -->
                            <!-- <img src="img/logo/logo-white.svg" alt="logo" /> -->

                            <!-- Or added via css to provide different ones for different color themes -->
                            <div class="img"></div>
                        </a>
                    </div>
                    <!-- Logo End -->

                    <!-- Language Switch Start -->
                    <div class="language-switch-container">
                        <button class="btn btn-empty language-button dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">EN</button>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item">DE</a>
                            <a href="#" class="dropdown-item active">EN</a>
                            <a href="#" class="dropdown-item">ES</a>
                        </div>
                    </div>
                    <!-- Language Switch End -->

                    <!-- User Menu Start -->
                    <div class="user-container d-flex">
                        <a href="#" class="d-flex user position-relative" data-bs-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <img class="profile" alt="profile" src="${pageContext.request.contextPath}/resources/img/profile/profile-9.jpg" />
                            <div class="name">Lisa Jackson</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end user-menu wide">
                            <div class="row mb-3 ms-0 me-0">
                                <div class="col-12 ps-1 mb-2">
                                    <div class="text-extra-small text-primary">ACCOUNT</div>
                                </div>
                                <div class="col-6 ps-1 pe-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">User Info</a>
                                        </li>
                                        <li>
                                            <a href="#">Preferences</a>
                                        </li>
                                        <li>
                                            <a href="#">Calendar</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6 pe-1 ps-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">Security</a>
                                        </li>
                                        <li>
                                            <a href="#">Billing</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-1 ms-0 me-0">
                                <div class="col-12 p-1 mb-2 pt-2">
                                    <div class="text-extra-small text-primary">APPLICATION</div>
                                </div>
                                <div class="col-6 ps-1 pe-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">Themes</a>
                                        </li>
                                        <li>
                                            <a href="#">Language</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6 pe-1 ps-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">Devices</a>
                                        </li>
                                        <li>
                                            <a href="#">Storage</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-1 ms-0 me-0">
                                <div class="col-12 p-1 mb-3 pt-3">
                                    <div class="separator-light"></div>
                                </div>
                                <div class="col-6 ps-1 pe-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">
                                                <i data-cs-icon="help" class="me-2" data-cs-size="17"></i>
                                                <span class="align-middle">Help</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i data-cs-icon="file-text" class="me-2" data-cs-size="17"></i>
                                                <span class="align-middle">Docs</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6 pe-1 ps-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">
                                                <i data-cs-icon="gear" class="me-2" data-cs-size="17"></i>
                                                <span class="align-middle">Settings</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i data-cs-icon="logout" class="me-2" data-cs-size="17"></i>
                                                <span class="align-middle">Logout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- User Menu End -->

                    <!-- Icons Menu Start -->
                    <ul class="list-unstyled list-inline text-center menu-icons">
                        <li class="list-inline-item">
                            <a href="#" data-bs-toggle="modal" data-bs-target="#searchPagesModal">
                                <i data-cs-icon="search" data-cs-size="18"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" id="pinButton" class="pin-button">
                                <i data-cs-icon="lock-on" class="unpin" data-cs-size="18"></i>
                                <i data-cs-icon="lock-off" class="pin" data-cs-size="18"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" id="colorButton">
                                <i data-cs-icon="light-on" class="light" data-cs-size="18"></i>
                                <i data-cs-icon="light-off" class="dark" data-cs-size="18"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" data-bs-toggle="dropdown" data-bs-target="#notifications" aria-haspopup="true"
                               aria-expanded="false" class="notification-button">
                                <div class="position-relative d-inline-flex">
                                    <i data-cs-icon="bell" data-cs-size="18"></i>
                                    <span class="position-absolute notification-dot rounded-xl"></span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end wide notification-dropdown scroll-out" id="notifications">
                                <div class="scroll">
                                    <ul class="list-unstyled border-last-none">
                                        <li class="mb-3 pb-3 border-bottom border-separator-light d-flex">
                                            <img src="${pageContext.request.contextPath}/resources/img/profile/profile-1.jpg" class="me-3 sw-4 sh-4 rounded-xl align-self-center"
                                                 alt="..." />
                                            <div class="align-self-center">
                                                <a href="#">Joisse Kaycee just sent a new comment!</a>
                                            </div>
                                        </li>
                                        <li class="mb-3 pb-3 border-bottom border-separator-light d-flex">
                                            <img src="${pageContext.request.contextPath}/resources/img/profile/profile-2.jpg" class="me-3 sw-4 sh-4 rounded-xl align-self-center"
                                                 alt="..." />
                                            <div class="align-self-center">
                                                <a href="#">New order received! It is total $147,20.</a>
                                            </div>
                                        </li>
                                        <li class="mb-3 pb-3 border-bottom border-separator-light d-flex">
                                            <img src="${pageContext.request.contextPath}/resources/img/profile/profile-3.jpg" class="me-3 sw-4 sh-4 rounded-xl align-self-center"
                                                 alt="..." />
                                            <div class="align-self-center">
                                                <a href="#">3 items just added to wish list by a user!</a>
                                            </div>
                                        </li>
                                        <li class="pb-3 pb-3 border-bottom border-separator-light d-flex">
                                            <img src="${pageContext.request.contextPath}/resources/img/profile/profile-6.jpg" class="me-3 sw-4 sh-4 rounded-xl align-self-center"
                                                 alt="..." />
                                            <div class="align-self-center">
                                                <a href="#">Kirby Peters just sent a new message!</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- Icons Menu End -->

                    <!-- Menu Start -->
                    <div class="menu-container flex-grow-1">
                        <ul id="menu" class="menu">

                        </ul>
                    </div>
                    <!-- Menu End -->

                    <!-- Mobile Buttons Start -->
                    <div class="mobile-buttons-container">
                        <!-- Scrollspy Mobile Button Start -->
                        <a href="#" id="scrollSpyButton" class="spy-button" data-bs-toggle="dropdown">
                            <i data-cs-icon="menu-dropdown"></i>
                        </a>
                        <!-- Scrollspy Mobile Button End -->

                        <!-- Scrollspy Mobile Dropdown Start -->
                        <div class="dropdown-menu dropdown-menu-end" id="scrollSpyDropdown"></div>
                        <!-- Scrollspy Mobile Dropdown End -->

                        <!-- Menu Button Start -->
                        <a href="#" id="mobileMenuButton" class="menu-button">
                            <i data-cs-icon="menu"></i>
                        </a>
                        <!-- Menu Button End -->
                    </div>
                    <!-- Mobile Buttons End -->
                </div>
                <div class="nav-shadow"></div>
            </div>

            <main>
                <div class="container">
                    <!-- Title and Top Buttons Start -->
                    <div class="page-title-container">
                        <div class="row">
                            <!-- Title Start -->
                            <div class="col-12 col-sm-6">
                                <h1 class="mb-0 pb-0 display-4" id="title">Default Dashboard</h1>
                                <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
                                    <ul class="breadcrumb pt-0">
                                        <li class="breadcrumb-item"><a href="Dashboards.Default.html">Home</a></li>
                                        <li class="breadcrumb-item"><a href="Dashboards.html">Dashboards</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- Title End -->

                            <!-- Top Buttons Start -->
                            <div class="col-12 col-sm-6 d-flex align-items-start justify-content-end">
                                <!-- Tour Button Start -->



                                <button type="button" class="btn btn-outline-primary btn-icon btn-icon-end w-100 w-sm-auto "
                                        data-bs-toggle="modal" data-bs-target="" id="settingsButton">
                                    <span><a href="${pageContext.request.contextPath}/admin/course?action=loadaddCourse"> Thêm khóa học</a></span>
                                    <i data-cs-icon="flag"></i>
                                </button>
                                <!-- Tour Button End -->
                            </div>
                            <!-- Top Buttons End -->
                        </div>
                    </div>
                    <!-- Title and Top Buttons End -->
                    <!-- Modal for add a member -->
                    <div class="modal fade w-100 scroll-out-negative" id="adds" data-bs-backdrop="true" tabindex="-2" role="dialog"
                         aria-labelledby="add" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable full" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="scroll-track-visible">
                                        <section class="scroll-section" id="personal">
                                            <h2 class="small-title">THÊM KHÓA HỌC</h2>
                                            <form  action="${pageContext.request.contextPath}/admin/course?action=add" method="post" class="tooltip-end-top" id="newModalForm"  >
                                                <div class="card-body">

                                                    <div class="row g-3">
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input type="text" class="form-control" name="name"  required/>
                                                                <span>TÊN KHÓA HỌC</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="price" required />
                                                                <span>GIÁ</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row g-3">
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="description" required />
                                                                <span>MÔ TẢ</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="content" required />
                                                                <span>NỘI DUNG</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row g-3">
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="level" required  />
                                                                <span>MỨC ĐỘ</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="thumbnail" required  />
                                                                <span>VIDEO GIỚI THIỆU</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row g-3">
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="duration" required  />
                                                                <span>THỜI GIAN HỌC</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <select name="category" class="form-select" aria-label="Default select example">
                                                                    <c:forEach items="${listCC}" var="o">
                                                                        <option value="${o.cid}">${o.cname}</option>
                                                                    </c:forEach>
                                                                </select>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class=" border-0 pt-0 d-flex justify-content-end align-items-center">
                                                        <div>

                                                            <button class="btn btn-icon btn-icon-end btn-outline-primary " type="submit">
                                                                <span><a href="" onclick="return submitform()">Thêm</a></span>
                                                                <i data-cs-icon="chevron-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(function () {

                            $("#newModalForm").validate({
                                rules: {
                                    pName: {
                                        required: true,
                                        minlength: 8
                                    },
                                    action: "required"
                                },
                                messages: {
                                    pName: {
                                        required: "Please enter some data",
                                        minlength: "Your data must be at least 8 characters"
                                    },
                                    action: "Please provide some data"
                                }
                            });
                        });
                    </script>
                    <!-- End model for add a member -->

                    <div class="row">
                        <div class="col-12 col-xl-12">
                            <!-- Sales & Stocks Charts Start -->
                            <section class="scroll-section" id="basic">
                                <h2 class="big-title">Quản lí khóa học</h2>
                                <div class="card mb-5">
                                    <div class="card-body">
                                        <form action="${pageContext.request.contextPath}/admin/course?action=searchCourse" method="post">
                                            <div style="display: flex" class="autocomplete-container">

                                                <input type="text" name="query" class="form-control" autocomplete="off" placeholder="Nhập giá trị tìm kiếm tại đây..... "/>&nbsp;

                                                <button style="" class="btn btn-icon btn-icon-end btn-primary mb-1" id="searchButton" type="submit">
                                                    <span>Tìm kiếm</span>
                                                    <i data-cs-icon="arrow-bottom-right"></i>
                                                </button>
                                            </div>
                                        </form>
                                        <div class="clearfix invisible-sm-block " style="margin: 10px"></div>

                                        <div class=" col-sm-8 col-xl-8   ">
                                            <div class="dropdown">
                                                <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" data-submenu="" aria-expanded="false">
                                                    Sắp xếp theo
                                                </button>                   
                                                <div class="dropdown-menu">
                                                    <!--                      <div class="dropdown dropend dropdown-submenu">
                                                                            <button class="dropdown-item dropdown-toggle" type="button">Mức độ</button>
                                                                               
                                                                            <div class="dropdown-menu">
                                                                                <form method="get" action="/page2">
                                                                              <button class="dropdown-item" type="submit">Beginner</button>
                                                                              </form>
                                                                                <form method="get" action="/page2">
                                                                              <button class="dropdown-item" type="submit">Junior</button>
                                                                              </form> 
                                                                                <form method="get" action="/page2">
                                                                              <button class="dropdown-item" type="submit">Senior</button>
                                                                              </form>
                                                                            </div>
                                                                          </div> -->
                                                    <div style="display: flex; align-items: center; flex-direction: column; width: 85px">
                                                        <a href="${pageContext.request.contextPath}/admin/course?action=sortName">Tên</a>
                                                        <a  href="${pageContext.request.contextPath}/admin/course?action=sortID">ID</a>
                                                        <a  href="${pageContext.request.contextPath}/admin/course?action=sortPrice">Giá</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">STT</th>
                                                    <th scope="col">Tên</th>
                                                    <th scope="col">Giá</th>
                                                    <th scope="col">Nội dung</th>
                                                    <th scope="col">Mức độ</th>
                                                    <th scope="col">Quản lí</th>
                                                    <th scope="col">Lựa chọn</th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <c:forEach items="${listCourse}" var="o">
                                                    <tr>
                                                        <th>${o.id}</th>
                                                        <td><a href="${pageContext.request.contextPath}/course/detail?cid=${o.id}">${o.name}</a></td>
                                                        <td>${o.price}$</td>
                                                        <td>${o.content}</a></td>
                                                        <td>${o.level}</td>
                                                        <td>
                                                            <button class="btn btn-icon btn-icon-end btn-outline-primary mb-1" type="button" data-bs-target="#closeButtonOutExample">
                                                                <span><a href="${pageContext.request.contextPath}/admin/course?action=delete&pid=${o.id}" onclick="return confirm('Are you sure you want to delete this course?')">Xóa</a></span>
                                                                <i data-cs-icon="bin"></i>
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-icon btn-icon-end btn-outline-primary mb-1" type="button"  >
                                                                <span><a href="${pageContext.request.contextPath}/admin/course?action=edit&pid=${o.id}">Chỉnh sửa</a></span>
                                                                <i data-cs-icon="arrow-bottom-right"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </section>
                            <div
                                class="modal fade modal-close-out"
                                id="closeButtonOutExample"
                                tabindex="-1"
                                role="dialog"
                                aria-labelledby="exampleModalLabelCloseOut"
                                aria-hidden="true"
                                >
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabelCloseOut">Modal title</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">...</div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary">Yes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-end">
                            <li class="page-item">
                                <c:if test="${action!=null}">
                                    <a class="page-link" href="${pageContext.request.contextPath}/admin/course?action=${action}&pageid=1" aria-label="First">
                                    </c:if>
                                    <c:if test="${action==null}">
                                        <a class="page-link" href="${pageContext.request.contextPath}/admin/course?pageid=1" aria-label="First">
                                        </c:if>
                                        <i data-cs-icon="arrow-double-left"></i>
                                    </a>
                            </li>
                            <li class="page-item">
                                <c:if test="${action!=null}">
                                    <a class="page-link" href="${pageContext.request.contextPath}/admin/course?action=${action}&pageid=${pageprev}" aria-label="Trang trước">
                                    </c:if>
                                    <c:if test="${action==null}">
                                        <a class="page-link" href="${pageContext.request.contextPath}/admin/course?pageid=${pageprev}" aria-label="Trang trước">
                                        </c:if>
                                        <i data-cs-icon="chevron-left"></i>
                                    </a>
                            </li>
                            <c:forEach begin="${1}" end="${num}" var="i">
                                <c:if test="${action!=null}">
                                    <li class="page-item ${i==page?"active":""}"><a class="page-link" href="${pageContext.request.contextPath}/admin/course?action=${action}&pageid=${i}">${i}</a></li>
                                    </c:if>
                                    <c:if test="${action==null}">
                                    <li class="page-item ${i==page?"active":""}"><a class="page-link" href="${pageContext.request.contextPath}/admin/course?pageid=${i}">${i}</a></li>
                                    </c:if>
                                </c:forEach>
                            <li class="page-item">
                                <c:if test="${action!=null}">
                                    <a class="page-link" href="${pageContext.request.contextPath}/admin/course?action=${action}&pageid=${pagenext}" aria-label="Trang sau">
                                    </c:if>
                                    <c:if test="${action==null}">
                                        <a class="page-link" href="${pageContext.request.contextPath}/admin/course?pageid=${pagenext}" aria-label="Trang sau">
                                        </c:if>
                                        <i data-cs-icon="chevron-right"></i>
                                    </a>
                            </li>
                        </ul>
                    </nav>
                    <!--          Content for delete confirmation-->

                    <!-- Contents for edit button -->
                    <div class="modal fade w-100 scroll-out-negative" id="edits" data-bs-backdrop="true" tabindex="-2"
                         role="dialog" aria-labelledby="edits" aria-hidden="false">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="scroll-track-visible">
                                        <section class="scroll-section" id="personal">
                                            <h2 class="small-title">CHỈNH SỬA KHÓA HỌC</h2>
                                            <form class="tooltip-end-top" id="personalForm" novalidate>
                                                <div class="card-body">
                                                    <div class="row g-3">
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="personalName" />
                                                                <span>${detail.name}</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="personalSocialSecurityNumber" />
                                                                <span>GIÁ</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row g-3">
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="personalPhone" />
                                                                <span>MÔ TẢ</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="personalEmail" />
                                                                <span>NỘI DUNG</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row g-3">
                                                        <div class="col-md-12">
                                                            <label class="mb-3 top-label">
                                                                <input class="form-control" name="personalBirthday" required id="personalBirthday" />
                                                                <span>MỨC ĐỘ</span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class=" border-0 pt-0 d-flex justify-content-end align-items-center">
                                                    <div>
                                                        <button class="btn btn-icon btn-icon-end btn-primary" type="submit">
                                                            <span>Save</span>
                                                            <i data-cs-icon="chevron-right"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                        </section>              
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                                <div class="row gy-5">
                                </div>
                            </div>
                            </main>

                            <!-- Layout Footer Start -->
                            <footer>
                                <div class="footer-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <p class="mb-0 text-muted text-medium">Colored Strategies 2021</p>
                                            </div>
                                            <div class="col-sm-6 d-none d-sm-block">
                                                <ul class="breadcrumb pt-0 pe-0 mb-0 float-end">
                                                    <li class="breadcrumb-item mb-0 text-medium">
                                                        <a href="https://1.envato.market/BX5oGy" target="_blank" class="btn-link">Review</a>
                                                    </li>
                                                    <li class="breadcrumb-item mb-0 text-medium">
                                                        <a href="https://1.envato.market/BX5oGy" target="_blank" class="btn-link">Purchase</a>
                                                    </li>
                                                    <li class="breadcrumb-item mb-0 text-medium">
                                                        <a href="https://acorn-html-docs.coloredstrategies.com/" target="_blank" class="btn-link">Docs</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                            <!-- Layout Footer End -->
                        </div>

                        <!-- Theme Settings Modal Start -->




                        <!-- Theme Settings Modal End -->

                        <!-- Niches Modal Start -->
                        <div class="modal fade modal-right scroll-out-negative" id="niches" data-bs-backdrop="true" tabindex="-1"
                             role="dialog" aria-labelledby="niches" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable full" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Niches</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>

                                    <div class="modal-body">
                                        <div class="scroll-track-visible">
                                            <div class="mb-5">
                                                <label class="mb-2 d-inline-block form-label">Classic Dashboard</label>
                                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                                        <img src="https://acorn.coloredstrategies.com/img/page/classic-dashboard.jpg"
                                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                                            <a target="_blank" href="https://acorn-html-classic-dashboard.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Html
                                                            </a>
                                                            <a target="_blank" href="https://acorn-laravel-classic-dashboard.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Laravel
                                                            </a>
                                                            <a target="_blank" href="https://acorn-dotnet-classic-dashboard.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                .Net5
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="mb-5">
                                                <label class="mb-2 d-inline-block form-label">Ecommerce Platform</label>
                                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                                        <img src="https://acorn.coloredstrategies.com/img/page/ecommerce-platform.jpg"
                                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                                            <a target="_blank" href="https://acorn-html-ecommerce-platform.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Html
                                                            </a>
                                                            <a target="_blank" href="https://acorn-laravel-ecommerce-platform.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Laravel
                                                            </a>
                                                            <a target="_blank" href="https://acorn-dotnet-ecommerce-platform.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                .Net5
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="mb-5">
                                                <label class="mb-2 d-inline-block form-label">Elearning Portal</label>
                                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                                        <img src="https://acorn.coloredstrategies.com/img/page/elearning-portal.jpg"
                                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                                            <a target="_blank" href="https://acorn-html-elearning-portal.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Html
                                                            </a>
                                                            <a target="_blank" href="https://acorn-laravel-elearning-portal.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Laravel
                                                            </a>
                                                            <a target="_blank" href="https://acorn-dotnet-elearning-portal.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                .Net5
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="mb-5">
                                                <label class="mb-2 d-inline-block form-label">Service Provider</label>
                                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                                        <img src="https://acorn.coloredstrategies.com/img/page/service-provider.jpg"
                                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                                            <a target="_blank" href="https://acorn-html-service-provider.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Html
                                                            </a>
                                                            <a target="_blank" href="https://acorn-laravel-service-provider.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Laravel
                                                            </a>
                                                            <a target="_blank" href="https://acorn-dotnet-service-provider.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                .Net5
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="mb-5">
                                                <label class="mb-2 d-inline-block form-label">Starter Project</label>
                                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                                        <img src="https://acorn.coloredstrategies.com/img/page/starter-project.jpg"
                                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                                            <a target="_blank" href="https://acorn-html-starter-project.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Html
                                                            </a>
                                                            <a target="_blank" href="https://acorn-laravel-starter-project.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                Laravel
                                                            </a>
                                                            <a target="_blank" href="https://acorn-dotnet-starter-project.coloredstrategies.com/"
                                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                                .Net5
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Niches Modal End -->

                        <!-- Theme Settings & Niches Buttons Start -->
                        <div class="settings-buttons-container">
                            <button type="button" class="btn settings-button btn-primary p-0" data-bs-toggle="modal" data-bs-target="#settings"
                                    id="settingsButton">
                                <span class="d-inline-block no-delay" data-bs-delay="0" data-bs-offset="0,3" data-bs-toggle="tooltip"
                                      data-bs-placement="left" title="Settings">
                                    <i data-cs-icon="paint-roller" class="position-relative"></i>
                                </span>
                            </button>
                            <button type="button" class="btn settings-button btn-primary p-0" data-bs-toggle="modal" data-bs-target="#niches"
                                    id="nichesButton">
                                <span class="d-inline-block no-delay" data-bs-delay="0" data-bs-offset="0,3" data-bs-toggle="tooltip"
                                      data-bs-placement="left" title="Niches">
                                    <i data-cs-icon="toy" class="position-relative"></i>
                                </span>
                            </button>
                            <a href="https://1.envato.market/BX5oGy" target="_blank" class="btn settings-button btn-primary p-0"
                               id="purchaseButton">
                                <span class="d-inline-block no-delay" data-bs-delay="0" data-bs-offset="0,3" data-bs-toggle="tooltip"
                                      data-bs-placement="left" title="Purchase">
                                    <i data-cs-icon="cart" class="position-relative"></i>
                                </span>
                            </a>
                        </div>
                        <!-- Theme Settings & Niches Buttons End -->

                        <!-- Search Modal Start -->
                        <div class="modal fade modal-under-nav modal-search modal-close-out" id="searchPagesModal" tabindex="-1" role="dialog"
                             aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header border-0 p-0">
                                        <button type="button" class="btn-close btn btn-icon btn-icon-only btn-foreground" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body ps-5 pe-5 pb-0 border-0">
                                        <input id="searchPagesInput" class="form-control form-control-xl borderless ps-0 pe-0 mb-1 auto-complete"
                                               type="text" autocomplete="off" />
                                    </div>
                                    <div class="modal-footer border-top justify-content-start ps-5 pe-5 pb-3 pt-3 border-0">
                                        <span class="text-alternate d-inline-block m-0 me-3">
                                            <i data-cs-icon="arrow-bottom" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                                            <span class="align-middle text-medium">Navigate</span>
                                        </span>
                                        <span class="text-alternate d-inline-block m-0 me-3">
                                            <i data-cs-icon="arrow-bottom-left" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                                            <span class="align-middle text-medium">Select</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Search Modal End -->

                        <!-- Vendor Scripts Start -->
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery-3.5.1.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap.bundle.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/OverlayScrollbars.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/autoComplete.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/clamp.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/Chart.bundle.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/chartjs-plugin-datalabels.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/chartjs-plugin-rounded-bar.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/glide.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/intro.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/select2.full.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/plyr.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/cs/scrollspy.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap-submenu.js"></script>
                        <!-- Vendor Scripts End -->

                        <!-- Template Base Scripts Start -->
                        <script src="${pageContext.request.contextPath}/resources/font/CS-Line/csicons.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/base/helpers.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/base/globals.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/base/nav.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/base/search.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/base/settings.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/base/init.js"></script>
                        <!-- Template Base Scripts End -->
                        <!-- Page Specific Scripts Start -->
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/cs/glide.custom.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/cs/charts.extend.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/pages/dashboard.default.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/common.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/cs/autocomplete.custom.js"></script>
                        <script src="${pageContext.request.contextPath}/resources/js/forms/controls.autocomplete.js"></script>
                        <!-- Page Specific Scripts End -->
                        </body>

                        </html>