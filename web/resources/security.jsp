<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en" data-footer="true" data-override='{"attributes":{"layout": "boxed"}}'>
<c:set var="context" value="${pageContext.request.contextPath}"/>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Bảo mật</title>
    <meta name="description" content="Profile Security" />
    <!-- Favicon Tags Start -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${context}/resources/img/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${context}/resources/img/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${context}/resources/img/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${context}/resources/img/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="${context}/resources/img/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="${context}/resources/img/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="${context}/resources/img/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="${context}/resources/img/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;" />
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="${context}/resources/img/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="${context}/resources/img/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="${context}/resources/img/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="${context}/resources/img/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="${context}/resources/img/favicon/mstile-310x310.png" />
    <!-- Favicon Tags End -->
    <!-- Font Tags Start -->
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="${context}/resources/font/CS-Interface/style.css" />
    <!-- Font Tags End -->
    <!-- Vendor Styles Start -->
    <link rel="stylesheet" href="${context}/resources/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="${context}/resources/css/vendor/OverlayScrollbars.min.css" />
    <link rel="stylesheet" href="${context}/resources/css/vendor/select2.min.css" />
    <link rel="stylesheet" href="${context}/resources/css/vendor/select2-bootstrap4.min.css" />
  
    <!-- Vendor Styles End -->
    <!-- Template Base Styles Start -->
    <link rel="stylesheet" href="${context}/resources/css/styles.css" />
    <!-- Template Base Styles End -->

    <link rel="stylesheet" href="${context}/resources/css/main.css" />
    <script src="${context}/resources/js/base/loader.js"></script>
  </head>

  <body>
    <%@include file="header.jsp" %>

      <main>
        <div class="container">
          <div class="row">
            <div class="col-auto d-none d-lg-flex">
              <div class="nav flex-column sw-25 mt-n2" id="settingsColumn">
                <!-- Content of this will be moved from #settingsMoveContent div based on the responsive breakpoint.  -->
              </div>
            </div>

            <div class="col">
              <!-- Title and Top Buttons Start -->
              <div class="page-title-container">
                <div class="row">
                  <!-- Title Start -->
                  <div class="col">
                    <h1 class="mb-0 pb-0 display-4" id="title">Profile</h1>
                    <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
                      <ul class="breadcrumb pt-0">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item"><a href="profile">Profile</a></li>
                        <li class="breadcrumb-item"><a href="security">Security</a></li>
                      </ul>
                    </nav>
                  </div>
                  <!-- Title End -->

                  <!-- Top Buttons Start -->
                  <div class="col-12 col-sm-auto d-flex align-items-start justify-content-end d-block d-lg-none">
                    <button type="button" class="btn btn-icon btn-icon-start btn-outline-primary w-100 w-sm-auto" data-bs-toggle="dropdown">
                      <i data-cs-icon="gear"></i>
                      <span>Cài đặt</span>
                    </button>
                    <!-- Content of below will be moved to #settingsColumn or back in here based on the data-move-breakpoint attribute below -->
                    <!-- Content will be here if the screen size is smaller than lg -->

                    <!-- In Page Menu Start -->
                    <div
                      class="dropdown-menu dropdown-menu-end sw-25 py-3 px-4"
                      id="settingsMoveContent"
                      data-move-target="#settingsColumn"
                      data-move-breakpoint="lg"
                    >
                      <div class="mb-2">
                        <a class="nav-link px-0" href="${context}/profile">
                          <i data-cs-icon="activity" class="me-2" data-cs-size="17"></i>
                          <span class="align-middle">Profile</span>
                        </a>
                        <div>
                          <a class="nav-link py-1 my-1 px-0" href="${context}/profile">
                            <i class="me-2 sw-2 d-inline-block"></i>
                            <span class="align-middle">Cá nhân</span>
                          </a>
                          <a class="nav-link py-1 my-1 px-0" href="#">
                            <i class="me-2 sw-2 d-inline-block"></i>
                            <span class="align-middle">Bạn bè</span>
                          </a>
                          <a class="nav-link py-1 my-1 px-0" href="#">
                            <i class="me-2 sw-2 d-inline-block"></i>
                            <span class="align-middle">Tài khoản</span>
                          </a>
                        </div>
                      </div>
                      <div class="mb-2">
                        <a class="nav-link px-0" href="#">
                          <i data-cs-icon="credit-card" class="me-2" data-cs-size="17"></i>
                          <span class="align-middle">Thanh toán</span>
                        </a>
                        <div>
                          <a class="nav-link py-1 my-1 px-0" href="#">
                            <i class="me-2 sw-2 d-inline-block"></i>
                            <span class="align-middle">Phương thức</span>
                          </a>
                          <a class="nav-link py-1 my-1 px-0" href="#">
                            <i class="me-2 sw-2 d-inline-block"></i>
                            <span class="align-middle">Hoá đơn</span>
                          </a>
                          
                        </div>
                      </div>
                      <div class="mb-2">
                        <a class="nav-link active px-0" href="password">
                          <i data-cs-icon="shield" class="me-2" data-cs-size="17"></i>
                          <span class="align-middle">Bảo mật</span>
                        </a>
                        <div>
                        
                         
                          
                        </div>
                      </div>
                      <div class="mb-2">
                        <a class="nav-link px-0" href="#">
                          <i data-cs-icon="notification" class="me-2" data-cs-size="17"></i>
                          <span class="align-middle">Thông báo</span>
                        </a>
                      </div>
                      <div class="mb-2">
                        <a class="nav-link px-0" href="#">
                          <i data-cs-icon="tablet" class="me-2" data-cs-size="17"></i>
                          <span class="align-middle">Ứng dụng</span>
                        </a>
                      </div>
                    </div>
                    <!-- In Page Menu End -->
                  </div>
                  <!-- Top Buttons End -->
                </div>
              </div>
              <!-- Title and Top Buttons End -->

              <!-- Public Info Start -->
              <h2 class="small-title">Đổi mật khẩu</h2>
              <div class="card mb-5">
                <div class="card-body">
                  <form id="changePassForm">

                    <div class="mb-3 row">
                      <label class="col-lg-2 col-md-3 col-sm-4 col-form-label">Mật khẩu hiện tại</label>
                      <div class="col-sm-8 col-md-9 col-lg-10">
                        <input type="password" class="form-control" name="password"  />
                      </div>
                    </div>

                    <div class="mb-3 row">
                      <label class="col-lg-2 col-md-3 col-sm-4 col-form-label">Mật khẩu mới</label>
                      <div class="col-sm-8 col-md-9 col-lg-10">
                        <input type="password" name="newpass" class="form-control" />
                      </div>
                    </div>

                    <div class="mb-3 row">
                      <label class="col-lg-2 col-md-3 col-sm-4 col-form-label">Nhập lại</label>
                      <div class="col-sm-8 col-md-9 col-lg-10">
                        <input type="password" class="form-control" name="repass"  />
                      </div>
                    </div>

                    
                   
                    <div class="mb-3 row mt-5">
                      <div class="col-sm-8 col-md-9 col-lg-10 ms-auto">
                        <button type="submit" class="btn btn-primary">Đổi mật khẩu</button>
                        <a style="margin-left: 10px" id="message"></a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- Public Info End -->

             

            
            </div>
          </div>
        </div>
      </main>
    
    <!-- Search Modal Start -->
    <div class="modal fade modal-under-nav modal-search modal-close-out" id="searchPagesModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header border-0 p-0">
            <button type="button" class="btn-close btn btn-icon btn-icon-only btn-foreground" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body ps-5 pe-5 pb-0 border-0">
            <input id="searchPagesInput" class="form-control form-control-xl borderless ps-0 pe-0 mb-1 auto-complete" type="text" autocomplete="off" />
          </div>
          <div class="modal-footer border-top justify-content-start ps-5 pe-5 pb-3 pt-3 border-0">
            <span class="text-alternate d-inline-block m-0 me-3">
              <i data-cs-icon="arrow-bottom" data-cs-size="15" class="text-alternate align-middle me-1"></i>
              <span class="align-middle text-medium">Navigate</span>
            </span>
            <span class="text-alternate d-inline-block m-0 me-3">
              <i data-cs-icon="arrow-bottom-left" data-cs-size="15" class="text-alternate align-middle me-1"></i>
              <span class="align-middle text-medium">Select</span>
            </span>
          </div>
        </div>
      </div>
    </div>
    <!-- Search Modal End -->

    <!-- Vendor Scripts Start -->
    <script src="${context}/resources/js/vendor/jquery-3.5.1.min.js"></script>
    <script src="${context}/resources/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="${context}/resources/js/vendor/OverlayScrollbars.min.js"></script>
    <script src="${context}/resources/js/vendor/autoComplete.min.js"></script>
    <script src="${context}/resources/js/vendor/clamp.min.js"></script>
    <script src="${context}/resources/js/vendor/movecontent.js"></script>
    <script src="${context}/resources/js/vendor/select2.full.min.js"></script>
    <!-- Vendor Scripts End -->

    <!-- Template Base Scripts Start -->
    <script src="${context}/resources/font/CS-Line/csicons.min.js"></script>
    <script src="${context}/resources/js/base/helpers.js"></script>
    <script src="${context}/resources/js/base/globals.js"></script>
    <script src="${context}/resources/js/base/nav.js"></script>
    <script src="${context}/resources/js/base/search.js"></script>
    <script src="${context}/resources/js/base/settings.js"></script>
    <script src="${context}/resources/js/base/init.js"></script>
    <!-- Template Base Scripts End -->
    <!-- Page Specific Scripts Start -->
    <script>/**
 *
 * ProfileSettings
 *
 * Pages.Profile.Settings.html page content scripts. Initialized from scripts.js file.
 *
 *
 */

class ProfileSecurity {
  constructor() {
    // Initialization of the page plugins
    this._initMoveContent();
    this._initForm();
  }

  // Moving menu for responsive sizes
  _initMoveContent() {
    if (document.querySelector('#settingsMoveContent') && typeof MoveContent !== 'undefined') {
      const menuMove = document.querySelector('#settingsMoveContent');
      const targetSelectorMenu = menuMove.getAttribute('data-move-target');
      const moveBreakpointMenu = menuMove.getAttribute('data-move-breakpoint');
      const menuMoveContent = new MoveContent(menuMove, {
        targetSelector: targetSelectorMenu,
        moveBreakpoint: moveBreakpointMenu,
      });
    }
  }

  
  _initForm() {
        const form = document.getElementById('changePassForm');
        if (!form) {
            return;
        }
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            event.stopPropagation();
          
                const formValues = {
                    password: form.querySelector('[name="password"]').value,
                    newpass: form.querySelector('[name="newpass"]').value,
                    repass: form.querySelector('[name="repass"]').value,
                    
                };
                $.ajax({
                    type: "POST",
                    data: formValues,
                    success: function (response) {
                        if (response == "Success") {
                            updateSuccess()
                        } else if (response == "Not match"){
                            updateFailed(1)
                        } else if (response == "Wrong old pass"){
                            updateFailed(2)
                        } else {
                            updateFailed(0)
                        }
                    }

                });
                return;
            
        });

        async function updateSuccess(response) {
            document.getElementById("message").style.color = "green";
            document.getElementById("message").innerHTML = "Cập nhật thành công!";
            setTimeout(function () {
                window.location.href = "security";
            }, 1000);
        }

        function updateFailed(code) {
            if (code == 0){
                
            document.getElementById("message").style.color = "red";
            document.getElementById("message").innerHTML = "Cập nhật thất bại";
        }
        if (code == 1){
                
            document.getElementById("message").style.color = "red";
            document.getElementById("message").innerHTML = "Vui lòng nhập lại mật khẩu mới";
        }
        if (code == 2){
                
            document.getElementById("message").style.color = "red";
            document.getElementById("message").innerHTML = "Sai mật khẩu cũ";
        }
        }
    }
}
</script>
    <script src="${context}/resources/js/common.js"></script>
    <script src="${context}/resources/js/scripts.js"></script>
    <!-- Page Specific Scripts End -->
  </body>
</html>
