<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>

<html lang="en">
    <c:set var="context" value="${pageContext.request.contextPath}"/>
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <title>Đăng ký</title>
        <meta name="description" content="Register Page"/>
        <!-- Favicon Tags Start -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57"
              href="${context}/resources/img/favicon/apple-touch-icon-57x57.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="114x114"
              href="${context}/resources/img/favicon/apple-touch-icon-114x114.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="72x72"
              href="${context}/resources/img/favicon/apple-touch-icon-72x72.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="144x144"
              href="${context}/resources/img/favicon/apple-touch-icon-144x144.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="60x60"
              href="${context}/resources/img/favicon/apple-touch-icon-60x60.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="120x120"
              href="${context}/resources/img/favicon/apple-touch-icon-120x120.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="76x76"
              href="${context}/resources/img/favicon/apple-touch-icon-76x76.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="152x152"
              href="${context}/resources/img/favicon/apple-touch-icon-152x152.png"/>
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-196x196.png" sizes="196x196"/>
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-96x96.png" sizes="96x96"/>
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-32x32.png" sizes="32x32"/>
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-16x16.png" sizes="16x16"/>
        <link rel="icon" type="image/png" href="${context}/resources/img/favicon/favicon-128.png" sizes="128x128"/>
        <meta name="application-name" content="&nbsp;"/>
        <meta name="msapplication-TileColor" content="#FFFFFF"/>
        <meta name="msapplication-TileImage" content="${context}/resources/img/favicon/mstile-144x144.png"/>
        <meta name="msapplication-square70x70logo" content="${context}/resources/img/favicon/mstile-70x70.png"/>
        <meta name="msapplication-square150x150logo" content="${context}/resources/img/favicon/mstile-150x150.png"/>
        <meta name="msapplication-wide310x150logo" content="${context}/resources/img/favicon/mstile-310x150.png"/>
        <meta name="msapplication-square310x310logo" content="${context}/resources/img/favicon/mstile-310x310.png"/>
        <!-- Favicon Tags End -->
        <!-- Font Tags Start -->
        <link rel="preconnect" href="https://fonts.gstatic.com"/>
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap"
              rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap"
              rel="stylesheet"/>
        <link rel="stylesheet" href="${context}/resources/font/CS-Interface/style.css"/>
        <!-- Font Tags End -->
        <!-- Vendor Styles Start -->
        <link rel="stylesheet" href="${context}/resources/css/vendor/bootstrap.min.css"/>
        <link rel="stylesheet" href="${context}/resources/css/vendor/OverlayScrollbars.min.css"/>

        <!-- Vendor Styles End -->
        <!-- Template Base Styles Start -->
        <link rel="stylesheet" href="${context}/resources/css/styles.css"/>
        <!-- Template Base Styles End -->

        <link rel="stylesheet" href="${context}/resources/css/main.css"/>
        <script src="${context}/resources/js/base/loader.js"></script>

        <!-- Captcha -->
        <script src="https://www.google.com/recaptcha/api.js?hl=en" async defer></script>


    </head>

    <body class="h-100">
        <div id="root" class="h-100">
            <!-- Background Start -->
            <div class="fixed-background"></div>
            <!-- Background End -->

            <div class="container-fluid p-0 h-100 position-relative">
                <div class="row g-0 h-100">
                    <!-- Left Side Start -->
                    <div class="offset-0 col-12 d-none d-lg-flex offset-md-1 col-lg h-lg-100">
                        <div class="min-h-100 d-flex align-items-center">
                            <div class="w-100 w-lg-75 w-xxl-50">
                                <div>
                                    <div class="mb-5">
                                        <h1 class="display-3 text-white">Đăng ký tài khoản</h1>

                                    </div>
                                    <p class="h6 text-white lh-1-5 mb-5">
                                        Đăng ký tài khoản ngay để có thể tham gia các khoá học chất lượng, độc quyền
                                        chỉ có trên hệ thống của chúng tôi.
                                    </p>
                                    <div class="mb-5">
                                        <a class="btn btn-lg btn-outline-white" href="home">Quay về trang chủ</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Left Side End -->

                    <!-- Right Side Start -->
                    <div class="col-12 col-lg-auto h-100 pb-4 px-4 pt-0 p-lg-0">
                        <div class="sw-lg-70 min-h-100 bg-foreground d-flex justify-content-center align-items-center shadow-deep py-5 full-page-content-right-border">
                            <div class="sw-lg-50 px-5">
                                <div class="sh-11">
                                    <a href="home">
                                        <div class="logo-default"></div>
                                    </a>
                                </div>
                                <div class="mb-5">
                                    <h2 class="cta-1 mb-0 text-primary">Hi,</h2>
                                    <h2 class="cta-1 text-primary">Cùng tham gia với Acorn nào!</h2>
                                </div>
                                <div class="mb-5">
                                    <p class="h6">Điền hết các thông tin dưới đây để đăng ký tài khoản.</p>
                                    <p class="h6">
                                        Nếu bạn đã có tài khoản, đăng nhập
                                        <a href="auth?action=login">tại đây</a>
                                        .
                                    </p>
                                </div>
                                <div>
                                    <form id="registerForm" class="tooltip-end-bottom" validate>
                                        <div class="mb-3 filled form-group tooltip-end-top">
                                            <i data-cs-icon="online-class"></i>
                                            <input class="form-control" name="yourname" type="text" id="yourname"
                                                   placeholder="Tên của bạn"/>
                                        </div>
                                        <div class="mb-3 filled form-group tooltip-end-top">
                                            <i data-cs-icon="email"></i>
                                            <input class="form-control" placeholder="Email" id="email" name="email"/>
                                        </div>
                                        <div class="mb-3 filled form-group tooltip-end-top">
                                            <i data-cs-icon="board-1"></i>
                                            <input class="form-control" name="username" type="text" id="username"
                                                   placeholder="Tên người dùng" autocomplete="off"/>
                                        </div>
                                        <div class="mb-3 filled form-group tooltip-end-top">
                                            <i data-cs-icon="key"></i>
                                            <input class="form-control" placeholder="Mật khẩu" id="password"
                                                   name="password" type="password" autocomplete="off"/>
                                        </div>
                                        <div class="mb-3 filled form-group tooltip-end-top">
                                            <i data-cs-icon="lock-off"></i>
                                            <input class="form-control" name="repass" type="password" id="repass"
                                                   placeholder="Nhập lại mật khẩu"/>
                                        </div>

                                        <div class="mb-3 position-relative form-group">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="registerCheck"
                                                       name="registerCheck"/>
                                                <label class="form-check-label" for="registerCheck">
                                                    Tôi đã đọc và đồng ý với các điều khoản.

                                                </label>
                                            </div>
                                        </div>
                                        <div class="mb-3 filled form-group tooltip-end-top">
                                            <div class="g-recaptcha"
                                                 data-sitekey="6LdUQWMcAAAAAEL_cBL5QMyn-tUaouSiK84m99nl"></div>
                                        </div>
                                        <button type="submit" id="regBut" class="btn btn-lg btn-primary">Đăng ký</button>
                                    </form>
                                    <div id="message">
                                        <p class="h6" id="message"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Right Side End -->
                </div>
            </div>
        </div>

        <!-- Vendor Scripts Start -->
        <script src="${context}/resources/js/vendor/jquery-3.5.1.min.js"></script>
        <script src="${context}/resources/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="${context}/resources/js/vendor/OverlayScrollbars.min.js"></script>
        <script src="${context}/resources/js/vendor/autoComplete.min.js"></script>
        <script src="${context}/resources/js/vendor/clamp.min.js"></script>
        <script src="${context}/resources/js/vendor/jquery.validate/jquery.validate.min.js"></script>
        <script src="${context}/resources/js/vendor/jquery.validate/additional-methods.min.js"></script>
        <!-- Vendor Scripts End -->

        <!-- Template Base Scripts Start -->
        <script src="${context}/resources/font/CS-Line/csicons.min.js"></script>
        <script src="${context}/resources/js/base/helpers.js"></script>
        <script src="${context}/resources/js/base/globals.js"></script>
        <script src="${context}/resources/js/base/nav.js"></script>
        <script src="${context}/resources/js/base/search.js"></script>
        <script src="${context}/resources/js/base/settings.js"></script>
        <script src="${context}/resources/js/base/init.js"></script>
        <!-- Template Base Scripts End -->
        <!-- Page Specific Scripts Start -->
        <script src="${context}/resources/js/pages/auth.register.js"></script>
        <script src="${context}/resources/js/common.js"></script>
        <script src="${context}/resources/js/scripts.js"></script>
        <!-- Page Specific Scripts End -->


        <!-- SweetAlert-->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.5/dist/sweetalert2.all.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@sweetalert2/themes@5.0.4/default/default.min.css">

    </body>
</html>
