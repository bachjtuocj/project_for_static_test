<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html
    lang="en"
    data-footer="true"
    data-override='{"attributes": {"placement": "vertical","layout": "boxed", "behaviour": "unpinned" }, "storagePrefix": "elearning-portal"}'
    >
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>Acorn Admin Template | Course Detail</title>
        <meta name="description" content="Acorn elearning platform course detail." />

        <!-- Favicon Tags Start -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="img/favicon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="img/favicon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="img/favicon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="img/favicon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="img/favicon/mstile-310x310.png" />
        <!-- Favicon Tags End -->

        <!-- Font Tags Start -->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font/CS-Interface/style.css" />
        <!-- Font Tags End -->
        <!-- Vendor Styles Start -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/OverlayScrollbars.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/plyr.css" />
        <!-- Vendor Styles End -->

        <!-- Template Base Styles Start -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" />
        <!-- Template Base Styles End -->

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
        <!-- Page Specific Style Start -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/courseDetail.css" />
        <!-- Page Specific Style End -->
        <script src="${pageContext.request.contextPath}/resources/js/base/loader.js"></script>
    </head>

    <body>
        <div id="root">
            <div id="nav" class="nav-container d-flex" data-horizontal-mobile="1500">
                <div class="nav-content d-flex">
                    <!-- Logo Start -->
                    <div class="logo position-relative">
                        <a href="index.html">
                            <!-- Logo can be added directly -->
                            <!-- <img src="${pageContext.request.contextPath}/resources/img/logo/logo-white.svg" alt="logo" /> -->

                            <!-- Or added via css to provide different ones for different color themes -->
                            <div class="img"></div>
                        </a>
                    </div>
                    <!-- Logo End -->

                    <!-- Language Switch Start -->
                    <div class="language-switch-container">
                        <button class="btn btn-empty language-button dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">EN</button>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item">DE</a>
                            <a href="#" class="dropdown-item active">EN</a>
                            <a href="#" class="dropdown-item">ES</a>
                        </div>
                    </div>
                    <!-- Language Switch End -->

                    <!-- User Menu Start -->
                    <div class="user-container d-flex">
                        <a href="#" class="d-flex user position-relative" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="profile" alt="profile" src="${pageContext.request.contextPath}/resources/img/profile/profile-9.jpg" />
                            <div class="name">Lisa Jackson</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end user-menu wide">
                            <div class="row mb-3 ms-0 me-0">
                                <div class="col-12 ps-1 mb-2">
                                    <div class="text-extra-small text-primary">ACCOUNT</div>
                                </div>
                                <div class="col-6 ps-1 pe-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">User Info</a>
                                        </li>
                                        <li>
                                            <a href="#">Preferences</a>
                                        </li>
                                        <li>
                                            <a href="#">Calendar</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6 pe-1 ps-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">Security</a>
                                        </li>
                                        <li>
                                            <a href="#">Billing</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-1 ms-0 me-0">
                                <div class="col-12 p-1 mb-2 pt-2">
                                    <div class="text-extra-small text-primary">APPLICATION</div>
                                </div>
                                <div class="col-6 ps-1 pe-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">Themes</a>
                                        </li>
                                        <li>
                                            <a href="#">Language</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6 pe-1 ps-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">Devices</a>
                                        </li>
                                        <li>
                                            <a href="#">Storage</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-1 ms-0 me-0">
                                <div class="col-12 p-1 mb-3 pt-3">
                                    <div class="separator-light"></div>
                                </div>
                                <div class="col-6 ps-1 pe-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">
                                                <i data-cs-icon="help" class="me-2" data-cs-size="17"></i>
                                                <span class="align-middle">Help</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i data-cs-icon="file-text" class="me-2" data-cs-size="17"></i>
                                                <span class="align-middle">Docs</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6 pe-1 ps-1">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="#">
                                                <i data-cs-icon="gear" class="me-2" data-cs-size="17"></i>
                                                <span class="align-middle">Settings</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i data-cs-icon="logout" class="me-2" data-cs-size="17"></i>
                                                <span class="align-middle">Logout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- User Menu End -->

                    <!-- Icons Menu Start -->
                    <ul class="list-unstyled list-inline text-center menu-icons">
                        <li class="list-inline-item">
                            <a href="#" data-bs-toggle="modal" data-bs-target="#searchPagesModal">
                                <i data-cs-icon="search" data-cs-size="18"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" id="pinButton" class="pin-button">
                                <i data-cs-icon="lock-on" class="unpin" data-cs-size="18"></i>
                                <i data-cs-icon="lock-off" class="pin" data-cs-size="18"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" id="colorButton">
                                <i data-cs-icon="light-on" class="light" data-cs-size="18"></i>
                                <i data-cs-icon="light-off" class="dark" data-cs-size="18"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" data-bs-toggle="dropdown" data-bs-target="#notifications" aria-haspopup="true" aria-expanded="false" class="notification-button">
                                <div class="position-relative d-inline-flex">
                                    <i data-cs-icon="bell" data-cs-size="18"></i>
                                    <span class="position-absolute notification-dot rounded-xl"></span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end wide notification-dropdown scroll-out" id="notifications">
                                <div class="scroll">
                                    <ul class="list-unstyled border-last-none">
                                        <li class="mb-3 pb-3 border-bottom border-separator-light d-flex">
                                            <img src="${pageContext.request.contextPath}/resources/img/profile/profile-1.jpg" class="me-3 sw-4 sh-4 rounded-xl align-self-center" alt="..." />
                                            <div class="align-self-center">
                                                <a href="#">Joisse Kaycee just sent a new comment!</a>
                                            </div>
                                        </li>
                                        <li class="mb-3 pb-3 border-bottom border-separator-light d-flex">
                                            <img src="${pageContext.request.contextPath}/resources/img/profile/profile-2.jpg" class="me-3 sw-4 sh-4 rounded-xl align-self-center" alt="..." />
                                            <div class="align-self-center">
                                                <a href="#">New order received! It is total $147,20.</a>
                                            </div>
                                        </li>
                                        <li class="mb-3 pb-3 border-bottom border-separator-light d-flex">
                                            <img src="${pageContext.request.contextPath}/resources/img/profile/profile-3.jpg" class="me-3 sw-4 sh-4 rounded-xl align-self-center" alt="..." />
                                            <div class="align-self-center">
                                                <a href="#">3 items just added to wish list by a user!</a>
                                            </div>
                                        </li>
                                        <li class="pb-3 pb-3 border-bottom border-separator-light d-flex">
                                            <img src="${pageContext.request.contextPath}/resources/img/profile/profile-6.jpg" class="me-3 sw-4 sh-4 rounded-xl align-self-center" alt="..." />
                                            <div class="align-self-center">
                                                <a href="#">Kirby Peters just sent a new message!</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- Icons Menu End -->

                    <!-- Menu Start -->
                    <div class="menu-container flex-grow-1">
                        <ul id="menu" class="menu">
                            <li>
                                <a href="#dashboards">
                                    <i data-cs-icon="home-garage" class="icon" data-cs-size="18"></i>
                                    <span class="label">Dashboards</span>
                                </a>
                                <ul id="dashboards">
                                    <li>
                                        <a href="Dashboards.Elearning.html">
                                            <span class="label">Elearning</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Dashboards.School.html">
                                            <span class="label">School</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#courses">
                                    <i data-cs-icon="online-class" class="icon" data-cs-size="18"></i>
                                    <span class="label">Courses</span>
                                </a>
                                <ul id="courses">
                                    <li>
                                        <a href="Course.Explore.html">
                                            <span class="label">Explore</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/course/list">
                                            <span class="label">List</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Course.Detail.html">
                                            <span class="label">Detail</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#quiz">
                                    <i data-cs-icon="quiz" class="icon" data-cs-size="18"></i>
                                    <span class="label">Quiz</span>
                                </a>
                                <ul id="quiz">
                                    <li>
                                        <a href="">
                                            <span class="label">List</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Quiz.Detail.html">
                                            <span class="label">Detail</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Quiz.Result.html">
                                            <span class="label">Result</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#paths">
                                    <i data-cs-icon="destination" class="icon" data-cs-size="18"></i>
                                    <span class="label">Paths</span>
                                </a>
                                <ul id="paths">
                                    <li>
                                        <a href="Path.List.html">
                                            <span class="label">List</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Path.Detail.html">
                                            <span class="label">Detail</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#instructors">
                                    <i data-cs-icon="lecture" class="icon" data-cs-size="18"></i>
                                    <span class="label">Instructors</span>
                                </a>
                                <ul id="instructors">
                                    <li>
                                        <a href="Instructor.List.html">
                                            <span class="label">List</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Instructor.Detail.html">
                                            <span class="label">Detail</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#miscellaneous">
                                    <i data-cs-icon="layout-5" class="icon" data-cs-size="18"></i>
                                    <span class="label">Miscellaneous</span>
                                </a>
                                <ul id="miscellaneous">
                                    <li>
                                        <a href="Misc.Player.html">
                                            <span class="label">Player</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Misc.Material.html">
                                            <span class="label">Material</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Misc.Syllabus.html">
                                            <span class="label">Syllabus</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- Menu End -->

                    <!-- Mobile Buttons Start -->
                    <div class="mobile-buttons-container">
                        <!-- Scrollspy Mobile Button Start -->
                        <a href="#" id="scrollSpyButton" class="spy-button" data-bs-toggle="dropdown">
                            <i data-cs-icon="menu-dropdown"></i>
                        </a>
                        <!-- Scrollspy Mobile Button End -->

                        <!-- Scrollspy Mobile Dropdown Start -->
                        <div class="dropdown-menu dropdown-menu-end" id="scrollSpyDropdown"></div>
                        <!-- Scrollspy Mobile Dropdown End -->

                        <!-- Menu Button Start -->
                        <a href="#" id="mobileMenuButton" class="menu-button">
                            <i data-cs-icon="menu"></i>
                        </a>
                        <!-- Menu Button End -->
                    </div>
                    <!-- Mobile Buttons End -->
                </div>
                <div class="nav-shadow"></div>
            </div>

            <main>
                <div class="container">
                    <!-- Title and Top Buttons Start -->
                    <div class="page-title-container">
                        <div class="row">
                            <!-- Title Start -->
                            <div class="col-12 col-sm-6">
                                <h1 class="mb-0 pb-0 display-4" id="title">${course.name}</h1>
                                <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
                                    <ul class="breadcrumb pt-0">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item"><a href="Course.Explore.html">Courses</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- Title End -->

                            <!-- Top Buttons Start -->
                            <div class="col-12 col-sm-6 d-flex align-items-start justify-content-end">
                                <!-- Start Now Start -->
                                <a href="#" class="btn btn-primary btn-icon btn-icon-start w-100 w-sm-auto">
                                    <i data-cs-icon="chevron-right"></i>
                                    <span>Enroll</span>
                                </a>
                                <!-- Start Now End -->
                            </div>
                            <!-- Top Buttons End -->
                        </div>
                    </div>
                    <!-- Title and Top Buttons End -->

                    <!-- Content Start -->

                    <div class="row">
                        <!-- Left Side Start -->
                        <div class="col-12 col-xxl-8 mb-5">
                            <!-- Preview Start -->
                            <h2 class="small-title">Preview</h2>
                            <div class="card mb-5">
                                <div class="card-img-top">
                                    <video class="player sh-50 cover" poster="${pageContext.request.contextPath}/resources/img/product/large/bread.jpg" id="videoPlayer">
                                        <source src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4" type="video/mp4" />
                                    </video>
                                </div>
                                <div class="card-body">
                                    <h6>Decription</h6>
                                    <p class="mb-4">
                                        ${course.description}
                                    </p>
                                </div>
                                <div class="card-footer border-0 pt-0">
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <div class="d-flex align-items-center">
                                                <div class="sw-5 d-inline-block position-relative me-3">
                                                    <img src="${pageContext.request.contextPath}/resources/img/profile/profile-1.jpg" class="img-fluid rounded-xl" alt="thumb" />
                                                </div>
                                                <div class="d-inline-block">
                                                    <a href="#">Olli Hawkins</a>
                                                    <div class="text-muted text-small">Development Lead</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 text-muted">
                                            <div class="row g-0 justify-content-end">
                                                <div class="col-auto pe-3">
                                                    <i data-cs-icon="eye" class="text-primary me-1" data-cs-size="15"></i>
                                                    <span class="align-middle">4.8K</span>
                                                </div>
                                                <div class="col-auto">
                                                    <i data-cs-icon="message" class="text-primary me-1" data-cs-size="15"></i>
                                                    <span class="align-middle">12</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Preview End -->

                            <!-- Table of Contents Start -->
                            <h2 class="small-title">Table of Contents</h2>
                            <div class="card mb-5">
                                <div class="card-body">
                                    <div class="row g-0">
                                        <div class="col-auto sw-1 d-flex flex-column justify-content-center align-items-center position-relative me-4">
                                            <div class="w-100 d-flex sh-1"></div>
                                            <div class="rounded-xl shadow d-flex flex-shrink-0 justify-content-center align-items-center">
                                                <div class="bg-gradient-primary sw-1 sh-1 rounded-xl position-relative"></div>
                                            </div>
                                            <div class="w-100 d-flex h-100 justify-content-center position-relative">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                        </div>
                                        <div class="col mb-4">
                                            <div class="h-100">
                                                <div class="d-flex flex-column justify-content-start">
                                                    <div class="d-flex flex-column">
                                                        <p class="heading">01. Dashboards</p>
                                                        <ul class="list-unstyled">
                                                            <li>- Danish brownie fruitcake tootsie</li>
                                                            <li>- Fruitcake tart dessert</li>
                                                            <li>- Bar carrot cake</li>
                                                            <li>- Snaps muffin macaroon tiramisu</li>
                                                            <li>- Ice cream marshmallow</li>
                                                            <li>- Plum caramels fruitcake</li>
                                                            <li>- Chocolate bar carrot cake</li>
                                                            <li>- Cotton candy gummies</li>
                                                            <li>- Danish cake gummies jelly</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0">
                                        <div class="col-auto sw-1 d-flex flex-column justify-content-center align-items-center position-relative me-4">
                                            <div class="w-100 d-flex sh-1 position-relative justify-content-center">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                            <div class="rounded-xl shadow d-flex flex-shrink-0 justify-content-center align-items-center">
                                                <div class="bg-gradient-primary sw-1 sh-1 rounded-xl position-relative"></div>
                                            </div>
                                            <div class="w-100 d-flex h-100 justify-content-center position-relative">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                        </div>
                                        <div class="col mb-4">
                                            <div class="h-100">
                                                <div class="d-flex flex-column">
                                                    <p class="heading">02. Applications</p>
                                                    <ul class="list-unstyled">
                                                        <li>- Ice cream marshmallow</li>
                                                        <li>- Plum caramels fruitcake</li>
                                                        <li>- Chocolate bar carrot cake</li>
                                                        <li>- Danish brownie fruitcake tootsie</li>
                                                        <li>- Fruitcake tart dessert</li>
                                                        <li>- Bar carrot cake</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0">
                                        <div class="col-auto sw-1 d-flex flex-column justify-content-center align-items-center position-relative me-4">
                                            <div class="w-100 d-flex sh-1 position-relative justify-content-center">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                            <div class="rounded-xl shadow d-flex flex-shrink-0 justify-content-center align-items-center">
                                                <div class="bg-gradient-primary sw-1 sh-1 rounded-xl position-relative"></div>
                                            </div>
                                            <div class="w-100 d-flex h-100 justify-content-center position-relative">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                        </div>
                                        <div class="col mb-4">
                                            <div class="h-100">
                                                <div class="d-flex flex-column">
                                                    <p class="heading">03. Interface</p>
                                                    <ul class="list-unstyled">
                                                        <li>- Ice cream marshmallow</li>
                                                        <li>- Plum caramels fruitcake</li>
                                                        <li>- Chocolate bar carrot cake</li>
                                                        <li>- Danish brownie fruitcake tootsie</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0">
                                        <div class="col-auto sw-1 d-flex flex-column justify-content-center align-items-center position-relative me-4">
                                            <div class="w-100 d-flex sh-1 position-relative justify-content-center">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                            <div class="rounded-xl shadow d-flex flex-shrink-0 justify-content-center align-items-center">
                                                <div class="bg-gradient-primary sw-1 sh-1 rounded-xl position-relative"></div>
                                            </div>
                                            <div class="w-100 d-flex h-100 justify-content-center position-relative">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                        </div>
                                        <div class="col mb-4">
                                            <div class="h-100">
                                                <div class="d-flex flex-column">
                                                    <p class="heading">04. Conclusion</p>
                                                    <ul class="list-unstyled">
                                                        <li>- Chocolate bar carrot cake</li>
                                                        <li>- Danish brownie fruitcake tootsie</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0">
                                        <div class="col-auto sw-1 d-flex flex-column justify-content-center align-items-center position-relative me-4">
                                            <div class="w-100 d-flex sh-1 position-relative justify-content-center">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                            <div class="rounded-xl shadow d-flex flex-shrink-0 justify-content-center align-items-center">
                                                <div class="bg-gradient-primary sw-1 sh-1 rounded-xl position-relative"></div>
                                            </div>
                                            <div class="w-100 d-flex h-100 justify-content-center position-relative">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                        </div>
                                        <div class="col mb-4">
                                            <div class="h-100">
                                                <div class="d-flex flex-column">
                                                    <p class="heading">05. What is Next?</p>
                                                    <ul class="list-unstyled">
                                                        <li>- Danish brownie fruitcake tootsie</li>
                                                        <li>- Fruitcake tart dessert</li>
                                                        <li>- Bar carrot cake</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0">
                                        <div class="col-auto sw-1 d-flex flex-column justify-content-center align-items-center position-relative me-4">
                                            <div class="w-100 d-flex sh-1 position-relative justify-content-center">
                                                <div class="line-w-1 bg-separator h-100 position-absolute"></div>
                                            </div>
                                            <div class="rounded-xl shadow d-flex flex-shrink-0 justify-content-center align-items-center">
                                                <div class="bg-gradient-primary sw-1 sh-1 rounded-xl position-relative"></div>
                                            </div>
                                            <div class="w-100 d-flex h-100 justify-content-center position-relative"></div>
                                        </div>
                                        <div class="col">
                                            <div class="h-100">
                                                <div class="d-flex flex-column justify-content-start">
                                                    <div class="d-flex flex-column">
                                                        <p class="heading">06. Credits</p>
                                                        <ul class="list-unstyled">
                                                            <li>- Chocolate bar carrot cake</li>
                                                            <li>- Danish brownie fruitcake tootsie</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Table of Contents End -->

                            <!-- Reviews Start -->
                            <h2 class="small-title">Reviews</h2>
                            <div class="card">
                                <div class="card-body">
                                    <!-- Total Rating Start -->
                                    <div class="row mb-5">
                                        <div class="col-12 col-sm-auto mb-3 mb-sm-0">
                                            <div
                                                class="w-100 sw-sm-19 sw-md-23 border border-separator-light sh-16 rounded-md d-flex flex-column align-items-center justify-content-center"
                                                >
                                                <div class="cta-1 text-alternate mb-2">${course.rating}</div>
                                                <div>
                                                    <div class="br-wrapper br-theme-cs-icon d-inline-block mb-2">
                                                        <select class="rating" name="rating" autocomplete="off" data-readonly="true" data-initial-rating="5">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                    <span class="text-muted">(22)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <div class="progress mb-1">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="me-3 text-muted text-small">%80</span>
                                                    <div class="br-wrapper br-theme-cs-icon d-inline-block mb-2">
                                                        <select class="rating" name="rating" autocomplete="off" data-readonly="true" data-initial-rating="5">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <div class="progress mb-1">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="me-3 text-muted text-small">%10</span>
                                                    <div class="br-wrapper br-theme-cs-icon d-inline-block mb-2">
                                                        <select class="rating" name="rating" autocomplete="off" data-readonly="true" data-initial-rating="4">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <div class="progress mb-1">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="me-3 text-muted text-small">%5</span>
                                                    <div class="br-wrapper br-theme-cs-icon d-inline-block mb-2">
                                                        <select class="rating" name="rating" autocomplete="off" data-readonly="true" data-initial-rating="3">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <div class="progress mb-1">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="me-3 text-muted text-small">%0</span>
                                                    <div class="br-wrapper br-theme-cs-icon d-inline-block mb-2">
                                                        <select class="rating" name="rating" autocomplete="off" data-readonly="true" data-initial-rating="2">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <div class="progress mb-1">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="me-3 text-muted text-small">%5</span>
                                                    <div class="br-wrapper br-theme-cs-icon d-inline-block mb-2">
                                                        <select class="rating" name="rating" autocomplete="off" data-readonly="true" data-initial-rating="1">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Total Rating End -->

                                    <!-- Server response review start -->
                                    <div id ="review-contain"></div>
                                    <!-- Server response review end -->

                                    <!-- Comments Start -->
                                    <c:forEach var="o" items="${courseReviewList}" >
                                        <div class="d-flex align-items-center border-bottom border-separator-light pb-3">
                                            <div class="row g-0 w-100">
                                                <div class="col-auto">
                                                    <div class="sw-5 me-3">
                                                        <img src="${pageContext.request.contextPath}/resources/img/profile/profile-1.jpg" class="img-fluid rounded-xl" alt="thumb" />
                                                    </div>
                                                </div>
                                                <div class="col pe-3">
                                                    <div>Cherish Kerr</div>
                                                    <div class="text-muted text-small mb-2">2 days ago</div>
                                                    <div class="br-wrapper br-theme-cs-icon d-inline-block mb-2">
                                                        <select class="rating" name="rating" autocomplete="off" data-readonly="true" data-initial-rating="${o.rating}">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                    <div class="text-medium text-alternate lh-1-25">${o.content}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>

                                    <!--Review form start-->
                                    <form id="reviewForm" class="pb-3 reviewForm" novalidate="novalidate">
                                        <!--Review input form start-->
                                        <div class="d-flex align-items-center mt-3 ReviewInput">
                                            <div class="col-auto">
                                                <div class="sw-5 me-3">
                                                    <img src="/resources/img/profile/profile-3.jpg" class="img-fluid rounded-xl" alt="thumb">
                                                </div>
                                            </div>
                                            <div class="col pe-3">
                                                <input type="text" class="form-control" placeholder="Enter your though" name="content">
                                            </div>
                                        </div>
                                        <p id="message" style="color:red; margin-left: 56px; width:150px" class="mt-2"></p>
                                        <!--Review form end-->

                                        <!--Rating component start-->
                                        <div class="col-12 col-sm-6 mb-3 mt-3 mb-sm-0 RatingBar">
                                            <div class="mb-1">
                                                <label class="d-block form-label m-0">Đánh giá của bạn</label>
                                                <div class="br-wrapper br-theme-cs-icon">
                                                    <div class="br-wrapper">
                                                        <select name="rating" autocomplete="off" data-initial-rating="5" id="ratingBar" style="display: none;">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Rating component end-->
                                    </form>
                                    <!-- Comments End -->
                                </div>
                            </div>
                            <!-- Reviews End -->
                        </div>
                        <!-- Left Side End -->

                        <!-- Right Side Start -->
                        <div class="col-12 col-xxl-4 mb-n5">
                            <!-- At a Glance Start -->
                            <h2 class="small-title">At a Glance</h2>
                            <div class="card mb-5">
                                <div class="card-body">
                                    <div class="row g-0 align-items-center mb-3">
                                        <div class="col-auto">
                                            <div class="sw-3 sh-4 d-flex justify-content-center align-items-center">
                                                <i data-cs-icon="clock" class="text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="col ps-3">
                                            <div class="row g-0">
                                                <div class="col">
                                                    <div class="sh-4 d-flex align-items-center lh-1-25">Duration</div>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="sh-4 d-flex align-items-center text-alternate">${course.duration} Hours</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0 align-items-center mb-3">
                                        <div class="col-auto">
                                            <div class="sw-3 sh-4 d-flex justify-content-center align-items-center">
                                                <i data-cs-icon="presentation" class="text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="col ps-3">
                                            <div class="row g-0">
                                                <div class="col">
                                                    <div class="sh-4 d-flex align-items-center lh-1-25">Content</div>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="sh-4 d-flex align-items-center text-alternate">8 Chapters</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0 align-items-center mb-3">
                                        <div class="col-auto">
                                            <div class="sw-3 sh-4 d-flex justify-content-center align-items-center">
                                                <i data-cs-icon="diploma" class="text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="col ps-3">
                                            <div class="row g-0">
                                                <div class="col">
                                                    <div class="sh-4 d-flex align-items-center lh-1-25">Level</div>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="sh-4 d-flex align-items-center text-alternate">${course.level}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0 align-items-center mb-3">
                                        <div class="col-auto">
                                            <div class="sw-3 sh-4 d-flex justify-content-center align-items-center">
                                                <i data-cs-icon="calendar" class="text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="col ps-3">
                                            <div class="row g-0">
                                                <div class="col">
                                                    <div class="sh-4 d-flex align-items-center lh-1-25">Release</div>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="sh-4 d-flex align-items-center text-alternate">05.11.2021</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0 align-items-center mb-3">
                                        <div class="col-auto">
                                            <div class="sw-3 sh-4 d-flex justify-content-center align-items-center">
                                                <i data-cs-icon="star" class="text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="col ps-3">
                                            <div class="row g-0">
                                                <div class="col">
                                                    <div class="sh-4 d-flex align-items-center lh-1-25">Rating</div>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="sh-4 d-flex align-items-center text-alternate">${course.rating}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-0 align-items-center mb-0">
                                        <div class="col-auto">
                                            <div class="sw-3 sh-4 d-flex justify-content-center align-items-center">
                                                <i data-cs-icon="graduation" class="text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="col ps-3">
                                            <div class="row g-0">
                                                <div class="col">
                                                    <div class="sh-4 d-flex align-items-center lh-1-25">Completed By</div>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="sh-4 d-flex align-items-center text-alternate">1.522</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- At a Glance End -->

                            <!-- Tags Start -->
                            <h2 class="small-title">Topic</h2>
                            <div class="card mb-5">
                                <div class="card-body">
                                    <button class="btn btn-sm btn-icon btn-icon-end btn-outline-alternate mb-1 me-1" type="button">
                                        <span>Food (12)</span>
                                    </button>
                                    <button class="btn btn-sm btn-icon btn-icon-end btn-outline-alternate mb-1 me-1" type="button">
                                        <span>Baking (3)</span>
                                    </button>
                                    <button class="btn btn-sm btn-icon btn-icon-end btn-outline-alternate mb-1 me-1" type="button">
                                        <span>Sweet (1)</span>
                                    </button>
                                    <button class="btn btn-sm btn-icon btn-icon-end btn-outline-alternate mb-1 me-1" type="button">
                                        <span>Molding (3)</span>
                                    </button>
                                    <button class="btn btn-sm btn-icon btn-icon-end btn-outline-alternate mb-1 me-1" type="button">
                                        <span>Pastry (5)</span>
                                    </button>
                                    <button class="btn btn-sm btn-icon btn-icon-end btn-outline-alternate mb-1 me-1" type="button">
                                        <span>Healthy (7)</span>
                                    </button>
                                    <button class="btn btn-sm btn-icon btn-icon-end btn-outline-alternate mb-1 me-1" type="button">
                                        <span>Rye (3)</span>
                                    </button>
                                    <button class="btn btn-sm btn-icon btn-icon-end btn-outline-alternate mb-1 me-1" type="button">
                                        <span>Simple (3)</span>
                                    </button>
                                </div>
                            </div>
                            <!-- Tags End -->

                            <!-- Badges Start -->
                            <h2 class="small-title">Badges</h2>
                            <div class="card mb-2 sh-15">
                                <div class="card-body text-center align-items-center d-flex flex-row">
                                    <div class="d-flex sw-6 sh-6 bg-gradient-primary align-items-center justify-content-center rounded-xl position-relative ms-1">
                                        <i data-cs-icon="burger" class="text-white"></i>
                                        <div class="achievement bg position-absolute">
                                            <svg width="75" height="75" viewBox="0 0 75 75" fill="black" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                fill-rule="evenodd"
                                                clip-rule="evenodd"
                                                d="M15.3422 7.24333C21.5482 2.69119 29.2117 0 37.5 0C45.7883 0 53.4518 2.69119 59.6578 7.24333C60.5562 7.90233 60.7431 9.15516 60.0752 10.0416C59.4073 10.9281 58.1375 11.1124 57.2391 10.4534C51.7048 6.39402 44.8833 4 37.5 4C30.1167 4 23.2952 6.39403 17.7609 10.4535C16.8625 11.1124 15.5927 10.9281 14.9248 10.0416C14.2569 9.15516 14.4438 7.90233 15.3422 7.24333ZM65.0942 15.1001C66.006 14.4592 67.2717 14.6688 67.9213 15.5684C72.3763 21.7377 75 29.3164 75 37.5C75 45.6836 72.3763 53.2623 67.9213 59.4316C67.2717 60.3311 66.006 60.5408 65.0942 59.8999C64.1825 59.2589 63.97 58.0101 64.6196 57.1105C68.599 51.5998 70.9459 44.8284 70.9459 37.5C70.9459 30.1716 68.599 23.4002 64.6196 17.8895C63.97 16.9899 64.1825 15.7411 65.0942 15.1001ZM9.90579 15.1001C10.8175 15.7411 11.03 16.9899 10.3804 17.8895C6.40105 23.4002 4.05405 30.1716 4.05405 37.5C4.05405 44.8284 6.40105 51.5998 10.3804 57.1105C11.03 58.0101 10.8175 59.2589 9.90579 59.8999C8.99405 60.5408 7.72832 60.3312 7.07871 59.4316C2.62373 53.2623 0 45.6836 0 37.5C0 29.3164 2.62373 21.7377 7.07871 15.5684C7.72832 14.6689 8.99404 14.4592 9.90579 15.1001ZM14.9248 64.9584C15.5927 64.0719 16.8625 63.8876 17.7609 64.5466C23.2952 68.606 30.1167 71 37.5 71C44.8833 71 51.7048 68.606 57.2391 64.5465C58.1375 63.8876 59.4073 64.0719 60.0752 64.9584C60.7431 65.8448 60.5562 67.0977 59.6578 67.7567C53.4518 72.3088 45.7883 75 37.5 75C29.2117 75 21.5483 72.3088 15.3422 67.7567C14.4438 67.0977 14.2569 65.8448 14.9248 64.9584Z"
                                                />
                                            </svg>
                                        </div>
                                        <div class="achievement level position-absolute">
                                            <svg width="75" height="75" viewBox="0 0 75 75" fill="black" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                fill-rule="evenodd"
                                                clip-rule="evenodd"
                                                d="M15.3422 7.24333C21.5482 2.69119 29.2117 0 37.5 0C45.7883 0 53.4517 2.69119 59.6578 7.24333C60.5562 7.90233 60.7431 9.15516 60.0752 10.0416C59.4073 10.9281 58.1375 11.1124 57.2391 10.4534C51.7048 6.39402 44.8833 4 37.5 4C30.1167 4 23.2952 6.39403 17.7609 10.4535C16.8625 11.1124 15.5927 10.9281 14.9248 10.0416C14.2569 9.15516 14.4438 7.90233 15.3422 7.24333Z"
                                                />
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="ms-5 text-start">
                                        <p class="mb-1">Sandwich Novice</p>
                                        <p class="mb-0 text-primary">Level 1</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-2 sh-15">
                                <div class="card-body text-center align-items-center d-flex flex-row">
                                    <div class="d-flex sw-6 sh-6 bg-gradient-primary align-items-center justify-content-center rounded-xl position-relative ms-1">
                                        <i data-cs-icon="cupcake" class="text-white"></i>
                                        <div class="achievement bg position-absolute">
                                            <svg width="75" height="75" viewBox="0 0 75 75" fill="black" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                fill-rule="evenodd"
                                                clip-rule="evenodd"
                                                d="M15.3422 7.24333C21.5482 2.69119 29.2117 0 37.5 0C45.7883 0 53.4518 2.69119 59.6578 7.24333C60.5562 7.90233 60.7431 9.15516 60.0752 10.0416C59.4073 10.9281 58.1375 11.1124 57.2391 10.4534C51.7048 6.39402 44.8833 4 37.5 4C30.1167 4 23.2952 6.39403 17.7609 10.4535C16.8625 11.1124 15.5927 10.9281 14.9248 10.0416C14.2569 9.15516 14.4438 7.90233 15.3422 7.24333ZM65.0942 15.1001C66.006 14.4592 67.2717 14.6688 67.9213 15.5684C72.3763 21.7377 75 29.3164 75 37.5C75 45.6836 72.3763 53.2623 67.9213 59.4316C67.2717 60.3311 66.006 60.5408 65.0942 59.8999C64.1825 59.2589 63.97 58.0101 64.6196 57.1105C68.599 51.5998 70.9459 44.8284 70.9459 37.5C70.9459 30.1716 68.599 23.4002 64.6196 17.8895C63.97 16.9899 64.1825 15.7411 65.0942 15.1001ZM9.90579 15.1001C10.8175 15.7411 11.03 16.9899 10.3804 17.8895C6.40105 23.4002 4.05405 30.1716 4.05405 37.5C4.05405 44.8284 6.40105 51.5998 10.3804 57.1105C11.03 58.0101 10.8175 59.2589 9.90579 59.8999C8.99405 60.5408 7.72832 60.3312 7.07871 59.4316C2.62373 53.2623 0 45.6836 0 37.5C0 29.3164 2.62373 21.7377 7.07871 15.5684C7.72832 14.6689 8.99404 14.4592 9.90579 15.1001ZM14.9248 64.9584C15.5927 64.0719 16.8625 63.8876 17.7609 64.5466C23.2952 68.606 30.1167 71 37.5 71C44.8833 71 51.7048 68.606 57.2391 64.5465C58.1375 63.8876 59.4073 64.0719 60.0752 64.9584C60.7431 65.8448 60.5562 67.0977 59.6578 67.7567C53.4518 72.3088 45.7883 75 37.5 75C29.2117 75 21.5483 72.3088 15.3422 67.7567C14.4438 67.0977 14.2569 65.8448 14.9248 64.9584Z"
                                                />
                                            </svg>
                                        </div>
                                        <div class="achievement level position-absolute">
                                            <svg width="75" height="75" viewBox="0 0 75 75" fill="black" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                fill-rule="evenodd"
                                                clip-rule="evenodd"
                                                d="M15.3422 7.24333C21.5482 2.69119 29.2117 0 37.5 0C45.7883 0 53.4517 2.69119 59.6578 7.24333C60.5562 7.90233 60.7431 9.15516 60.0752 10.0416C59.4073 10.9281 58.1375 11.1124 57.2391 10.4534C51.7048 6.39402 44.8833 4 37.5 4C30.1167 4 23.2952 6.39403 17.7609 10.4535C16.8625 11.1124 15.5927 10.9281 14.9248 10.0416C14.2569 9.15516 14.4438 7.90233 15.3422 7.24333ZM65.0942 15.1001C66.006 14.4592 67.2717 14.6688 67.9213 15.5684C72.3763 21.7377 75 29.3164 75 37.5C75 45.6836 72.3763 53.2623 67.9213 59.4316C67.2717 60.3311 66.006 60.5408 65.0942 59.8999C64.1825 59.2589 63.97 58.0101 64.6196 57.1105C68.599 51.5998 70.9459 44.8284 70.9459 37.5C70.9459 30.1716 68.5989 23.4002 64.6196 17.8895C63.97 16.9899 64.1825 15.7411 65.0942 15.1001Z"
                                                />
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="ms-5 text-start">
                                        <p class="mb-1">Cake Apprentice</p>
                                        <p class="mb-0 text-primary">Level 2</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Badges End -->
                        </div>
                        <!-- Right Side End -->
                    </div>
                    <!-- Content End -->
                </div>
            </main>
            <!-- Layout Footer Start -->
            <footer>
                <div class="footer-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <p class="mb-0 text-muted text-medium">Colored Strategies 2021</p>
                            </div>
                            <div class="col-sm-6 d-none d-sm-block">
                                <ul class="breadcrumb pt-0 pe-0 mb-0 float-end">
                                    <li class="breadcrumb-item mb-0 text-medium">
                                        <a href="https://1.envato.market/BX5oGy" target="_blank" class="btn-link">Review</a>
                                    </li>
                                    <li class="breadcrumb-item mb-0 text-medium">
                                        <a href="https://1.envato.market/BX5oGy" target="_blank" class="btn-link">Purchase</a>
                                    </li>
                                    <li class="breadcrumb-item mb-0 text-medium">
                                        <a href="https://acorn-html-docs.coloredstrategies.com/" target="_blank" class="btn-link">Docs</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- Layout Footer End -->
        </div>
        <!-- Theme Settings Modal Start -->
        <div
            class="modal fade modal-right scroll-out-negative"
            id="settings"
            data-bs-backdrop="true"
            tabindex="-1"
            role="dialog"
            aria-labelledby="settings"
            aria-hidden="true"
            >
            <div class="modal-dialog modal-dialog-scrollable full" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Theme Settings</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <div class="scroll-track-visible">
                            <div class="mb-5" id="color">
                                <label class="mb-3 d-inline-block form-label">Color</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-blue" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="blue-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT BLUE</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-blue" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="blue-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK BLUE</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-red" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="red-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT RED</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-red" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="red-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK RED</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-green" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="green-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT GREEN</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-green" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="green-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK GREEN</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-purple" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="purple-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT PURPLE</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-purple" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="purple-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK PURPLE</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-pink" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="pink-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT PINK</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-pink" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="pink-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK PINK</span>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="mb-5" id="navcolor">
                                <label class="mb-3 d-inline-block form-label">Override Nav Palette</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap">
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="default" data-parent="navcolor">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DEFAULT</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="light" data-parent="navcolor">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-secondary figure-light top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="dark" data-parent="navcolor">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-muted figure-dark top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK</span>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="mb-5" id="behaviour">
                                <label class="mb-3 d-inline-block form-label">Menu Behaviour</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="pinned" data-parent="behaviour">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary left large"></div>
                                            <div class="figure figure-secondary right small"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">PINNED</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="unpinned" data-parent="behaviour">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary left"></div>
                                            <div class="figure figure-secondary right"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">UNPINNED</span>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="mb-5" id="layout">
                                <label class="mb-3 d-inline-block form-label">Layout</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="fluid" data-parent="layout">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">FLUID</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="boxed" data-parent="layout">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom small"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">BOXED</span>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="mb-5" id="radius">
                                <label class="mb-3 d-inline-block form-label">Radius</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap">
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="rounded" data-parent="radius">
                                        <div class="card rounded-md radius-rounded p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">ROUNDED</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="standard" data-parent="radius">
                                        <div class="card rounded-md radius-regular p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">STANDARD</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="flat" data-parent="radius">
                                        <div class="card rounded-md radius-flat p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">FLAT</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Theme Settings Modal End -->

        <!-- Niches Modal Start -->
        <div
            class="modal fade modal-right scroll-out-negative"
            id="niches"
            data-bs-backdrop="true"
            tabindex="-1"
            role="dialog"
            aria-labelledby="niches"
            aria-hidden="true"
            >
            <div class="modal-dialog modal-dialog-scrollable full" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Niches</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <div class="scroll-track-visible">
                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Classic Dashboard</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img
                                            src="https://acorn.coloredstrategies.com/img/page/classic-dashboard.jpg"
                                            class="img-fluid rounded-sm lower-opacity border border-separator-light"
                                            alt="card image"
                                            />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a
                                                target="_blank"
                                                href="https://acorn-html-classic-dashboard.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Html
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-laravel-classic-dashboard.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Laravel
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-dotnet-classic-dashboard.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Ecommerce Platform</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img
                                            src="https://acorn.coloredstrategies.com/img/page/ecommerce-platform.jpg"
                                            class="img-fluid rounded-sm lower-opacity border border-separator-light"
                                            alt="card image"
                                            />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a
                                                target="_blank"
                                                href="https://acorn-html-ecommerce-platform.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Html
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-laravel-ecommerce-platform.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Laravel
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-dotnet-ecommerce-platform.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Elearning Portal</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img
                                            src="https://acorn.coloredstrategies.com/img/page/elearning-portal.jpg"
                                            class="img-fluid rounded-sm lower-opacity border border-separator-light"
                                            alt="card image"
                                            />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a
                                                target="_blank"
                                                href="https://acorn-html-elearning-portal.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Html
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-laravel-elearning-portal.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Laravel
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-dotnet-elearning-portal.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Service Provider</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img
                                            src="https://acorn.coloredstrategies.com/img/page/service-provider.jpg"
                                            class="img-fluid rounded-sm lower-opacity border border-separator-light"
                                            alt="card image"
                                            />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a
                                                target="_blank"
                                                href="https://acorn-html-service-provider.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Html
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-laravel-service-provider.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Laravel
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-dotnet-service-provider.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Starter Project</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img
                                            src="https://acorn.coloredstrategies.com/img/page/starter-project.jpg"
                                            class="img-fluid rounded-sm lower-opacity border border-separator-light"
                                            alt="card image"
                                            />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a
                                                target="_blank"
                                                href="https://acorn-html-starter-project.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Html
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-laravel-starter-project.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                Laravel
                                            </a>
                                            <a
                                                target="_blank"
                                                href="https://acorn-dotnet-starter-project.coloredstrategies.com/"
                                                class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1"
                                                >
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Niches Modal End -->

        <!-- Theme Settings & Niches Buttons Start -->
        <div class="settings-buttons-container">
            <button type="button" class="btn settings-button btn-primary p-0" data-bs-toggle="modal" data-bs-target="#settings" id="settingsButton">
                <span class="d-inline-block no-delay" data-bs-delay="0" data-bs-offset="0,3" data-bs-toggle="tooltip" data-bs-placement="left" title="Settings">
                    <i data-cs-icon="paint-roller" class="position-relative"></i>
                </span>
            </button>
            <button type="button" class="btn settings-button btn-primary p-0" data-bs-toggle="modal" data-bs-target="#niches" id="nichesButton">
                <span class="d-inline-block no-delay" data-bs-delay="0" data-bs-offset="0,3" data-bs-toggle="tooltip" data-bs-placement="left" title="Niches">
                    <i data-cs-icon="toy" class="position-relative"></i>
                </span>
            </button>
            <a href="https://1.envato.market/BX5oGy" target="_blank" class="btn settings-button btn-primary p-0" id="purchaseButton">
                <span class="d-inline-block no-delay" data-bs-delay="0" data-bs-offset="0,3" data-bs-toggle="tooltip" data-bs-placement="left" title="Purchase">
                    <i data-cs-icon="cart" class="position-relative"></i>
                </span>
            </a>
        </div>
        <!-- Theme Settings & Niches Buttons End -->

        <!-- Search Modal Start -->
        <div class="modal fade modal-under-nav modal-search modal-close-out" id="searchPagesModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header border-0 p-0">
                        <button type="button" class="btn-close btn btn-icon btn-icon-only btn-foreground" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body ps-5 pe-5 pb-0 border-0">
                        <input id="searchPagesInput" class="form-control form-control-xl borderless ps-0 pe-0 mb-1 auto-complete" type="text" autocomplete="off" />
                    </div>
                    <div class="modal-footer border-top justify-content-start ps-5 pe-5 pb-3 pt-3 border-0">
                        <span class="text-alternate d-inline-block m-0 me-3">
                            <i data-cs-icon="arrow-bottom" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                            <span class="align-middle text-medium">Navigate</span>
                        </span>
                        <span class="text-alternate d-inline-block m-0 me-3">
                            <i data-cs-icon="arrow-bottom-left" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                            <span class="align-middle text-medium">Select</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <!-- Vendor Scripts Start -->
        <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery-3.5.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/OverlayScrollbars.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/autoComplete.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/clamp.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/plyr.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/cs/scrollspy.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.validate/jquery.validate.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.validate/additional-methods.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.barrating.min.js"></script>

        <!-- Vendor Scripts End -->

        <!-- Template Base Scripts Start -->
        <script src="${pageContext.request.contextPath}/resources/font/CS-Line/csicons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/helpers.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/globals.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/nav.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/search.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/init.js"></script>
        <!-- Template Base Scripts End -->

        <!-- Page Specific Scripts Start -->
        <script src="${pageContext.request.contextPath}/resources/js/pages/course.detail.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/forms/controls.rating.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/common.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
        <!-- Page Specific Scripts End -->
    </body>
</html>
