<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" data-footer="true"
      data-override='{"attributes": {"placement": "vertical","layout": "boxed", "behaviour": "unpinned" }, "storagePrefix": "elearning-portal"}'>

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>Acorn Admin Template | Course Detail</title>
        <meta name="description" content="Acorn elearning platform course detail." />
        <!-- Favicon Tags Start -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="${pageContext.request.contextPath}/resources/img/favicon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/img/favicon/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="${pageContext.request.contextPath}/resources/img/favicon/mstile-310x310.png" />
        <!-- Favicon Tags End -->
        <!-- Font Tags Start -->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font/CS-Interface/style.css" />
        <!-- Font Tags End -->
        <!-- Vendor Styles Start -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/OverlayScrollbars.min.css" />

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/vendor/plyr.css" />

        <!-- Vendor Styles End -->
        <!-- Template Base Styles Start -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" />
        <!-- Template Base Styles End -->

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/discussion.css">

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
        <script src="${pageContext.request.contextPath}/resources/js/base/loader.js"></script>
    </head>

    <body>
        <div id="root">
            <div id="nav" class="nav-container d-flex" data-horizontal-mobile="1500">
                <div class="nav-content d-flex">
                    <!-- Logo Start -->
                    <div class="logo position-relative">
                        <a href="${pageContext.request.contextPath}/resources/index.html">
                            <!-- Logo can be added directly -->
                            <!-- <img src="${pageContext.request.contextPath}/resources/img/logo/logo-white.svg" alt="logo" /> -->

                            <!-- Or added via css to provide different ones for different color themes -->
                            <div class="img"></div>
                        </a>
                    </div>
                    <!-- Logo End -->

                    <!-- Language Switch Start -->
                    <div class="language-switch-container">
                        <button class="btn btn-empty language-button dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">EN</button>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item">DE</a>
                            <a href="#" class="dropdown-item active">EN</a>
                            <a href="#" class="dropdown-item">ES</a>
                        </div>
                    </div>
                    <!-- Language Switch End -->

                    <!-- Menu Start -->
                    <div class="menu-container flex-grow-1">
                        <ul id="menu" class="menu">
                            <li>
                                <a href="#dashboards">
                                    <i data-cs-icon="home-garage" class="icon" data-cs-size="18"></i>
                                    <span class="label">Dashboards</span>
                                </a>
                                <ul id="dashboards">
                                    <li>
                                        <a href="Dashboards.Elearning.html">
                                            <span class="label">Elearning</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Dashboards.School.html">
                                            <span class="label">School</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#courses">
                                    <i data-cs-icon="online-class" class="icon" data-cs-size="18"></i>
                                    <span class="label">Courses</span>
                                </a>
                                <ul id="courses">
                                    <li>
                                        <a href="Course.Explore.html">
                                            <span class="label">Explore</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Course.List.html">
                                            <span class="label">List</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Course.Detail.html">
                                            <span class="label">Detail</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#quiz">
                                    <i data-cs-icon="quiz" class="icon" data-cs-size="18"></i>
                                    <span class="label">Quiz</span>
                                </a>
                                <ul id="quiz">
                                    <li>
                                        <a href="Quiz.List.html">
                                            <span class="label">List</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Quiz.Detail.html">
                                            <span class="label">Detail</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Quiz.Result.html">
                                            <span class="label">Result</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#paths">
                                    <i data-cs-icon="destination" class="icon" data-cs-size="18"></i>
                                    <span class="label">Paths</span>
                                </a>
                                <ul id="paths">
                                    <li>
                                        <a href="Path.List.html">
                                            <span class="label">List</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Path.Detail.html">
                                            <span class="label">Detail</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#instructors">
                                    <i data-cs-icon="lecture" class="icon" data-cs-size="18"></i>
                                    <span class="label">Instructors</span>
                                </a>
                                <ul id="instructors">
                                    <li>
                                        <a href="Instructor.List.html">
                                            <span class="label">List</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Instructor.Detail.html">
                                            <span class="label">Detail</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#miscellaneous">
                                    <i data-cs-icon="layout-5" class="icon" data-cs-size="18"></i>
                                    <span class="label">Miscellaneous</span>
                                </a>
                                <ul id="miscellaneous">
                                    <li>
                                        <a href="Misc.Player.html">
                                            <span class="label">Player</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Misc.Material.html">
                                            <span class="label">Material</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Misc.Syllabus.html">
                                            <span class="label">Syllabus</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- Menu End -->

                    <!-- Mobile Buttons Start -->
                    <div class="mobile-buttons-container">
                        <!-- Scrollspy Mobile Button Start -->
                        <a href="#" id="scrollSpyButton" class="spy-button" data-bs-toggle="dropdown">
                            <i data-cs-icon="menu-dropdown"></i>
                        </a>
                        <!-- Scrollspy Mobile Button End -->

                        <!-- Scrollspy Mobile Dropdown Start -->
                        <div class="dropdown-menu dropdown-menu-end" id="scrollSpyDropdown"></div>
                        <!-- Scrollspy Mobile Dropdown End -->

                        <!-- Menu Button Start -->
                        <a href="#" id="mobileMenuButton" class="menu-button">
                            <i data-cs-icon="menu"></i>
                        </a>
                        <!-- Menu Button End -->
                    </div>
                    <!-- Mobile Buttons End -->
                </div>
                <div class="nav-shadow"></div>
            </div>

            <main>
                <div class="container">
                    <!-- Title and Top Buttons Start -->
                    <div class="page-title-container">
                        <div class="row">
                            <!-- Title Start -->
                            <div class="col-12 col-sm-6">
                                <h1 class="mb-0 pb-0 display-4" id="title">Bread Making Techniques</h1>
                                <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
                                    <ul class="breadcrumb pt-0">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                        <li class="breadcrumb-item"><a href="Course.Explore.html">Courses</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- Title End -->

                            <!-- Top Buttons Start -->
                            <div class="col-12 col-sm-6 d-flex align-items-start justify-content-end">
                                <!-- Start Now Start -->
                                <a href="Misc.Player.html" class="btn btn-primary btn-icon btn-icon-start w-100 w-sm-auto">
                                    <i data-cs-icon="chevron-right"></i>
                                    <span>Start Now</span>
                                </a>
                                <!-- Start Now End -->
                            </div>
                            <!-- Top Buttons End -->
                        </div>
                    </div>
                    <!-- Title and Top Buttons End -->

                    <!-- Content Start -->

                    <div class="row">
                        <!-- Left Side Start -->
                        <div class="col-12 col-xxl-8 mb-5">
                            <!-- Reviews Start -->
                            <h2 class="small-title">Discussion</h2>
                            <div class="card">
                                <div class="card-body banana1" >
                                    <!-- Comments Start -->
                                    <h2>What is your favorite food?</h2>
                                    <br>
                                    <!-- User input comment Start-->
                                    <form style="display: grid;" id="discussionForm" class="pb-3">
                                        <div class="d-flex align-items-center mt-3">
                                            <div class="col-auto">
                                                <div class="sw-5 me-3">
                                                    <c:if test="${sessionScope.AuthenticationUser != null}">
                                                        <img src="${pageContext.request.contextPath}/resources/img/profile/${sessionScope.AuthenticationUser.avatar}" class="img-fluid rounded-xl" alt="thumb">
                                                    </c:if>
                                                    <c:if test="${sessionScope.AuthenticationUser == null}">
                                                        <img src="${pageContext.request.contextPath}/resources/img/profile/profile-3.jpg" class="img-fluid rounded-xl" alt="thumb">
                                                    </c:if>
                                                </div>
                                            </div>
                                            <div class="col pe-3">
                                                <input type="text" class="form-control" placeholder="Enter your though" name="content">
                                            </div>
                                            <p id="message" style="color:red"></p>
                                        </div>
                                        <!-- submit button -->
                                    </form>
                                    <div id ="discussion-contain"></div>
                                    <!-- User input comment End-->
                                    <c:forEach var="discussion" items="${discussionList}">
                                        <div class="d-flex align-items-center border-top border-separator-light pb-3 pt-3">
                                            <div class="row g-0 w-100">
                                                <div class="col-auto">
                                                    <div class="sw-5 me-3">
                                                        <img src="${pageContext.request.contextPath}/resources/img/profile/profile-3.jpg" class="img-fluid rounded-xl" alt="thumb" />
                                                    </div>
                                                </div>
                                                <div class="col pe-3 reply" id="comment-#${discussion.id}" name="Cherish Kerr">
                                                    <div>Cherish Kerr</div>
                                                    <div class="text-muted text-small mb-2">2 days ago</div>
                                                    <div class="text-alternate lh-1-25 mb-1">${discussion.content}</div>
                                                    <a href="#" class="text-medium like-btn">like</a>
                                                    <a href="#" class="text-medium reply-btn">reply</a>
                                                    <div class="insertHere"></div>
                                                    <c:forEach var="discussionReply" items="${discussion.discussionReplyList}">
                                                        <div class="d-flex align-items-center mt-3">
                                                            <div class="row g-0 w-100">
                                                                <div class="col-auto">
                                                                    <div class="sw-5 me-3">
                                                                        <img src="${pageContext.request.contextPath}/resources/img/profile/profile-3.jpg" class="img-fluid rounded-xl" alt="thumb" />
                                                                    </div>
                                                                </div>
                                                                <div class="col pe-3 reply" id="comment-#${discussion.id}" name="Cherish Kerr">
                                                                    <div>Cherish Kerr</div>
                                                                    <div class="text-muted text-small mb-2">2 days ago</div>
                                                                    <div class="text-alternate lh-1-25 mb-1">${discussionReply.content}</div>
                                                                    <a href="#" class="text-medium like-btn">like</a>
                                                                    <a href="#" class="text-medium reply-btn reply-main-btn">reply</a>
                                                                    <div class="insertReplyHere"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    <!-- Comments End -->

                                </div>
                            </div>
                            <!-- Reviews End -->
                        </div>
                        <!-- Left Side End -->
                    </div>
                    <!-- Content End -->
                </div>
            </main>
            <!-- Layout Footer Start -->
            <footer>
                <div class="footer-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <p class="mb-0 text-muted text-medium">Colored Strategies 2021</p>
                            </div>
                            <div class="col-sm-6 d-none d-sm-block">
                                <ul class="breadcrumb pt-0 pe-0 mb-0 float-end">
                                    <li class="breadcrumb-item mb-0 text-medium">
                                        <a href="https://1.envato.market/BX5oGy" target="_blank" class="btn-link">Review</a>
                                    </li>
                                    <li class="breadcrumb-item mb-0 text-medium">
                                        <a href="https://1.envato.market/BX5oGy" target="_blank" class="btn-link">Purchase</a>
                                    </li>
                                    <li class="breadcrumb-item mb-0 text-medium">
                                        <a href="https://acorn-html-docs.coloredstrategies.com/" target="_blank" class="btn-link">Docs</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- Layout Footer End -->
        </div>
        <!-- Theme Settings Modal Start -->
        <div class="modal fade modal-right scroll-out-negative" id="settings" data-bs-backdrop="true" tabindex="-1"
             role="dialog" aria-labelledby="settings" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable full" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Theme Settings</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <div class="scroll-track-visible">
                            <div class="mb-5" id="color">
                                <label class="mb-3 d-inline-block form-label">Color</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-blue" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="blue-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT BLUE</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-blue" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="blue-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK BLUE</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-red" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="red-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT RED</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-red" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="red-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK RED</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-green" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="green-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT GREEN</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-green" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="green-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK GREEN</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-purple" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="purple-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT PURPLE</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-purple" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="purple-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK PURPLE</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="row d-flex g-3 justify-content-between flex-wrap mb-3">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="light-pink" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="pink-light"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT PINK</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="dark-pink" data-parent="color">
                                        <div class="card rounded-md p-3 mb-1 no-shadow color">
                                            <div class="pink-dark"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK PINK</span>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="mb-5" id="navcolor">
                                <label class="mb-3 d-inline-block form-label">Override Nav Palette</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap">
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="default" data-parent="navcolor">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DEFAULT</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="light" data-parent="navcolor">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-secondary figure-light top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">LIGHT</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="dark" data-parent="navcolor">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-muted figure-dark top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">DARK</span>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="mb-5" id="behaviour">
                                <label class="mb-3 d-inline-block form-label">Menu Behaviour</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="pinned" data-parent="behaviour">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary left large"></div>
                                            <div class="figure figure-secondary right small"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">PINNED</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="unpinned" data-parent="behaviour">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary left"></div>
                                            <div class="figure figure-secondary right"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">UNPINNED</span>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="mb-5" id="layout">
                                <label class="mb-3 d-inline-block form-label">Layout</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap">
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="fluid" data-parent="layout">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">FLUID</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-50 option col" data-value="boxed" data-parent="layout">
                                        <div class="card rounded-md p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom small"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">BOXED</span>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="mb-5" id="radius">
                                <label class="mb-3 d-inline-block form-label">Radius</label>
                                <div class="row d-flex g-3 justify-content-between flex-wrap">
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="rounded" data-parent="radius">
                                        <div class="card rounded-md radius-rounded p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">ROUNDED</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="standard" data-parent="radius">
                                        <div class="card rounded-md radius-regular p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">STANDARD</span>
                                        </div>
                                    </a>
                                    <a href="#" class="flex-grow-1 w-33 option col" data-value="flat" data-parent="radius">
                                        <div class="card rounded-md radius-flat p-3 mb-1 no-shadow">
                                            <div class="figure figure-primary top"></div>
                                            <div class="figure figure-secondary bottom"></div>
                                        </div>
                                        <div class="text-muted text-part">
                                            <span class="text-extra-small align-middle">FLAT</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Theme Settings Modal End -->

        <!-- Niches Modal Start -->
        <div class="modal fade modal-right scroll-out-negative" id="niches" data-bs-backdrop="true" tabindex="-1"
             role="dialog" aria-labelledby="niches" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable full" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Niches</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <div class="scroll-track-visible">
                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Classic Dashboard</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img src="https://acorn.coloredstrategies.com/img/page/classic-dashboard.jpg"
                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a target="_blank" href="https://acorn-html-classic-dashboard.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Html
                                            </a>
                                            <a target="_blank" href="https://acorn-laravel-classic-dashboard.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Laravel
                                            </a>
                                            <a target="_blank" href="https://acorn-dotnet-classic-dashboard.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Ecommerce Platform</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img src="https://acorn.coloredstrategies.com/img/page/ecommerce-platform.jpg"
                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a target="_blank" href="https://acorn-html-ecommerce-platform.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Html
                                            </a>
                                            <a target="_blank" href="https://acorn-laravel-ecommerce-platform.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Laravel
                                            </a>
                                            <a target="_blank" href="https://acorn-dotnet-ecommerce-platform.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Elearning Portal</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img src="https://acorn.coloredstrategies.com/img/page/elearning-portal.jpg"
                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a target="_blank" href="https://acorn-html-elearning-portal.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Html
                                            </a>
                                            <a target="_blank" href="https://acorn-laravel-elearning-portal.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Laravel
                                            </a>
                                            <a target="_blank" href="https://acorn-dotnet-elearning-portal.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Service Provider</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img src="https://acorn.coloredstrategies.com/img/page/service-provider.jpg"
                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a target="_blank" href="https://acorn-html-service-provider.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Html
                                            </a>
                                            <a target="_blank" href="https://acorn-laravel-service-provider.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Laravel
                                            </a>
                                            <a target="_blank" href="https://acorn-dotnet-service-provider.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-5">
                                <label class="mb-2 d-inline-block form-label">Starter Project</label>
                                <div class="hover-reveal-buttons position-relative hover-reveal cursor-default">
                                    <div class="position-relative mb-3 mb-lg-5 rounded-sm">
                                        <img src="https://acorn.coloredstrategies.com/img/page/starter-project.jpg"
                                             class="img-fluid rounded-sm lower-opacity border border-separator-light" alt="card image" />
                                        <div class="position-absolute reveal-content rounded-sm absolute-center-vertical text-center w-100">
                                            <a target="_blank" href="https://acorn-html-starter-project.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Html
                                            </a>
                                            <a target="_blank" href="https://acorn-laravel-starter-project.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                Laravel
                                            </a>
                                            <a target="_blank" href="https://acorn-dotnet-starter-project.coloredstrategies.com/"
                                               class="btn btn-primary btn-sm sw-10 sw-lg-12 d-block mx-auto my-1">
                                                .Net5
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Niches Modal End -->

        <!-- Theme Settings & Niches Buttons Start -->
        <div class="settings-buttons-container">
            <button type="button" class="btn settings-button btn-primary p-0" data-bs-toggle="modal" data-bs-target="#settings"
                    id="settingsButton">
                <span class="d-inline-block no-delay" data-bs-delay="0" data-bs-offset="0,3" data-bs-toggle="tooltip"
                      data-bs-placement="left" title="Settings">
                    <i data-cs-icon="paint-roller" class="position-relative"></i>
                </span>
            </button>
            <button type="button" class="btn settings-button btn-primary p-0" data-bs-toggle="modal" data-bs-target="#niches"
                    id="nichesButton">
                <span class="d-inline-block no-delay" data-bs-delay="0" data-bs-offset="0,3" data-bs-toggle="tooltip"
                      data-bs-placement="left" title="Niches">
                    <i data-cs-icon="toy" class="position-relative"></i>
                </span>
            </button>
            <a href="https://1.envato.market/BX5oGy" target="_blank" class="btn settings-button btn-primary p-0"
               id="purchaseButton">
                <span class="d-inline-block no-delay" data-bs-delay="0" data-bs-offset="0,3" data-bs-toggle="tooltip"
                      data-bs-placement="left" title="Purchase">
                    <i data-cs-icon="cart" class="position-relative"></i>
                </span>
            </a>
        </div>
        <!-- Theme Settings & Niches Buttons End -->

        <!-- Search Modal Start -->
        <div class="modal fade modal-under-nav modal-search modal-close-out" id="searchPagesModal" tabindex="-1" role="dialog"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header border-0 p-0">
                        <button type="button" class="btn-close btn btn-icon btn-icon-only btn-foreground" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body ps-5 pe-5 pb-0 border-0">
                        <input id="searchPagesInput" class="form-control form-control-xl borderless ps-0 pe-0 mb-1 auto-complete"
                               type="text" autocomplete="off" />
                    </div>
                    <div class="modal-footer border-top justify-content-start ps-5 pe-5 pb-3 pt-3 border-0">
                        <span class="text-alternate d-inline-block m-0 me-3">
                            <i data-cs-icon="arrow-bottom" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                            <span class="align-middle text-medium">Navigate</span>
                        </span>
                        <span class="text-alternate d-inline-block m-0 me-3">
                            <i data-cs-icon="arrow-bottom-left" data-cs-size="15" class="text-alternate align-middle me-1"></i>
                            <span class="align-middle text-medium">Select</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <!-- Vendor Scripts Start -->
        <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery-3.5.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/OverlayScrollbars.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/autoComplete.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/clamp.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.validate/jquery.validate.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.validate/additional-methods.min.js"></script>


        <!-- Vendor Scripts End -->

        <!-- Template Base Scripts Start -->
        <script src="${pageContext.request.contextPath}/resources/font/CS-Line/csicons.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/helpers.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/globals.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/nav.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/search.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/base/init.js"></script>
        <!-- Template Base Scripts End -->
        <!-- Page Specific Scripts Start -->

        <script src="${pageContext.request.contextPath}/resources/js/common.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
        <!-- Page Specific Scripts End -->

        <!-- Script reply for specific discussion feature -->
        <script src="${pageContext.request.contextPath}/resources/js/pages/lesson.replyDiscussion.js"></script>

        <!-- Script for discussion feature -->
        <script src="${pageContext.request.contextPath}/resources/js/pages/lesson.discussion.js"></script>
    </body>

</html>