CREATE database elearning;
USE elearning;

create table user
(
    id       int primary key auto_increment,
    username varchar(32) not null,
    email    varchar(100) not null,
    name     nvarchar(100),
    dob      date,
    gender   int,
    phone    varchar(15),
    bio      longtext,
    avatar   varchar(100),
    level    int,
    role     int not null,
    status   int not null,
    password varchar(100),
    created  datetime
);

create table user_token_log
(
    id    int not null ,
    foreign key (id) references user(id),
    token varchar(100) not null,
    action varchar(10) not null,
    valid bit not null,
    created_time datetime not null,
    expired_time datetime not null
);
create table category(
    id int primary key auto_increment,
    name nvarchar(256) not null,
    description longtext,
    thumbnail varchar(256)
);


create table course(
    id int primary key auto_increment,
    category_id int not null,
    foreign key (category_id) references category(id),
    name nvarchar(256) not null,
    description longtext,
    thumbnail varchar(256),
    price float,
    rating float,
    content longtext,
    level int,
    duration int
);

create table course_topic(
    id int primary key auto_increment,
    course_id int not null,
    foreign key (course_id) references course(id),
    name nvarchar(256) not null,
    description longtext,
    material nvarchar(256)
);

create table topic_lesson(
    id int primary key auto_increment,
    topic_id int not null,
    foreign key (topic_id) references course_topic(id),
    name nvarchar(256) not null,
    description longtext,
    material nvarchar(256)
);
create table discussion(
    id int primary key auto_increment,
    lesson_id int not null,
    foreign key (lesson_id) references topic_lesson(id),
    content longtext
);

create table discussion_reply(
    id int primary key auto_increment,
    discussion_id int not null ,
    foreign key (discussion_id) references discussion(id),
    content longtext
);

create table wishlist(
    id int primary key auto_increment,
    uid int not null,
    foreign key (uid) references user(id),
    courseid int not null ,
    foreign key (courseid) references course(id),
    created_date datetime
);

/* Password for admin account: admin */
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('admin1', 'admin1@gmail.com', N'Nguyễn Văn An', '2021-12-12', 0, '0987654321', 'test1', 'default.jpg', 1,1,1,'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('admin2', 'admin2@gmail.com', N'Nguyễn Văn Bê', '2021-11-11', 0, '0989878787', 'test2', 'default.jpg', 1,1,1,'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('admin3', 'admin3@gmail.com', N'Nguyễn Văn Cê', '2021-10-10', 0, '0987677777', 'test3', 'default.jpg', 1,1,1,'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('admin4', 'admin4@gmail.com', N'Nguyễn Văn Đê', '2021-09-09', 0, '0987654543', 'test4', 'default.jpg', 1,1,1,'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('admin5', 'admin5@gmail.com', N'Nguyễn Văn Ê', '2021-08-08', 0, '0987676756', 'test5', 'default.jpg', 1,1,1,'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('admin6', 'admin6@gmail.com', N'Nguyễn Văn Gờ', '2021-07-07', 0, '0987765765', 'test6', 'default.jpg', 1,1,1,'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('admin7', 'admin7@gmail.com', N'Nguyễn Thị Hát', '2021-12-05', 1, '0987890987', 'test7', 'default.jpg', 1,1,1,'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', '2021-09-18 20:01:46.000');
/* Password for user account: user */
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('user1', 'user1@gmail.com', N'Nguyễn Văn I', '2021-11-12', 0, '0982345678', 'test8', 'default.jpg', 1,0,1,'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('user2', 'user2@gmail.com', N'Nguyễn Văn J', '2021-10-12', 0, '0982345678', 'test9', 'default.jpg', 1,0,1,'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('user3', 'user3@gmail.com', N'Nguyễn Thị Ka', '2021-09-12', 1, '0987656565', 'test10', 'default.jpg', 1,0,1,'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('user4', 'user4@gmail.com', N'Nguyễn Văn L', '2021-12-12', 0, '0987676765', 'test11', 'default.jpg', 1,0,1,'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('user5', 'user5@gmail.com', N'Nguyễn Thị Mờ', '2021-07-12', 1, '0989898987', 'test12', 'default.jpg', 1,0,1,'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('user6', 'user6@gmail.com', N'Nguyễn Văn Nờ', '2021-02-12', 2, '0986438212', 'test13', 'default.jpg', 1,0,1,'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('user7', 'user7@gmail.com', N'Nguyễn Văn O', '2021-04-12', 0, '0976543523', 'test14', 'default.jpg', 1,0,1,'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', '2021-09-18 20:01:46.000');
insert into user(username, email, name, dob, gender, phone, bio, avatar, level, role, status, password, created) values ('user8', 'user8@gmail.com', N'Nguyễn Thị Pê', '2021-01-12', 1, '0987654321', 'test15', 'default.jpg', 1,0,1,'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', '2021-09-18 20:01:46.000');
insert into category(id, name, description, thumbnail) values ('1', 'test', 'aaa','1.png');
INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration) VALUES (1, 1, N'Introduce to Bread Making', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/baguette.jpg', 81, 3, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 0, 160);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (2, 1, N'Better Ways to Mix Dough for the Molds', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/barmbrack.jpg', 0, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 1, 240);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (3, 1, N'Apple Cake Recipe', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/pain-de-campagne.jpg', 6, 5, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 2, 355);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (4, 1, N'Cooking Tips the Perfect Burger', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/sandwich-bread.jpg', 65, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 3, 245);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (5, 1, N'Fruit Decorations', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/michetta.jpg', 0, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 0, 212);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (6, 1, N'Recipes for Sweet and Healty Treats', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/bauernbrot.jpg', 23, 3, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 1, 273);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (7, 1, N'Better Ways to Mix Dough for the Molds', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/cornbread.jpg', 0, 5, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 2, 254);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (8, 1, N'Carrot Cake Gingerbread', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/zopf.jpg', 53, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 3, 168);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (9, 1, N'Facts About Sugar Products', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/rugbraud.jpg', 60, 5, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 0, 184);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (10, 1, N'Introduction to Baking Cakes', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/guernsey-gache.jpg', 30, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 1, 70);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (11, 1, N'Apple Cake Recipe for Starters', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/pullman-loaf.jpg', 0, 5, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 2, 89);

INSERT course (id, category_id, name, description, thumbnail, price, rating, content, level, duration)  VALUES (12, 1, N'Advanced Sandwich Making Techniques', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/pane-sciocco.jpg', 0, 3, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 3, 207);


INSERT course_topic (id, course_id, name, description) VALUES (7, 1, N'Introduction to Bread Making', N'Aliquam erat volutpat. Integer non nisl eleifend, facilisis libero tincidunt, molestie ex. Mauris fringilla bibendum libero ac vestibulum. Integer gravida pellentesque neque nec egestas. Suspendisse in bibendum odio. Praesent fringilla, urna ac fermentum hendrerit, nisi erat feugiat velit, sit amet ornare elit metus nec nibh. Duis venenatis, leo vitae tincidunt ullamcorper, urna tellus luctus purus, vitae maximus arcu justo sit amet dui. In et facilisis dolor.');
INSERT course_topic (id, course_id, name, description) VALUES (8, 1, N'Molding Technique', N'Fusce et aliquet massa, a semper ipsum. Praesent eget viverra diam. Suspendisse ut ligula tincidunt, fermentum nulla at, malesuada dui. Proin maximus dui vitae lorem dictum facilisis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris ligula felis, interdum eu nisi ac, volutpat facilisis purus. Suspendisse potenti. Vestibulum ullamcorper convallis ultrices. Aliquam semper sodales nulla, sit amet feugiat velit. Quisque efficitur risus aliquam ex commodo, ac iaculis est pulvinar. Sed ac arcu libero. Phasellus rhoncus, felis vitae accumsan vestibulum, nisi lorem accumsan metus, eu vehicula orci elit ut ligula. Nulla sit amet lacinia augue, eget molestie sem. Suspendisse potenti. Donec molestie turpis id quam eleifend, eget imperdiet mi bibendum. Pellentesque vestibulum convallis sodales.');
INSERT course_topic (id, course_id, name, description) VALUES (9, 1, N'Baking the Right Way', N'Morbi id pretium purus. Sed varius tellus eget ante eleifend, quis semper elit eleifend. Mauris efficitur tellus nibh, vel placerat turpis viverra non. Praesent vel rutrum leo. Sed orci metus, lobortis id scelerisque eget, dictum eu augue. Maecenas vel odio ex. Duis eu magna et magna blandit congue. Ut convallis luctus porta. Pellentesque ut nunc laoreet, sagittis odio sed, feugiat massa. Etiam pretium pulvinar turpis vitae egestas.');
INSERT course_topic (id, course_id, name, description) VALUES (10, 1, N'Presentation', N'Phasellus laoreet euismod fermentum. Pellentesque et elit enim. Proin in libero magna. Fusce mi velit, viverra eget orci eu, luctus blandit est. Donec rhoncus eu augue eget posuere. Aliquam euismod massa at velit porttitor luctus. Aliquam fringilla mollis diam, vel faucibus dolor consectetur non. Fusce et diam quis augue venenatis ullamcorper ut ut diam.');

INSERT course_topic (id, course_id, name, description)VALUES (11, 1, N'Conclusion', N'Sed elementum magna at elit aliquam consequat. Praesent nisi ante, vulputate sit amet arcu convallis, euismod porttitor massa. Mauris rutrum eleifend risus vitae posuere. Aliquam erat volutpat. In et velit nec dui mollis condimentum. Aliquam semper suscipit mauris sed tincidunt. Fusce vitae dui erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla nec justo ac elit fringilla porta. Nunc mattis nulla et elit faucibus, et varius arcu dignissim.');

INSERT course_topic (id, course_id, name, description) VALUES (12, 1, N'What is Next?', N'Mauris sed posuere odio. Nulla non congue urna, in semper ligula. Sed vestibulum bibendum porta. Morbi in mauris eros. Donec sodales, arcu at tristique hendrerit, dui tortor scelerisque mauris, tempus sollicitudin dui eros eget lectus. Nunc bibendum, velit lacinia bibendum tincidunt, libero diam aliquet dolor, ac semper odio augue sed lacus. Praesent iaculis, ipsum et tempor lobortis, lorem erat finibus ante, id faucibus tortor purus fringilla nisl. Cras id convallis metus. Nulla facilisi. Mauris tortor nulla, molestie vel tempus eget, sodales ut mi. Cras eleifend risus ligula, quis posuere ipsum eleifend eu. Nullam ante justo, lobortis eu nibh eget, gravida imperdiet libero. Sed quis tellus sagittis, eleifend risus imperdiet, volutpat dolor. Duis congue ipsum sit amet lacus lobortis, in dapibus neque congue.');

INSERT topic_lesson (id, topic_id, name, description, material) VALUES (1, 7, N'Candy cake gummi bears', N'Sed varius tellus eget ante eleifend, quis semper elit eleifend. Mauris efficitur tellus nibh, vel placerat turpis viverra non. ', N'ZRLjZ4f1NyU');

INSERT topic_lesson (id, topic_id, name, description, material) VALUES (2, 8, N'Gummi bears dessert muffin pie', N'Praesent vel rutrum leo. Sed orci metus, lobortis id scelerisque eget, dictum eu augue. Maecenas vel odio ex. Duis eu magna et magna blandit congue.', N'_XVNvnQLo44');

INSERT topic_lesson (id, topic_id, name, description, material) VALUES (3, 9, N'Lemon drops wafer brownie pastry', N'Ut convallis luctus porta. Pellentesque ut nunc laoreet, sagittis odio sed, feugiat massa. Etiam pretium pulvinar turpis vitae egestas.', N'J70el4lrtIM');

INSERT topic_lesson (id, topic_id, name, description, material) VALUES (4, 10, N'Danish cake gummies jelly oat cake', N'Phasellus laoreet euismod fermentum. Pellentesque et elit enim. Proin in libero magna.', N'L4wSpD1iaPE');

INSERT topic_lesson (id, topic_id, name, description, material)VALUES (5, 11, N'Sweet roll candy muffin chocolate', N'Donec rhoncus eu augue eget posuere. Aliquam euismod massa at velit porttitor luctus. Aliquam fringilla mollis diam, vel faucibus dolor consectetur non. Fusce et diam quis augue venenatis ullamcorper ut ut diam.', N'cDHh1ZuiiKQ');

INSERT topic_lesson (id, topic_id, name, description, material) VALUES (6, 12, N'Danish cake gummies jelly oat cake advance', N'Ut a porta ligula. Fusce gravida erat libero, vel tincidunt mauris semper ornare. Aenean fringilla justo vel arcu ultricies, vel iaculis turpis suscipit. Maecenas sapien dui, pharetra ultrices erat scelerisque, aliquet cursus sapien.', N'vxRuzZXdb_Q');

INSERT topic_lesson (id, topic_id, name, description, material) VALUES (7, 7, N'French Banana Bread', N'Vestibulum id vehicula dolor, sed scelerisque orci. In sollicitudin magna ligula, vel faucibus magna gravida id. Sed molestie metus quis dui ornare posuere. Vestibulum nibh odio, laoreet sed velit nec, imperdiet pretium nunc. Pellentesque ultricies maximus nunc, eget luctus ante vestibulum sit amet. Duis tristique leo sed dui aliquet, a ullamcorper felis faucibus. Vestibulum quis ullamcorper lectus, et ullamcorper urna. Nunc euismod hendrerit libero in malesuada. Fusce sit amet arcu urna. Integer arcu risus, imperdiet ac eros et, laoreet congue ex. Maecenas luctus gravida lectus non pharetra. Duis aliquam feugiat ante, id bibendum lorem accumsan id. Duis ut tempor urna. Quisque ut tincidunt arcu, id dapibus massa. Vivamus eget ornare lorem. Nullam augue neque, ultrices at nulla tristique, scelerisque condimentum lorem.', N'6S6Kf7FadC0');
