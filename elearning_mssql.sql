USE [master]
GO
CREATE DATABASE [elearning_beta]
GO
USE [elearning_beta]
GO
CREATE TABLE [category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](256) NULL,
	[description] [nvarchar](max) NULL,
	[thumbnail] [nvarchar](256) NULL,
 CONSTRAINT [PK__category__3213E83FB0255046] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [course](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category_id] [int] NULL,
	[name] [nvarchar](256) NULL,
	[description] [nvarchar](max) NULL,
	[thumbnail] [nvarchar](256) NULL,
	[price] [float] NULL,
	[rating] [int] NULL,
	[content] [nvarchar](max) NULL,
	[level] [int] NULL,
	[duration] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [course_topic](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[course_id] [int] NULL,
	[name] [nvarchar](256) NULL,
	[description] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [discussion](
	[id] [int] NOT NULL,
	[lession_id] [int] NOT NULL,
	[content] [ntext] NOT NULL,
 CONSTRAINT [PK_discussion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [discussion_reply](
	[id] [int] NOT NULL,
	[discussion_id] [int] NOT NULL,
	[content] [ntext] NOT NULL,
 CONSTRAINT [PK_discussion_reply] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [topic_lesson](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[topic_id] [int] NULL,
	[name] [nvarchar](256) NULL,
	[description] [nvarchar](max) NULL,
	[material] [nvarchar](256) NULL,
 CONSTRAINT [PK__topic_le__3213E83FE73228F2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](32) NULL,
	[email] [varchar](100) NOT NULL,
	[name] [nvarchar](100) NULL,
	[dob] [date] NULL,
	[gender] [int] NULL,
	[phone] [varchar](15) NULL,
	[bio] [ntext] NULL,
	[avatar] [varchar](100) NULL,
	[level] [int] NULL,
	[role] [int] NOT NULL,
	[status] [int] NOT NULL,
	[password] [varchar](100) NULL,
	[created] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [user_token_log](
	[id] [int] NULL,
	[token] [varchar](100) NULL,
	[action] [varchar](10) NULL,
	[valid] [bit] NULL,
	[created_time] [datetime] NULL,
	[expired_time] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [wishlist](
	[id] [int] NOT NULL,
	[uid] [int] NOT NULL,
	[courseid] [int] NOT NULL,
	[created_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [category] ON 
GO
INSERT [category] ([id], [name], [description], [thumbnail]) VALUES (1, N'Food', N'Liquorice caramels apple pie chupa chups bonbon. Jelly-o candy apple pie sugar plum icing chocolate cake lollipop jujubes bear claw. Pastry sweet roll carrot cake cake macaroon gingerbread cookie. Brownie candy cookie candy pie sweet roll biscuit marzipan. Chocolate bar candy canes macaroon liquorice danish biscuit biscuit.

', N'https://cdn.domestika.org/c_fill,dpr_1.0,f_auto,h_428,pg_1,q_auto:eco,t_base_params,w_760/v1629793568/course-covers/000/001/711/1711-original.jpg?1629793568')
GO
SET IDENTITY_INSERT [category] OFF
GO
SET IDENTITY_INSERT [course] ON 
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (1, 1, N'Introduce to Bread Making', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/baguette.jpg', 81, 3, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 0, 160)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (2, 1, N'Better Ways to Mix Dough for the Molds', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/barmbrack.jpg', 0, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 1, 240)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (3, 1, N'Apple Cake Recipe', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/pain-de-campagne.jpg', 6, 5, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 2, 355)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (4, 1, N'Cooking Tips the Perfect Burger', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/sandwich-bread.jpg', 65, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 3, 245)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (5, 1, N'Fruit Decorations', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/michetta.jpg', 0, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 0, 212)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (6, 1, N'Recipes for Sweet and Healty Treats', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/bauernbrot.jpg', 23, 3, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 1, 273)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (7, 1, N'Better Ways to Mix Dough for the Molds', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/cornbread.jpg', 0, 5, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 2, 254)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (8, 1, N'Carrot Cake Gingerbread', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/zopf.jpg', 53, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 3, 168)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (9, 1, N'Facts About Sugar Products', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/rugbraud.jpg', 60, 5, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 0, 184)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (10, 1, N'Introduction to Baking Cakes', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/guernsey-gache.jpg', 30, 4, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 1, 70)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (11, 1, N'Apple Cake Recipe for Starters', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/pullman-loaf.jpg', 0, 5, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 2, 89)
GO
INSERT [course] ([id], [category_id], [name], [description], [thumbnail], [price], [rating], [content], [level], [duration]) VALUES (12, 1, N'Advanced Sandwich Making Techniques', N'Tired of baking bricks when everyone else is baking super gorgeous sourdough bread with a holey crumb and crispy crust? Change all of that by enrolling in "Sourdough Bread Baking 101."

This is the sourdough baking course you need if you are just starting out in sourdough bread baking. In my other courses, I teach how to bake all sorts of sourdough breads, but in this course, I won''t load you down with lots of information, just enough so it is easy and fun to bake your first loaf of sourdough.

You will first learn how to make your own sourdough starter at home, day by day with detailed instructions, videos and a free pdf downloadable booklet.

Next, you''ll move onto baking waffles to test your new sourdough starter and then onto your first sourdough test loaf. Once you''ve taken a basic white sourdough loaf, steaming hot, out of your own oven, you will be ready to move on to more complex formulas.

There are two basic sourdough bread formulas in this course. I call them "First Loaf" and "Second Loaf." We will follow the easy step by step directions of the process together. The first loaf is easy. It is a one day round boule with a lower hydration dough, which means it is not too wet or sticky. The second loaf is a two day high hydration dough and is a batard or French style loaf.  Working together, I will show you how to handle a wet, sticky dough and how to do an autolyse. This bread results in a lovely tasting bread with large holes and a wonderful crispy crust.

This course is for BEGINNERS who have no idea of how to bake with sourdough or those who have tried and stumbled. I have fifteen years of experience teaching the newbie sourdough baker, so come on! Let''s have some fun! You know want to!

This course would make an excellent gift for the aspiring baker in your family circle, whether an older baker or a young new baker.

Some skills you will learn:

How to make and care for your own sourdough starter

Why it''s important to weigh ingredients.

Gathering some basic baking equipment

How to mix and fold dough. No kneading here.

Shaping, scoring and baking the dough.

What to do after you''ve baked your first loaf... hint (bake your second loaf!)

When you have finished Sourdough Bread Baking 101, you will have the confidence to not only bake using your own sourdough starter, you will be ready for more advanced sourdough baking techniques which will enable you to bake all sorts of sourdough artisan breads.



Some Student Reviews:

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Amanda H - Fantastic Class

I absolutely loved Teresa''s Sourdough 101 class. Her directions were easy to follow, full of great info, and I was able to bake incredible sourdough bread!!! I loved it so much that I have already signed up for her other three courses. My family and I say "thank you Teresa"!!

by Samira M - Very good

For the first time I could bake a bread a reaaly sourdough bread. The course is really good. Simple and direct. Thank you so much Teresa.

by Ban S  - excellent course for sourdough baking

an excellent course for beginners in sourdough baking and excellent instructor with great experience.

by Robin E - More, More, More

Can''t get enough! I love Teresa Greenway''s courses. She is so complete and does not just leave you hanging. She is there for you just like in a classroom and helps troubleshoot and encourage. Thank you so much I look forward to the future bakes and classes.

by Simon H - Brilliant!

I''ve checked out many sourdough baking texts and videos but then I discovered Teresa! Absolutely brilliant, not least, Teresa uses highly accurate gram measurements and not flippin'' cups (what''s more she explains why cups are an inaccurate measure). The course takes the student by the hand and slow, confidently and kindly walks through each step simply. There''s no technical jargon and no assumption that the student knows this or that before setting out. The texts are clear, the videos are clear. This is without doubt the best sourdough bread making course I''ve ever seen. Thank you Teresa!

by Jeni H - Absolutely fantastic!

I have enjoyed this course so much, I''m baking my loafs right now! I found this course answered so many questions I had about making sourdough, I would highly recommend it to anyone starting out!

by Pam H - Outstanding

This is an outstanding class on sourdough - well written text lectures, great videos and clear recipes/formulas. I''ve been a bread baker for 40 years - this class has taken my sourdough baking skill to a whole new level.

by Trish D - Perfect for beginners

This is another great course from Teresa. I have also completed two other sourdough courses of Teresa''s and totally recommend them. If you are new to sourdough....start here and learn from a friendly and informative teacher. Teresa takes you through the process of sourdough baking and you learn at your own pace. Perfect place to start to enter the wonderful world of sourdough baking. A+++', N'/resources/img/product/small/pane-sciocco.jpg', 0, 3, N'Nam cursus eu justo nec dictum. Suspendisse dapibus nibh id mattis semper. Vestibulum dapibus sit amet magna vitae cursus. Nam quis tincidunt diam', 3, 207)
GO
SET IDENTITY_INSERT [course] OFF
GO
SET IDENTITY_INSERT [course_topic] ON 
GO
INSERT [course_topic] ([id], [course_id], [name], [description]) VALUES (7, 1, N'Introduction to Bread Making', N'Aliquam erat volutpat. Integer non nisl eleifend, facilisis libero tincidunt, molestie ex. Mauris fringilla bibendum libero ac vestibulum. Integer gravida pellentesque neque nec egestas. Suspendisse in bibendum odio. Praesent fringilla, urna ac fermentum hendrerit, nisi erat feugiat velit, sit amet ornare elit metus nec nibh. Duis venenatis, leo vitae tincidunt ullamcorper, urna tellus luctus purus, vitae maximus arcu justo sit amet dui. In et facilisis dolor.')
GO
INSERT [course_topic] ([id], [course_id], [name], [description]) VALUES (8, 1, N'Molding Technique', N'Fusce et aliquet massa, a semper ipsum. Praesent eget viverra diam. Suspendisse ut ligula tincidunt, fermentum nulla at, malesuada dui. Proin maximus dui vitae lorem dictum facilisis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris ligula felis, interdum eu nisi ac, volutpat facilisis purus. Suspendisse potenti. Vestibulum ullamcorper convallis ultrices. Aliquam semper sodales nulla, sit amet feugiat velit. Quisque efficitur risus aliquam ex commodo, ac iaculis est pulvinar. Sed ac arcu libero. Phasellus rhoncus, felis vitae accumsan vestibulum, nisi lorem accumsan metus, eu vehicula orci elit ut ligula. Nulla sit amet lacinia augue, eget molestie sem. Suspendisse potenti. Donec molestie turpis id quam eleifend, eget imperdiet mi bibendum. Pellentesque vestibulum convallis sodales.

')
GO
INSERT [course_topic] ([id], [course_id], [name], [description]) VALUES (9, 1, N'Baking the Right Way', N'Morbi id pretium purus. Sed varius tellus eget ante eleifend, quis semper elit eleifend. Mauris efficitur tellus nibh, vel placerat turpis viverra non. Praesent vel rutrum leo. Sed orci metus, lobortis id scelerisque eget, dictum eu augue. Maecenas vel odio ex. Duis eu magna et magna blandit congue. Ut convallis luctus porta. Pellentesque ut nunc laoreet, sagittis odio sed, feugiat massa. Etiam pretium pulvinar turpis vitae egestas.

')
GO
INSERT [course_topic] ([id], [course_id], [name], [description]) VALUES (10, 1, N'Presentation', N'Phasellus laoreet euismod fermentum. Pellentesque et elit enim. Proin in libero magna. Fusce mi velit, viverra eget orci eu, luctus blandit est. Donec rhoncus eu augue eget posuere. Aliquam euismod massa at velit porttitor luctus. Aliquam fringilla mollis diam, vel faucibus dolor consectetur non. Fusce et diam quis augue venenatis ullamcorper ut ut diam.

')
GO
INSERT [course_topic] ([id], [course_id], [name], [description]) VALUES (11, 1, N'Conclusion', N'Sed elementum magna at elit aliquam consequat. Praesent nisi ante, vulputate sit amet arcu convallis, euismod porttitor massa. Mauris rutrum eleifend risus vitae posuere. Aliquam erat volutpat. In et velit nec dui mollis condimentum. Aliquam semper suscipit mauris sed tincidunt. Fusce vitae dui erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla nec justo ac elit fringilla porta. Nunc mattis nulla et elit faucibus, et varius arcu dignissim.

')
GO
INSERT [course_topic] ([id], [course_id], [name], [description]) VALUES (12, 1, N'What is Next?', N'Mauris sed posuere odio. Nulla non congue urna, in semper ligula. Sed vestibulum bibendum porta. Morbi in mauris eros. Donec sodales, arcu at tristique hendrerit, dui tortor scelerisque mauris, tempus sollicitudin dui eros eget lectus. Nunc bibendum, velit lacinia bibendum tincidunt, libero diam aliquet dolor, ac semper odio augue sed lacus. Praesent iaculis, ipsum et tempor lobortis, lorem erat finibus ante, id faucibus tortor purus fringilla nisl. Cras id convallis metus. Nulla facilisi. Mauris tortor nulla, molestie vel tempus eget, sodales ut mi. Cras eleifend risus ligula, quis posuere ipsum eleifend eu. Nullam ante justo, lobortis eu nibh eget, gravida imperdiet libero. Sed quis tellus sagittis, eleifend risus imperdiet, volutpat dolor. Duis congue ipsum sit amet lacus lobortis, in dapibus neque congue.

')
GO
SET IDENTITY_INSERT [course_topic] OFF
GO
SET IDENTITY_INSERT [topic_lesson] ON 
GO
INSERT [topic_lesson] ([id], [topic_id], [name], [description], [material]) VALUES (1, 7, N'Candy cake gummi bears', N'Sed varius tellus eget ante eleifend, quis semper elit eleifend. Mauris efficitur tellus nibh, vel placerat turpis viverra non. ', N'ZRLjZ4f1NyU')
GO
INSERT [topic_lesson] ([id], [topic_id], [name], [description], [material]) VALUES (2, 8, N'Gummi bears dessert muffin pie', N'Praesent vel rutrum leo. Sed orci metus, lobortis id scelerisque eget, dictum eu augue. Maecenas vel odio ex. Duis eu magna et magna blandit congue.', N'_XVNvnQLo44')
GO
INSERT [topic_lesson] ([id], [topic_id], [name], [description], [material]) VALUES (3, 9, N'Lemon drops wafer brownie pastry', N'Ut convallis luctus porta. Pellentesque ut nunc laoreet, sagittis odio sed, feugiat massa. Etiam pretium pulvinar turpis vitae egestas.', N'J70el4lrtIM')
GO
INSERT [topic_lesson] ([id], [topic_id], [name], [description], [material]) VALUES (4, 10, N'Danish cake gummies jelly oat cake', N'Phasellus laoreet euismod fermentum. Pellentesque et elit enim. Proin in libero magna.', N'L4wSpD1iaPE')
GO
INSERT [topic_lesson] ([id], [topic_id], [name], [description], [material]) VALUES (5, 11, N'Sweet roll candy muffin chocolate', N'Donec rhoncus eu augue eget posuere. Aliquam euismod massa at velit porttitor luctus. Aliquam fringilla mollis diam, vel faucibus dolor consectetur non. Fusce et diam quis augue venenatis ullamcorper ut ut diam.', N'cDHh1ZuiiKQ')
GO
INSERT [topic_lesson] ([id], [topic_id], [name], [description], [material]) VALUES (6, 12, N'Danish cake gummies jelly oat cake advance', N'Ut a porta ligula. Fusce gravida erat libero, vel tincidunt mauris semper ornare. Aenean fringilla justo vel arcu ultricies, vel iaculis turpis suscipit. Maecenas sapien dui, pharetra ultrices erat scelerisque, aliquet cursus sapien.', N'vxRuzZXdb_Q')
GO
INSERT [topic_lesson] ([id], [topic_id], [name], [description], [material]) VALUES (7, 7, N'French Banana Bread', N'Vestibulum id vehicula dolor, sed scelerisque orci. In sollicitudin magna ligula, vel faucibus magna gravida id. Sed molestie metus quis dui ornare posuere. Vestibulum nibh odio, laoreet sed velit nec, imperdiet pretium nunc. Pellentesque ultricies maximus nunc, eget luctus ante vestibulum sit amet. Duis tristique leo sed dui aliquet, a ullamcorper felis faucibus. Vestibulum quis ullamcorper lectus, et ullamcorper urna. Nunc euismod hendrerit libero in malesuada. Fusce sit amet arcu urna. Integer arcu risus, imperdiet ac eros et, laoreet congue ex. Maecenas luctus gravida lectus non pharetra. Duis aliquam feugiat ante, id bibendum lorem accumsan id. Duis ut tempor urna. Quisque ut tincidunt arcu, id dapibus massa. Vivamus eget ornare lorem. Nullam augue neque, ultrices at nulla tristique, scelerisque condimentum lorem.

', N'6S6Kf7FadC0')
GO
SET IDENTITY_INSERT [topic_lesson] OFF
GO
SET IDENTITY_INSERT [user] ON 
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (1, N'admin1', N'admin1@gmail.com', N'Nguyễn Văn An', CAST(N'2021-12-12' AS Date), 1, N'0987654321', N'test1', N'default.jpg', 1, 1, 1, N'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (2, N'admin2', N'admin2@gmail.com', N'Nguyễn Văn Bê', CAST(N'2021-11-11' AS Date), 1, N'0989878787', N'test2', N'default.jpg', 1, 1, 1, N'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (3, N'admin3', N'admin3@gmail.com', N'Nguyễn Văn Cê', CAST(N'2021-10-10' AS Date), 1, N'0987677777', N'test3', N'default.jpg', 1, 1, 1, N'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (4, N'admin4', N'admin4@gmail.com', N'Nguyễn Văn Đê', CAST(N'2021-09-09' AS Date), 1, N'0987654543', N'test4', N'default.jpg', 1, 1, 1, N'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (5, N'admin5', N'admin5@gmail.com', N'Nguyễn Văn Ê', CAST(N'2021-08-08' AS Date), 1, N'0987676756', N'test5', N'default.jpg', 1, 1, 1, N'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (6, N'admin6', N'admin6@gmail.com', N'Nguyễn Văn Gờ', CAST(N'2021-07-07' AS Date), 1, N'0987765765', N'test6', N'default.jpg', 1, 1, 1, N'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (7, N'admin7', N'admin7@gmail.com', N'Nguyễn Thị Hát', CAST(N'2021-12-05' AS Date), 1, N'0987890987', N'test7', N'default.jpg', 1, 1, 1, N'$2b$10$5swOA7vNuMcey7jFDrzP4OP1U44u/4wEEn/fjSieMcvMTqNbrSbfK', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (8, N'user1', N'user1@gmail.com', N'Nguyễn Văn I', CAST(N'2021-11-12' AS Date), 1, N'0982345678', N'test8', N'default.jpg', 1, 0, 1, N'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (9, N'user2', N'user2@gmail.com', N'Nguyễn Văn J', CAST(N'2021-10-12' AS Date), 1, N'0982345678', N'test9', N'default.jpg', 1, 0, 1, N'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (10, N'user3', N'user3@gmail.com', N'Nguyễn Thị Ka', CAST(N'2021-09-12' AS Date), 1, N'0987656565', N'test10', N'default.jpg', 1, 0, 1, N'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (11, N'user4', N'user4@gmail.com', N'Nguyễn Văn L', CAST(N'2021-12-12' AS Date), 1, N'0987676765', N'test11', N'default.jpg', 1, 0, 1, N'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (12, N'user5', N'user5@gmail.com', N'Nguyễn Thị Mờ', CAST(N'2021-07-12' AS Date), 1, N'0989898987', N'test12', N'default.jpg', 1, 0, 1, N'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (13, N'user6', N'user6@gmail.com', N'Nguyễn Văn Nờ', CAST(N'2021-02-12' AS Date), 2, N'0986438212', N'test13', N'default.jpg', 1, 0, 1, N'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (14, N'user7', N'user7@gmail.com', N'Nguyễn Văn O', CAST(N'2021-04-12' AS Date), 1, N'0976543523', N'test14', N'default.jpg', 1, 0, 1, N'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
INSERT [user] ([id], [username], [email], [name], [dob], [gender], [phone], [bio], [avatar], [level], [role], [status], [password], [created]) VALUES (15, N'user8', N'user8@gmail.com', N'Nguyễn Thị Pê', CAST(N'2021-01-12' AS Date), 1, N'0987654321', N'test15', N'default.jpg', 1, 0, 1, N'$2b$10$PiPMn/wYaasvhTdVwcaNYOL52tJzHHDvVCSDh2uY0MKxCqypPcMAG', CAST(N'2021-09-18T20:01:46.000' AS DateTime))
GO
SET IDENTITY_INSERT [user] OFF
GO
ALTER TABLE [course]  WITH CHECK ADD  CONSTRAINT [FK_course_category] FOREIGN KEY([category_id])
REFERENCES [category] ([id])
GO
ALTER TABLE [course] CHECK CONSTRAINT [FK_course_category]
GO
ALTER TABLE [course_topic]  WITH CHECK ADD FOREIGN KEY([course_id])
REFERENCES [course] ([id])
GO
ALTER TABLE [discussion]  WITH CHECK ADD  CONSTRAINT [FK_discussion_topic_lesson] FOREIGN KEY([lession_id])
REFERENCES [topic_lesson] ([id])
GO
ALTER TABLE [discussion] CHECK CONSTRAINT [FK_discussion_topic_lesson]
GO
ALTER TABLE [discussion_reply]  WITH CHECK ADD  CONSTRAINT [FK_discussion_reply_discussion] FOREIGN KEY([discussion_id])
REFERENCES [discussion] ([id])
GO
ALTER TABLE [discussion_reply] CHECK CONSTRAINT [FK_discussion_reply_discussion]
GO
ALTER TABLE [topic_lesson]  WITH CHECK ADD  CONSTRAINT [FK__topic_les__topic__5070F446] FOREIGN KEY([topic_id])
REFERENCES [course_topic] ([id])
GO
ALTER TABLE [topic_lesson] CHECK CONSTRAINT [FK__topic_les__topic__5070F446]
GO
ALTER TABLE [user_token_log]  WITH CHECK ADD FOREIGN KEY([id])
REFERENCES [user] ([id])
GO
ALTER TABLE [wishlist]  WITH CHECK ADD FOREIGN KEY([courseid])
REFERENCES [course] ([id])
GO
ALTER TABLE [wishlist]  WITH CHECK ADD FOREIGN KEY([uid])
REFERENCES [user] ([id])
GO
USE [master]
GO
ALTER DATABASE [elearning_beta] SET  READ_WRITE 
GO
